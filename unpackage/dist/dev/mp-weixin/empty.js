"use strict";
const common_vendor = require("./common/vendor.js");
const tuiWingBlank = () => "./components/thorui/tui-wing-blank/tui-wing-blank.js";
const _sfc_main = {
  name: "danganempty",
  components: {
    tuiWingBlank
  },
  data() {
    return {
      itemimg: "../../../static/ann/index/danganadd.png"
    };
  }
};
if (!Array) {
  const _easycom_tui_wing_blank2 = common_vendor.resolveComponent("tui-wing-blank");
  _easycom_tui_wing_blank2();
}
const _easycom_tui_wing_blank = () => "./components/thorui/tui-wing-blank/tui-wing-blank.js";
if (!Math) {
  _easycom_tui_wing_blank();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: $data.itemimg,
    b: common_vendor.p({
      gap: "64"
    })
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/pages/ann/dangan/empty.vue"]]);
exports.MiniProgramPage = MiniProgramPage;
