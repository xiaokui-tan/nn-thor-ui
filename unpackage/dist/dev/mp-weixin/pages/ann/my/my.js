"use strict";
const common_vendor = require("../../../common/vendor.js");
const tuiNavigationBar = () => "../../../components/thorui/tui-navigation-bar/tui-navigation-bar.js";
const _sfc_main = {
  components: {
    tuiNavigationBar
  },
  data() {
    return {
      top: 0,
      //标题图标距离顶部距离
      opacity: 0,
      scrollTop: 0.5
    };
  },
  methods: {
    initNavigation(e) {
      this.opacity = e.opacity;
      this.top = e.top;
    },
    opacityChange(e) {
      this.opacity = e.opacity;
    },
    back() {
      common_vendor.index.navigateBack();
    }
  },
  onPageScroll(e) {
    this.scrollTop = e.scrollTop;
  }
};
if (!Array) {
  const _easycom_tui_icon2 = common_vendor.resolveComponent("tui-icon");
  const _easycom_tui_navigation_bar2 = common_vendor.resolveComponent("tui-navigation-bar");
  (_easycom_tui_icon2 + _easycom_tui_navigation_bar2)();
}
const _easycom_tui_icon = () => "../../../components/thorui/tui-icon/tui-icon.js";
const _easycom_tui_navigation_bar = () => "../../../components/thorui/tui-navigation-bar/tui-navigation-bar.js";
if (!Math) {
  (_easycom_tui_icon + _easycom_tui_navigation_bar)();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.p({
      name: "message"
    }),
    b: common_vendor.p({
      name: "setup"
    }),
    c: $data.top + "px",
    d: common_vendor.o($options.initNavigation),
    e: common_vendor.o($options.opacityChange),
    f: common_vendor.p({
      splitLine: true,
      scrollTop: $data.scrollTop,
      title: "我的",
      backgroundColor: "#fff",
      color: "#333"
    })
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/pages/ann/my/my.vue"]]);
_sfc_main.__runtimeHooks = 1;
wx.createPage(MiniProgramPage);
