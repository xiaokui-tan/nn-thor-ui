"use strict";
const json = {
  tabBar: [
    {
      pagePath: "/pages/ann/index/index",
      text: "首页",
      iconPath: "/static/ann/tabbar/index.png",
      selectedIconPath: "/static/ann/tabbar/index-active.png",
      iconSize: 80
    },
    {
      pagePath: "/pages/ann/dangan/dindex",
      text: "档案",
      iconPath: "/static/ann/tabbar/dangan1.png",
      selectedIconPath: "/static/ann/tabbar/dangan1-active.png"
    },
    {
      pagePath: "/pages/ann/dangan/dindex",
      text: "发布",
      iconPath: "/static/ann/tabbar/fabu.png",
      hump: true,
      selectedIconPath: "/static/ann/tabbar/fabu-active.png"
    },
    {
      pagePath: "/pages/ann/dangan/dindex",
      text: "记账",
      iconPath: "/static/ann/tabbar/jizhang1.png",
      selectedIconPath: "/static/ann/tabbar/jizhang1-active.png"
    },
    {
      pagePath: "/pages/ann/my/my",
      text: "我的",
      iconPath: "/static/ann/tabbar/my1.png",
      selectedIconPath: "/static/ann/tabbar/my1-active.png"
    }
  ],
  category: [
    {
      img: "../../../static/ann/index/dangan.png",
      name: "档案"
    },
    {
      img: "../../../static/ann/index/jizhang.png",
      name: "记账"
    }
  ],
  action: [{
    img: "../../../static/ann/index/gongying.png",
    name: "供应"
  }, {
    img: "../../../static/ann/index/qiugou.png",
    name: "求购"
  }, {
    img: "../../../static/ann/index/ditu.png",
    name: "地图"
  }, {
    img: "../../../static/ann/index/hangqing.png",
    name: "行情"
  }],
  items: [
    {
      id: 1,
      text: "推荐",
      isScreen: false,
      down: false
    },
    {
      id: 2,
      text: "供应",
      isScreen: false,
      down: false
    },
    {
      id: 3,
      text: "求购",
      isScreen: false,
      down: false
    },
    {
      id: 4,
      text: "综合排序",
      isScreen: false,
      down: false
    }
  ],
  comsorts: [{
    id: "1",
    value: "按时间从近到远"
  }, {
    id: "2",
    value: "按位置从近到远"
  }, {
    id: "3",
    value: "按总价从低到高"
  }, {
    id: "4",
    value: "按分类"
  }],
  indexdetails: [
    {
      "id": "1",
      "image": "../../../static/ann/images/free-range-chicken7.png",
      "title": "农家土鸡",
      "actiontype": 0,
      "address": "广东省深圳市南山区XX街XX号",
      "hasphonenum": true,
      "allowonlineconnect": false
    },
    {
      "id": "2",
      "image": "../../../static/ann/images/neimeng-cattle.png",
      "title": "草原牛",
      "actiontype": 1,
      "address": "内蒙古自治区呼和浩特市赛罕区XX路XX号",
      "hasphonenum": false,
      "allowonlineconnect": true
    },
    {
      "id": "3",
      "image": "../../../static/ann/images/goat.png",
      "title": "山羊",
      "actiontype": 0,
      "address": "河南省郑州市二七区XX大道XX号",
      "hasphonenum": true,
      "allowonlineconnect": true
    },
    {
      "id": "4",
      "image": "../../../static/ann/images/hetian-horse.png",
      "title": "和田马",
      "actiontype": 1,
      "address": "浙江省杭州市西湖区XX路XX号",
      "hasphonenum": false,
      "allowonlineconnect": false
    },
    {
      "id": "6",
      "image": "../../../static/ann/images/rabbit.png",
      "title": "兔子",
      "actiontype": 1,
      "address": "广东省广州市天河区XX路XX号",
      "hasphonenum": false,
      "allowonlineconnect": true
    },
    {
      "id": "7",
      "image": "../../../static/ann/images/jiayang-duck.png",
      "title": "家养鸭",
      "actiontype": 0,
      "address": "福建省厦门市思明区XX街XX号",
      "hasphonenum": true,
      "allowonlineconnect": true
    },
    {
      "id": "8",
      "image": "../../../static/ann/images/yufei-goose.png",
      "title": "育肥鹅",
      "actiontype": 1,
      "address": "四川省成都市武侯区XX路XX号",
      "hasphonenum": false,
      "allowonlineconnect": false
    },
    {
      "id": "9",
      "image": "../../../static/ann/images/hemu-dove.png",
      "title": "食用贺姆鸽",
      "actiontype": 0,
      "address": "湖南省长沙市芙蓉区XX街XX号",
      "hasphonenum": true,
      "allowonlineconnect": true
    }
  ]
};
exports.json = json;
