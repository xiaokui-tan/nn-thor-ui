"use strict";
const common_vendor = require("../../../common/vendor.js");
const pages_ann_index_indexjson = require("./indexjson.js");
const _sfc_main = {
  components: {},
  data() {
    return {
      current: 0,
      tabBar: pages_ann_index_indexjson.json.tabBar,
      visible: false,
      category: pages_ann_index_indexjson.json.category,
      action: pages_ann_index_indexjson.json.action,
      items: pages_ann_index_indexjson.json.items,
      comsorts: pages_ann_index_indexjson.json.comsorts,
      indexdetails: pages_ann_index_indexjson.json.indexdetails
    };
  },
  methods: {
    tabbarSwitch(e) {
      this.current = e.index;
    },
    more() {
    },
    handleClick(item) {
      if (item.name == "地图") {
        common_vendor.index.navigateTo({
          url: "/pages/ann/index/maps/maps"
        });
      }
    },
    screen(item) {
      this.items.forEach((item2) => {
        item2.isScreen = false;
      });
      item.isScreen = true;
      if (item.id == 4) {
        item.down = !item.down;
      } else {
        this.items.forEach((item2) => {
          item2.down = false;
        });
      }
      this.visible = item.down;
    },
    comorderby(item) {
      this.tui.toast(item.value);
    }
  }
};
if (!Array) {
  const _easycom_tui_searchbar2 = common_vendor.resolveComponent("tui-searchbar");
  const _easycom_tui_icon2 = common_vendor.resolveComponent("tui-icon");
  const _easycom_tui_list_cell2 = common_vendor.resolveComponent("tui-list-cell");
  const _easycom_tui_list_view2 = common_vendor.resolveComponent("tui-list-view");
  const _easycom_tui_form_button2 = common_vendor.resolveComponent("tui-form-button");
  (_easycom_tui_searchbar2 + _easycom_tui_icon2 + _easycom_tui_list_cell2 + _easycom_tui_list_view2 + _easycom_tui_form_button2)();
}
const _easycom_tui_searchbar = () => "../../../components/thorui/tui-searchbar/tui-searchbar.js";
const _easycom_tui_icon = () => "../../../components/thorui/tui-icon/tui-icon.js";
const _easycom_tui_list_cell = () => "../../../components/thorui/tui-list-cell/tui-list-cell.js";
const _easycom_tui_list_view = () => "../../../components/thorui/tui-list-view/tui-list-view.js";
const _easycom_tui_form_button = () => "../../../components/thorui/tui-form-button/tui-form-button.js";
if (!Math) {
  (_easycom_tui_searchbar + _easycom_tui_icon + _easycom_tui_list_cell + _easycom_tui_list_view + _easycom_tui_form_button)();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.p({
      radius: "40rpx",
      height: "60rpx",
      padding: "5rpx",
      placeholder: "搜索",
      disabled: true
    }),
    b: common_vendor.f($data.category, (item, index, i0) => {
      return {
        a: item.img,
        b: common_vendor.t(item.name),
        c: index,
        d: item.name,
        e: common_vendor.o((...args) => $options.more && $options.more(...args), index)
      };
    }),
    c: common_vendor.f($data.action, (item, index, i0) => {
      return {
        a: item.img,
        b: common_vendor.o(($event) => $options.handleClick(item), index),
        c: common_vendor.t(item.name),
        d: index,
        e: item.name,
        f: common_vendor.o((...args) => $options.more && $options.more(...args), index)
      };
    }),
    d: common_vendor.f($data.items, (item, index, i0) => {
      return {
        a: common_vendor.t(item.text),
        b: item.id == 4 && !item.down || item.id != 4 && item.down,
        c: common_vendor.o(($event) => $options.screen(item), index),
        d: "2ddaedfb-1-" + i0,
        e: item.down,
        f: common_vendor.o(($event) => $options.screen(item), index),
        g: "2ddaedfb-2-" + i0,
        h: index,
        i: item.isScreen ? 1 : "",
        j: common_vendor.o(($event) => $options.screen(item), index)
      };
    }),
    e: common_vendor.p({
      name: "arrowup",
      size: 12,
      color: "#5677fc",
      bold: true
    }),
    f: common_vendor.p({
      name: "arrowdown",
      size: 12,
      color: "#5677fc",
      bold: true
    }),
    g: common_vendor.f($data.comsorts, (item, id, i0) => {
      return {
        a: common_vendor.t(item.value),
        b: common_vendor.o(($event) => $options.comorderby(item), item.id),
        c: item.id,
        d: item.value,
        e: "2ddaedfb-4-" + i0 + ",2ddaedfb-3"
      };
    }),
    h: common_vendor.p({
      hover: false,
      arrow: false
    }),
    i: $data.visible,
    j: common_vendor.p({
      title: ""
    }),
    k: common_vendor.f($data.indexdetails, (item, id, i0) => {
      return common_vendor.e({
        a: item.image,
        b: common_vendor.t(item.title),
        c: item.actiontype === 0
      }, item.actiontype === 0 ? {} : {}, {
        d: "2ddaedfb-7-" + i0 + "," + ("2ddaedfb-6-" + i0),
        e: common_vendor.t(item.address),
        f: item.hasphonenum
      }, item.hasphonenum ? {
        g: "2ddaedfb-9-" + i0 + "," + ("2ddaedfb-8-" + i0),
        h: common_vendor.p({
          name: "kefu",
          size: "15"
        }),
        i: "2ddaedfb-8-" + i0 + "," + ("2ddaedfb-6-" + i0),
        j: common_vendor.p({
          background: "#7CD8B4",
          color: "#333333",
          type: "primary",
          width: "160rpx",
          size: "20",
          height: "36rpx",
          radius: "50px"
        })
      } : {}, {
        k: item.allowonlineconnect
      }, item.allowonlineconnect ? {
        l: "2ddaedfb-11-" + i0 + "," + ("2ddaedfb-10-" + i0),
        m: common_vendor.p({
          name: "message-fill",
          size: "15"
        }),
        n: "2ddaedfb-10-" + i0 + "," + ("2ddaedfb-6-" + i0),
        o: common_vendor.p({
          background: "#7CD8B4",
          color: "#333333",
          type: "primary",
          width: "160rpx",
          size: "20",
          height: "36rpx",
          radius: "50px"
        })
      } : {}, {
        p: item.id,
        q: item.value,
        r: "2ddaedfb-6-" + i0 + ",2ddaedfb-5"
      });
    }),
    l: common_vendor.p({
      name: "gps",
      size: "15"
    }),
    m: common_vendor.p({
      hover: false,
      arrow: false
    }),
    n: common_vendor.p({
      title: "",
      color: "#777"
    })
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/pages/ann/index/index.vue"]]);
wx.createPage(MiniProgramPage);
