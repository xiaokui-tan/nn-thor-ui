"use strict";
const common_vendor = require("../../../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      latitude: 39.984245,
      longitude: 116.407408,
      scale: 12,
      searchKeyword: "",
      markers: [],
      searching: false,
      isMapZooming: false,
      accuracy: "",
      selectedLocation: "",
      selectedPlace: "",
      infoWindow: null,
      map: null
    };
  },
  mounted() {
    this.loadAMapModule().then(() => {
      this.initMap();
      this.getLocation();
    });
  },
  methods: {
    loadAMapModule() {
      return new Promise((resolve, reject) => {
        common_vendor.index.requireModule("amap").load({
          key: "0b9c1cb014eca86e069e00c64cc6f4d4",
          version: "1.4.15",
          plugins: []
        }, () => {
          resolve();
        }, (error) => {
          reject(error);
        });
      });
    },
    getLocation() {
      this.map = new AMap.Map("map", {
        resizeEnable: true
      });
      this.map.plugin("AMap.Geolocation", function() {
        var geolocation = new AMap.Geolocation({
          enableHighAccuracy: true,
          //是否使用高精度定位，默认:true
          timeout: 1e4
          //超过10秒后停止定位，默认：5s
        });
        geolocation.getCurrentPosition();
        AMap.event.addListener(geolocation, "complete", (data) => {
          this.latitude = data.position.lat;
          this.longitude = data.position.lng;
        });
        AMap.event.addListener(geolocation, "error", (err) => {
          console.error("定位失败：", err);
        });
      });
    },
    // 使用高德地图的搜索方法
    async search() {
      if (this.searching || !this.searchKeyword.trim())
        return;
      this.searching = true;
      try {
        const response = await fetch(
          `https://restapi.amap.com/v3/place/text?key=0b9c1cb014eca86e069e00c64cc6f4d4&keywords=${this.searchKeyword}&city=武汉`
        );
        const data = await response.json();
        this.markers = data.pois.map((poi) => ({
          latitude: poi.location.lat,
          longitude: poi.location.lng,
          title: poi.name
        }));
      } catch (error) {
        console.error("搜索出错：", error);
      } finally {
        this.searching = false;
      }
    },
    // 处理地图标记点击事件
    markerTap(e) {
      const marker = this.markers.find((marker2) => marker2.latitude === e.marker.latitude && marker2.longitude === e.marker.longitude);
      if (marker) {
        console.log(marker.title);
      }
    },
    // 处理地图点击事件
    handleMapTap(event) {
      const {
        lng,
        lat
      } = event.lnglat;
      this.longitude = lng.toFixed(6);
      this.latitude = lat.toFixed(6);
      this.accuracy = "10000";
      this.map.clearMap();
      new AMap.Marker({
        position: [lng, lat],
        map: this.map
      });
      const geocoder = new AMap.Geocoder();
      geocoder.getAddress([lng, lat], (status, result) => {
        if (status === "complete" && result.regeocode) {
          this.selectedLocation = result.regeocode.formattedAddress;
          this.selectedPlace = result.regeocode.formattedAddress;
          this.openInfoWindow([lng, lat], this.selectedLocation);
        }
      });
    },
    // 使用高德地图的初始化方法
    initMap() {
      this.map = new AMap.Map("map", {
        resizeEnable: true
      });
      AMap.plugin("AMap.Geolocation", () => {
        const geolocation = new AMap.Geolocation({
          enableHighAccuracy: true,
          timeout: 1e4,
          buttonOffset: new AMap.Pixel(10, 20)
        });
        this.map.addControl(geolocation);
        geolocation.getCurrentPosition();
        AMap.event.addListener(geolocation, "complete", (data) => {
          this.longitude = data.position.lng;
          this.latitude = data.position.lat;
        });
        AMap.event.addListener(geolocation, "error", (err) => {
          console.error("定位失败：", err);
        });
      });
    },
    // 打开信息窗口方法
    openInfoWindow(point, content) {
      if (this.infoWindow) {
        this.infoWindow.close();
      }
      this.infoWindow = new AMap.InfoWindow({
        content,
        offset: new AMap.Pixel(0, -20)
      });
      this.infoWindow.open(this.map, point);
    },
    // 定位到位置方法
    locateOnMap() {
      if (this.longitude && this.latitude) {
        this.map.setCenter([this.longitude, this.latitude]);
      }
    },
    // 设置定位方法
    setLocation() {
      const center = this.map.getCenter();
      this.longitude = center.lng.toFixed(6);
      this.latitude = center.lat.toFixed(6);
      this.accuracy = "10000";
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return common_vendor.e({
    a: $data.longitude,
    b: $data.latitude,
    c: $data.scale,
    d: common_vendor.o((...args) => $options.handleMapTap && $options.handleMapTap(...args)),
    e: common_vendor.o((...args) => $options.markerTap && $options.markerTap(...args)),
    f: common_vendor.o((...args) => _ctx.handleMapTouchStart && _ctx.handleMapTouchStart(...args)),
    g: common_vendor.o((...args) => _ctx.handleMapTouchEnd && _ctx.handleMapTouchEnd(...args)),
    h: $data.searchKeyword,
    i: common_vendor.o(($event) => $data.searchKeyword = $event.detail.value),
    j: !$data.searching
  }, !$data.searching ? {} : {}, {
    k: common_vendor.o((...args) => $options.search && $options.search(...args)),
    l: $data.searching || !$data.searchKeyword.trim(),
    m: $data.longitude,
    n: common_vendor.o(($event) => $data.longitude = $event.detail.value),
    o: $data.latitude,
    p: common_vendor.o(($event) => $data.latitude = $event.detail.value),
    q: $data.accuracy,
    r: common_vendor.o(($event) => $data.accuracy = $event.detail.value),
    s: common_vendor.t($data.selectedPlace),
    t: common_vendor.o((...args) => $options.locateOnMap && $options.locateOnMap(...args)),
    v: common_vendor.o((...args) => $options.setLocation && $options.setLocation(...args))
  });
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-761821f5"], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/pages/ann/index/maps/maps.vue"]]);
wx.createPage(MiniProgramPage);
