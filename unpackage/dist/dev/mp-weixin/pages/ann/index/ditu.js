"use strict";
const common_vendor = require("../../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      latitude: 39.984245,
      longitude: 116.407408,
      scale: 12,
      searchKeyword: "",
      markers: [],
      searching: false,
      // 添加一个标志位，用于表示是否正在搜索中
      isMapZooming: false,
      // 标志位，用于表示地图是否正在缩放中
      accuracy: "",
      selectedLocation: "",
      selectedPlace: "",
      infoWindow: null,
      map: null
    };
  },
  mounted() {
    this.getLocation();
  },
  methods: {
    getLocation() {
      common_vendor.index.getLocation({
        type: "gcj02",
        success: (res) => {
          this.latitude = res.latitude;
          this.longitude = res.longitude;
          this.initMap();
        }
      });
    },
    async search() {
      if (this.searching || !this.searchKeyword.trim())
        return;
      this.searching = true;
      const that = this;
      const url = `https://api.map.baidu.com/place/v2/search?query=${this.searchKeyword}&region=北京&output=json&ak=PlhFWpA02aoURjAOpnWcRGqw7AI8EEyO`;
      try {
        const res = await common_vendor.index.request({
          url,
          method: "GET"
        });
        const result = res[1].data.results;
        that.markers = [];
        result.forEach((item) => {
          that.markers.push({
            latitude: item.location.lat,
            longitude: item.location.lng,
            title: item.name
          });
        });
      } catch (error) {
        console.error("搜索出错：", error);
      } finally {
        this.searching = false;
      }
    },
    markerTap(e) {
      console.log(e.markerId);
    },
    handleMapTouchStart() {
      this.isMapZooming = true;
    },
    handleMapTouchEnd() {
      this.isMapZooming = false;
    },
    initMap() {
      this.map = new BMap.Map("map");
      const point = new BMap.Point(this.longitude, this.latitude);
      this.map.centerAndZoom(point, 15);
      this.map.addControl(new BMap.NavigationControl());
      this.map.addControl(new BMap.ScaleControl());
      this.map.addControl(new BMap.OverviewMapControl());
      this.map.addEventListener("tilesloaded", () => {
        const logo = document.querySelector(".anchorBL");
        if (logo) {
          logo.remove();
        }
      });
      this.map.addEventListener("click", (e) => {
        const {
          lng,
          lat
        } = e.point;
        this.longitude = lng.toFixed(6);
        this.latitude = lat.toFixed(6);
        this.accuracy = "10000";
        this.map.clearOverlays();
        const marker = new BMap.Marker(new BMap.Point(lng, lat));
        this.map.addOverlay(marker);
        const geoc = new BMap.Geocoder();
        geoc.getLocation(new BMap.Point(lng, lat), (rs) => {
          rs.poiList && rs.poiList[0];
          this.selectedLocation = rs.address;
          this.selectedPlace = rs.address;
          this.openInfoWindow(e.point, this.selectedLocation);
        });
      });
    },
    openInfoWindow(point, content) {
      if (this.infoWindow) {
        this.map.closeInfoWindow(this.infoWindow);
      }
      this.infoWindow = new BMap.InfoWindow(content, {
        width: 200,
        height: 50,
        enableCloseOnClick: false,
        style: {
          borderRadius: "10px",
          backgroundColor: "#ffffff",
          boxShadow: "10 10 10px rgba(0,0,0,0.3)"
          // 添加阴影效果
        }
      });
      this.map.openInfoWindow(this.infoWindow, point);
    },
    locateOnMap() {
      if (this.longitude && this.latitude) {
        const point = new BMap.Point(this.longitude, this.latitude);
        this.map.centerAndZoom(point, 15);
      }
    },
    setLocation() {
      const center = this.map.getCenter();
      this.longitude = center.lng.toFixed(6);
      this.latitude = center.lat.toFixed(6);
      this.accuracy = "10000";
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return common_vendor.e({
    a: $data.latitude,
    b: $data.longitude,
    c: $data.scale,
    d: common_vendor.o((...args) => $options.markerTap && $options.markerTap(...args)),
    e: common_vendor.o((...args) => $options.handleMapTouchStart && $options.handleMapTouchStart(...args)),
    f: common_vendor.o((...args) => $options.handleMapTouchEnd && $options.handleMapTouchEnd(...args)),
    g: $data.searchKeyword,
    h: common_vendor.o(($event) => $data.searchKeyword = $event.detail.value),
    i: !$data.searching
  }, !$data.searching ? {} : {}, {
    j: common_vendor.o((...args) => $options.search && $options.search(...args)),
    k: $data.searching || !$data.searchKeyword.trim(),
    l: $data.longitude,
    m: common_vendor.o(($event) => $data.longitude = $event.detail.value),
    n: $data.latitude,
    o: common_vendor.o(($event) => $data.latitude = $event.detail.value),
    p: $data.accuracy,
    q: common_vendor.o(($event) => $data.accuracy = $event.detail.value),
    r: common_vendor.t($data.selectedPlace),
    s: common_vendor.o((...args) => $options.locateOnMap && $options.locateOnMap(...args)),
    t: common_vendor.o((...args) => $options.setLocation && $options.setLocation(...args))
  });
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/pages/ann/index/ditu.vue"]]);
wx.createPage(MiniProgramPage);
