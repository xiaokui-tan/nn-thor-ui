"use strict";
const common_vendor = require("../../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      loopData0: [
        {
          lanhuimage0: "https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPng4f02faeb8e00067ec1291e9335324a899e85cabf0278f202e588b45e5a2d151c",
          lanhutext0: "供应"
        },
        {
          lanhuimage0: "https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPng006c123a91f7d6c4603acca7ea01496967a8763e4c32c9f5c68cf49445e00fb1",
          lanhutext0: "求购"
        },
        {
          lanhuimage0: "https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPng23242e75960e9485ae73e0500a3d53e2f671a0fac91e2606707966e551e49471",
          lanhutext0: "地图"
        },
        {
          lanhuimage0: "https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPng8526698f483565b1363ab11de7bf0d79a7ee023229c1e3941e517ce96d306fde",
          lanhutext0: "行情"
        }
      ],
      loopData1: [
        {
          lanhutext0: "首页",
          lanhufontColor0: "rgba(59,194,164,1)",
          specialSlot1: {
            lanhuimage0: "https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPngb0b8c4245e1f1b72ac1f822b3e2ff271188c8b485217dd78ac3d681454a3bb3f"
          },
          slot1: 1
        },
        {
          lanhutext0: "档案",
          lanhufontColor0: "rgba(102,102,102,1)",
          specialSlot1: {
            lanhuimage0: "https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPng636b54511e77538a0c9920907bd3479d9c6f6a5c04e67ae7dafda530640c64df"
          },
          slot1: 1
        },
        {
          lanhutext0: "发布",
          lanhufontColor0: "rgba(102,102,102,1)",
          specialSlot1: {
            lanhuimage0: "https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPngfa43fb247f6b046c96ffd5918a11c4ec6543c0199b44233d3adb8ec0ee732770"
          },
          slot1: 1
        },
        {
          lanhutext0: "记账",
          lanhufontColor0: "rgba(102,102,102,1)",
          specialSlot2: {
            lanhuimage0: "https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPng8aa9788d898b8d3df6f49f2d99e077035fb26e39a8c8609c9229be2d9812e862"
          },
          slot2: 2
        },
        {
          lanhutext0: "我的",
          lanhufontColor0: "rgba(102,102,102,1)",
          specialSlot2: {
            lanhuimage0: "https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPng1d8b22f07d3f6dd6580b07ca9718abff61a20c4074c40cf5362ef333cadd7011"
          },
          slot2: 2
        }
      ],
      constants: {}
    };
  },
  methods: {}
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.f($data.loopData0, (item, index, i0) => {
      return {
        a: item.lanhuimage0,
        b: item.lanhutext0,
        c: index
      };
    }),
    b: common_vendor.f($data.loopData1, (item, index, i0) => {
      return common_vendor.e({
        a: item.slot1 === 1
      }, item.slot1 === 1 ? {
        b: item.specialSlot1.lanhuimage0
      } : {}, {
        c: item.slot2 === 2
      }, item.slot2 === 2 ? {
        d: item.specialSlot2.lanhuimage0
      } : {}, {
        e: item.lanhutext0,
        f: item.lanhufontColor0,
        g: index
      });
    })
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/pages/ann/index1/index1.vue"]]);
wx.createPage(MiniProgramPage);
