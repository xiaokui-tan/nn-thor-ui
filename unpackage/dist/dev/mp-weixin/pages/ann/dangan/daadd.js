"use strict";
const common_vendor = require("../../../common/vendor.js");
const components_common_tuiValidation_tuiValidation = require("../../../components/common/tui-validation/tui-validation.js");
const tuiRadio = () => "../../../components/thorui/tui-radio/tui-radio.js";
const tuiRadioGroup = () => "../../../components/thorui/tui-radio-group/tui-radio-group.js";
const tuiNumberbox = () => "../../../components/thorui/tui-numberbox/tui-numberbox.js";
const tuiInput = () => "../../../components/thorui/tui-input/tui-input.js";
const tuiDatetime = () => "../../../components/thorui/tui-datetime/tui-datetime.js";
const tuiButton = () => "../../../components/thorui/tui-button/tui-button.js";
const _sfc_main = {
  components: {
    tuiRadio,
    tuiRadioGroup,
    tuiNumberbox,
    tuiInput,
    tuiDatetime,
    tuiButton
  },
  data() {
    return {
      pwdArr: ["", "", "", "", "", ""],
      pricevalue: 18.5,
      weightvalue: 269.7,
      totalpricevalue: 4989.45,
      buycostvalue: 450,
      initweightvalue: 45.9,
      type: 2,
      startYear: 1980,
      endYear: 2030,
      cancelColor: "#888",
      color: "#5677fc",
      setDateTime: "",
      result: "",
      unitTop: false,
      radius: false,
      sexItems: [
        {
          name: "公",
          value: "1",
          checked: true
        },
        {
          name: "母",
          value: "2",
          checked: false
        },
        {
          name: "无",
          value: "3",
          checked: false
        }
      ],
      statusItems: [
        {
          name: "养殖中",
          value: "1",
          checked: true
        },
        {
          name: "待出栏",
          value: "2",
          checked: false
        },
        {
          name: "已销售",
          value: "3",
          checked: false
        },
        {
          name: "已处理",
          value: "4",
          checked: false
        }
      ]
    };
  },
  methods: {
    formSubmit: function(e) {
      let formData = e.detail.value;
      let checkRes = components_common_tuiValidation_tuiValidation.form.validation(formData, rules);
      if (!checkRes) {
        common_vendor.index.showToast({
          title: "验证通过!",
          icon: "none"
        });
      } else {
        common_vendor.index.showToast({
          title: checkRes,
          icon: "none"
        });
      }
    },
    formReset: function(e) {
      console.log("清空数据");
    },
    change: function(e) {
      this.value = e.value;
    },
    //调用此方法显示选择器，需等组件初始化完成后调用，避免在onLoad中调用
    show: function(e) {
      this.$refs.dateTime && this.$refs.dateTime.show();
    },
    changedatatime: function(e) {
      this.result = e.result;
    }
  }
};
let rules = [{
  name: "mobile",
  rule: ["required"],
  msg: ["请输入编号"]
}];
if (!Array) {
  const _easycom_tui_list_cell2 = common_vendor.resolveComponent("tui-list-cell");
  const _easycom_tui_radio2 = common_vendor.resolveComponent("tui-radio");
  const _easycom_tui_label2 = common_vendor.resolveComponent("tui-label");
  const _easycom_tui_radio_group2 = common_vendor.resolveComponent("tui-radio-group");
  const _easycom_tui_numberbox2 = common_vendor.resolveComponent("tui-numberbox");
  const _easycom_tui_datetime2 = common_vendor.resolveComponent("tui-datetime");
  (_easycom_tui_list_cell2 + _easycom_tui_radio2 + _easycom_tui_label2 + _easycom_tui_radio_group2 + _easycom_tui_numberbox2 + _easycom_tui_datetime2)();
}
const _easycom_tui_list_cell = () => "../../../components/thorui/tui-list-cell/tui-list-cell.js";
const _easycom_tui_radio = () => "../../../components/thorui/tui-radio/tui-radio.js";
const _easycom_tui_label = () => "../../../components/thorui/tui-label/tui-label.js";
const _easycom_tui_radio_group = () => "../../../components/thorui/tui-radio-group/tui-radio-group.js";
const _easycom_tui_numberbox = () => "../../../components/thorui/tui-numberbox/tui-numberbox.js";
const _easycom_tui_datetime = () => "../../../components/thorui/tui-datetime/tui-datetime.js";
if (!Math) {
  (_easycom_tui_list_cell + _easycom_tui_radio + _easycom_tui_label + _easycom_tui_radio_group + _easycom_tui_numberbox + _easycom_tui_datetime)();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.p({
      hover: false
    }),
    b: common_vendor.f($data.sexItems, (item, index, i0) => {
      return {
        a: "2a9be13c-5-" + i0 + "," + ("2a9be13c-4-" + i0),
        b: common_vendor.p({
          checked: item.checked,
          value: item.value,
          color: "#07c160",
          borderColor: "#999"
        }),
        c: common_vendor.t(item.name),
        d: "2a9be13c-4-" + i0 + "," + ("2a9be13c-3-" + i0),
        e: index,
        f: "2a9be13c-3-" + i0 + ",2a9be13c-2"
      };
    }),
    c: common_vendor.p({
      hover: false
    }),
    d: common_vendor.f($data.statusItems, (item, index, i0) => {
      return {
        a: "2a9be13c-10-" + i0 + "," + ("2a9be13c-9-" + i0),
        b: common_vendor.p({
          checked: item.checked,
          value: item.value,
          color: "#07c160",
          borderColor: "#999"
        }),
        c: common_vendor.t(item.name),
        d: "2a9be13c-9-" + i0 + "," + ("2a9be13c-8-" + i0),
        e: index,
        f: "2a9be13c-8-" + i0 + ",2a9be13c-7"
      };
    }),
    e: common_vendor.p({
      hover: false,
      unlined: true
    }),
    f: common_vendor.p({
      name: "price",
      step: 0.5,
      value: $data.pricevalue,
      min: 0.01,
      max: 5e3,
      width: 400,
      height: 60,
      ["background-color"]: "rgb(248,248,248)",
      hange: "change"
    }),
    g: common_vendor.p({
      hover: false
    }),
    h: common_vendor.p({
      name: "weight",
      step: 0.5,
      value: $data.weightvalue,
      min: 0.01,
      max: 5e3,
      width: 400,
      height: 60,
      ["background-color"]: "rgb(248,248,248)",
      hange: "change"
    }),
    i: common_vendor.p({
      hover: false
    }),
    j: common_vendor.p({
      name: "totalprice",
      step: 0.5,
      value: $data.totalpricevalue,
      min: 0.01,
      max: 5e3,
      width: 400,
      height: 60,
      ["background-color"]: "rgb(248,248,248)",
      hange: "change"
    }),
    k: common_vendor.p({
      hover: false
    }),
    l: common_vendor.p({
      name: "buycost",
      step: 0.5,
      value: $data.buycostvalue,
      min: 0.01,
      max: 5e3,
      width: 400,
      height: 60,
      ["background-color"]: "rgb(248,248,248)",
      hange: "change"
    }),
    m: common_vendor.p({
      hover: false
    }),
    n: common_vendor.t($data.result),
    o: common_vendor.o((...args) => $options.show && $options.show(...args)),
    p: common_vendor.sr("dateTime", "2a9be13c-20,2a9be13c-19"),
    q: common_vendor.o($options.changedatatime),
    r: common_vendor.p({
      type: 2
    }),
    s: common_vendor.p({
      hover: false
    }),
    t: common_vendor.p({
      name: "weight",
      step: 0.5,
      value: $data.initweightvalue,
      min: 0.01,
      max: 5e3,
      width: 400,
      height: 60,
      ["background-color"]: "rgb(248,248,248)",
      hange: "change"
    }),
    v: common_vendor.p({
      hover: false
    }),
    w: common_vendor.p({
      name: "weight",
      step: 0.5,
      value: $data.initweightvalue,
      min: 0.01,
      max: 5e3,
      width: 400,
      height: 60,
      ["background-color"]: "rgb(248,248,248)",
      hange: "change"
    }),
    x: common_vendor.p({
      hover: false
    }),
    y: common_vendor.o((...args) => $options.formSubmit && $options.formSubmit(...args)),
    z: common_vendor.o((...args) => $options.formReset && $options.formReset(...args))
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/pages/ann/dangan/daadd.vue"]]);
wx.createPage(MiniProgramPage);
