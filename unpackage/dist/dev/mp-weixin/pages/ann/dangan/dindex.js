"use strict";
const pages_ann_dangan_danganjson = require("./danganjson.js");
const common_vendor = require("../../../common/vendor.js");
const tuiBottomPopup = () => "../../../components/thorui/tui-bottom-popup/tui-bottom-popup.js";
const danganempty = () => "./empty2.js";
const _sfc_main = {
  components: {
    tuiBottomPopup,
    danganempty
  },
  data() {
    return {
      visible: true,
      category: pages_ann_dangan_danganjson.json.category,
      popupShow: false,
      itemimg: "../../../static/ann/index/danganadd.png"
    };
  },
  mounted() {
    this.visible = false;
  },
  methods: {
    //调用此方法显示弹层
    showPopup: function() {
      this.popupShow = true;
    },
    hiddenPopup: function() {
      this.popupShow = false;
    }
  }
};
if (!Array) {
  const _component_danganempty = common_vendor.resolveComponent("danganempty");
  _component_danganempty();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: $data.visible
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/pages/ann/dangan/dindex.vue"]]);
wx.createPage(MiniProgramPage);
