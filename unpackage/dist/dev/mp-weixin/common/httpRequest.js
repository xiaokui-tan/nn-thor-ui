"use strict";
const common_vendor = require("./vendor.js");
const tui = {
  //接口地址
  interfaceUrl: function() {
    return "https://www.thorui.cn";
  },
  toast: function(text, duration, success) {
    common_vendor.index.showToast({
      duration: duration || 2e3,
      title: text || "出错啦~",
      icon: success ? "success" : "none"
    });
  },
  modal: function(title, content, showCancel, callback, confirmColor, confirmText) {
    common_vendor.index.showModal({
      title: title || "提示",
      content,
      showCancel,
      cancelColor: "#555",
      confirmColor: confirmColor || "#5677fc",
      confirmText: confirmText || "确定",
      success(res) {
        if (res.confirm) {
          callback && callback(true);
        } else {
          callback && callback(false);
        }
      }
    });
  },
  isAndroid: function() {
    const res = common_vendor.index.getSystemInfoSync();
    return res.platform.toLocaleLowerCase() == "android";
  },
  isPhoneX: function() {
    const res = common_vendor.index.getSystemInfoSync();
    let iphonex = false;
    let models = ["iphonex", "iphonexr", "iphonexsmax", "iphone11", "iphone11pro", "iphone11promax"];
    const model = res.model.replace(/\s/g, "").toLowerCase();
    if (models.includes(model)) {
      iphonex = true;
    }
    return iphonex;
  },
  constNum: function() {
    let time = 0;
    return time;
  },
  delayed: null,
  loadding: false,
  showLoading: function(title, mask = true) {
    common_vendor.index.showLoading({
      mask,
      title: title || "请稍候..."
    });
  },
  /**
   * 请求数据处理
   * @param string url 请求地址
   * @param string method 请求方式
   *  GET or POST
   * @param {*} postData 请求参数
   * @param bool isDelay 是否延迟显示loading
   * @param bool isForm 数据格式
   *  true: 'application/x-www-form-urlencoded'
   *  false:'application/json'
   * @param bool hideLoading 是否隐藏loading
   *  true: 隐藏
   *  false:显示
   */
  request: async function(url, method, postData, isDelay, isForm, hideLoading) {
    tui.loadding && common_vendor.index.hideLoading();
    tui.loadding = false;
    if (!hideLoading) {
      if (isDelay) {
        tui.delayed = setTimeout(() => {
          tui.loadding = true;
          tui.showLoading();
          clearTimeout(tui.delayed);
        }, 1e3);
      } else {
        tui.loadding = true;
        tui.showLoading();
      }
    }
    return new Promise((resolve, reject) => {
      common_vendor.index.request({
        url: tui.interfaceUrl() + url,
        data: postData,
        header: {
          "content-type": isForm ? "application/x-www-form-urlencoded" : "application/json",
          "Authorization": tui.getToken()
        },
        method,
        //'GET','POST'
        dataType: "json",
        success: (res) => {
          clearTimeout(tui.delayed);
          tui.delayed = null;
          if (tui.loadding && !hideLoading) {
            common_vendor.index.hideLoading();
          }
          resolve(res.data);
        },
        fail: (res) => {
          clearTimeout(tui.delayed);
          tui.delayed = null;
          tui.toast("网络不给力，请稍后再试~");
          reject(res);
        }
      });
    });
  },
  /**
   * 上传文件
   * @param string url 请求地址
   * @param string src 文件路径
   */
  uploadFile: function(url, src) {
    tui.showLoading();
    return new Promise((resolve, reject) => {
      common_vendor.index.uploadFile({
        url: tui.interfaceUrl() + url,
        filePath: src,
        name: "file",
        header: {
          "token": tui.getToken()
        },
        formData: {
          // sizeArrayText:""
        },
        success: function(res) {
          common_vendor.index.hideLoading();
          let d = JSON.parse(res.data.replace(/\ufeff/g, "") || "{}");
          if (d.code % 100 == 0) {
            let fileObj = d.data;
            resolve(fileObj);
          } else {
            that.toast(res.msg);
          }
        },
        fail: function(res) {
          reject(res);
          that.toast(res.msg);
        }
      });
    });
  },
  tuiJsonp: function(url, callback, callbackname) {
  },
  //设置用户信息
  setUserInfo: function(mobile, token) {
    common_vendor.index.setStorageSync("thorui_mobile", mobile);
  },
  //获取token
  getToken() {
    return common_vendor.index.getStorageSync("thorui_token");
  },
  //判断是否登录
  isLogin: function() {
    return common_vendor.index.getStorageSync("thorui_mobile") ? true : false;
  },
  //跳转页面，校验登录状态
  href(url, isVerify) {
    if (isVerify && !tui.isLogin()) {
      common_vendor.index.navigateTo({
        url: "/pages/common/login/login"
      });
    } else {
      common_vendor.index.navigateTo({
        url
      });
    }
  }
};
exports.tui = tui;
