"use strict";
const common_vendor = require("../../../common/vendor.js");
const components_common_tuiRequest_tuiBase = require("./tui-base.js");
const components_common_tuiRequest_tuiTaskKeyStore = require("./tui-taskKeyStore.js");
const store = components_common_tuiRequest_tuiTaskKeyStore.createTaskKeyStore();
class THORUI_INNER {
  constructor(initConfig = {}) {
    this.initConfig = initConfig;
    this.request = [];
    this.response = [];
    this.dispatchRequest = this.dispatchRequest.bind(this);
    this.loading = false;
  }
  dispatchRequest(config = {}) {
    let params = components_common_tuiRequest_tuiBase.base.mergeConfig(this.initConfig, config);
    if (params.requestTaskKey && store.requestTaskStorage(params.requestTaskKey)) {
      return new Promise((resolve, reject) => {
        reject({
          statusCode: -9999,
          errMsg: "request:cancelled"
        });
      });
    }
    let options = components_common_tuiRequest_tuiBase.base.getOptions(params);
    let promise = Promise.resolve(options);
    promise = promise.then((config2) => {
      if (params.showLoading && !this.loading) {
        this.loading = true;
        components_common_tuiRequest_tuiBase.base.showLoading();
      }
      return new Promise((resolve, reject) => {
        let requestTask = common_vendor.index.request({
          ...options,
          success: (res) => {
            if (params.showLoading && this.loading) {
              this.loading = false;
              common_vendor.index.hideLoading();
            }
            resolve(params.concise ? res.data : res);
          },
          fail: (err) => {
            if (params.showLoading && this.loading) {
              this.loading = false;
              common_vendor.index.hideLoading();
            }
            if (params.errorMsg) {
              components_common_tuiRequest_tuiBase.base.toast(params.errorMsg);
            }
            reject(err);
          },
          complete: () => {
            store.removeRequestTaskKey(params.requestTaskKey);
          }
        });
        if (params.timeout && typeof params.timeout === "number" && params.timeout > 3e3) {
          setTimeout(() => {
            try {
              store.removeRequestTaskKey(params.requestTaskKey);
              requestTask.abort();
            } catch (e) {
            }
            resolve({
              statusCode: -9999,
              errMsg: "request:cancelled"
            });
          }, params.timeout);
        }
      });
    });
    return promise;
  }
}
const inner = new THORUI_INNER(components_common_tuiRequest_tuiBase.base.config());
const http = {
  interceptors: {
    request: {
      use: (fulfilled, rejected) => {
        inner.request.push({
          fulfilled,
          rejected
        });
      },
      eject: (name) => {
        if (inner.request[name]) {
          inner.request[name] = null;
        }
      }
    },
    response: {
      use: (fulfilled, rejected) => {
        inner.response.push({
          fulfilled,
          rejected
        });
      },
      eject: (name) => {
        if (inner.response[name]) {
          inner.response[name] = null;
        }
      }
    }
  },
  create(config) {
    inner.initConfig = components_common_tuiRequest_tuiBase.base.mergeConfig(components_common_tuiRequest_tuiBase.base.config(), config, true);
  },
  get(url, config = {}) {
    config.method = "GET";
    config.url = url || config.url || "";
    return http.request(config);
  },
  post(url, config = {}) {
    config.method = "POST";
    config.url = url || config.url || "";
    return http.request(config);
  },
  all(requests) {
    return Promise.all(requests);
  },
  request(config = {}) {
    let chain = [inner.dispatchRequest, void 0];
    let promise = Promise.resolve(config);
    inner.request.forEach((interceptor) => {
      chain.unshift(interceptor.fulfilled, interceptor.rejected);
    });
    inner.response.forEach((interceptor) => {
      chain.push(interceptor.fulfilled, interceptor.rejected);
    });
    while (chain.length) {
      promise = promise.then(chain.shift(), chain.shift());
    }
    return promise;
  }
};
exports.http = http;
