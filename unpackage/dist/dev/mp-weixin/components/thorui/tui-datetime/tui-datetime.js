"use strict";
const common_vendor = require("../../../common/vendor.js");
const _sfc_main = {
  name: "tuiDatetime",
  emits: ["cancel", "confirm"],
  props: {
    //0-年 1-日期+时间（年月日+时分） 2-日期(年月日) 3-日期(年月) 4-时间（时分） 5-时分秒 6-分秒 7-年月日 时分秒 8-年月日+小时
    type: {
      type: [Number, String],
      default: 1
    },
    //年份区间
    startYear: {
      type: Number,
      default: 1980
    },
    //年份区间
    endYear: {
      type: Number,
      default: 2050
    },
    hoursData: {
      type: Array,
      default() {
        return [];
      }
    },
    minutesData: {
      type: Array,
      default() {
        return [];
      }
    },
    secondsData: {
      type: Array,
      default() {
        return [];
      }
    },
    //显示标题
    title: {
      type: String,
      default: ""
    },
    //标题字体大小
    titleSize: {
      type: [Number, String],
      default: 34
    },
    //标题字体颜色
    titleColor: {
      type: String,
      default: "#333"
    },
    //"取消"字体颜色
    cancelColor: {
      type: String,
      default: "#888"
    },
    //"确定"字体颜色
    color: {
      type: String,
      default: ""
    },
    //设置默认显示日期 2019-08-01 || 2019-08-01 17:01 || 2019/08/01
    setDateTime: {
      type: String,
      default: ""
    },
    //单位置顶
    unitTop: {
      type: Boolean,
      default: false
    },
    //圆角设置
    radius: {
      type: Boolean,
      default: false
    },
    //头部背景色
    headerBackground: {
      type: String,
      default: "#fff"
    },
    //根据实际调整，不建议使用深颜色
    bodyBackground: {
      type: String,
      default: "#fff"
    },
    //单位置顶时，单位条背景色
    unitBackground: {
      type: String,
      default: "#fff"
    },
    height: {
      type: [Number, String],
      default: 520
    },
    //点击遮罩 是否可关闭
    maskClosable: {
      type: Boolean,
      default: true
    },
    zIndex: {
      type: [Number, String],
      default: 998
    }
  },
  data() {
    let immediate = true;
    return {
      immediate,
      isShow: false,
      years: [],
      months: [],
      days: [],
      hours: [],
      minutes: [],
      seconds: [],
      year: 0,
      month: 0,
      day: 0,
      hour: 0,
      minute: 0,
      second: 0,
      startDate: "",
      endDate: "",
      value: [],
      isEnd: true,
      firstShow: false
    };
  },
  mounted() {
    this.$nextTick(() => {
      setTimeout(() => {
        this.initData();
      }, 20);
    });
  },
  computed: {
    yearOrMonth() {
      return `${this.year}-${this.month}`;
    },
    propsChange() {
      return `${this.type}-${this.startYear}-${this.endYear}-${this.hoursData}-${this.minutesData}-${this.secondsData}`;
    },
    getColor() {
      return this.color || common_vendor.index && common_vendor.index.$tui && common_vendor.index.$tui.color.primary || "#5677fc";
    },
    getMaskZIndex() {
      return Number(this.zIndex) + 1;
    },
    getPickerZIndex() {
      return Number(this.zIndex) + 2;
    }
  },
  watch: {
    yearOrMonth() {
      this.setDays();
    },
    propsChange() {
      this.$nextTick(() => {
        setTimeout(() => {
          this.initData();
        }, 20);
      });
    },
    setDateTime(val) {
      if (val && val !== true) {
        setTimeout(() => {
          this.initData();
        }, 20);
      }
    }
  },
  methods: {
    stop() {
    },
    formatNum: function(num) {
      return num < 10 ? "0" + num : num + "";
    },
    generateArray: function(start, end) {
      return Array.from(new Array(end + 1).keys()).slice(start);
    },
    getIndex: function(arr, val) {
      if (!arr || arr.length === 0)
        return 0;
      let index = arr.indexOf(val);
      return ~index ? index : 0;
    },
    getCharCount(str) {
      let regex = new RegExp("/", "g");
      let result = str.match(regex);
      return !result ? 0 : result.length;
    },
    //日期时间处理
    initSelectValue() {
      let fdate = "";
      if (this.setDateTime && this.setDateTime !== true) {
        fdate = this.setDateTime.replace(/\-/g, "/");
        if (this.type == 3 && this.getCharCount(fdate) === 1) {
          fdate += "/01";
        } else if (this.type == 0) {
          fdate += "/01/01";
        }
        fdate = fdate && fdate.indexOf("/") == -1 ? `2023/01/01 ${fdate}` : fdate;
      }
      let time = null;
      if (fdate)
        time = new Date(fdate);
      else
        time = /* @__PURE__ */ new Date();
      this.year = time.getFullYear();
      this.month = time.getMonth() + 1;
      this.day = time.getDate();
      this.hour = time.getHours();
      this.minute = time.getMinutes();
      this.second = time.getSeconds();
    },
    initData() {
      this.initSelectValue();
      const type = Number(this.type);
      switch (type) {
        case 0:
          this.setYears();
          break;
        case 1:
          this.setYears();
          this.setMonths();
          this.setDays();
          this.setHours();
          this.setMinutes();
          break;
        case 2:
          this.setYears();
          this.setMonths();
          this.setDays();
          break;
        case 3:
          this.setYears();
          this.setMonths();
          break;
        case 4:
          this.setHours();
          this.setMinutes();
          break;
        case 5:
          this.setHours();
          this.setMinutes();
          this.setSeconds();
          break;
        case 6:
          this.setMinutes();
          this.setSeconds();
          break;
        case 7:
          this.setYears();
          this.setMonths();
          this.setDays();
          this.setHours();
          this.setMinutes();
          this.setSeconds();
          break;
        case 8:
          this.setYears();
          this.setMonths();
          this.setDays();
          this.setHours();
          break;
      }
      this.$nextTick(() => {
        setTimeout(() => {
          this.setDefaultValues();
        }, 0);
      });
    },
    setDefaultValues() {
      let vals = [];
      const year = this.getIndex(this.years, this.year);
      const month = this.getIndex(this.months, this.month);
      const day = this.getIndex(this.days, this.day);
      const hour = this.getIndex(this.hours, this.hour);
      const minute = this.getIndex(this.minutes, this.minute);
      const second = this.getIndex(this.seconds, this.second);
      const type = Number(this.type);
      switch (type) {
        case 0:
          vals = [year];
        case 1:
          vals = [year, month, day, hour, minute];
          break;
        case 2:
          vals = [year, month, day];
          break;
        case 3:
          vals = [year, month];
          break;
        case 4:
          vals = [hour, minute];
          break;
        case 5:
          vals = [hour, minute, second];
          break;
        case 6:
          vals = [minute, second];
          break;
        case 7:
          vals = [year, month, day, hour, minute, second];
          break;
        case 8:
          vals = [year, month, day, hour];
          break;
      }
      if (this.value.join(",") === vals.join(","))
        return;
      setTimeout(() => {
        this.value = vals;
      }, 200);
    },
    setYears() {
      this.years = this.generateArray(this.startYear, this.endYear);
    },
    setMonths() {
      this.months = this.generateArray(1, 12);
    },
    setDays() {
      if (this.type == 3 || this.type == 4)
        return;
      let totalDays = new Date(this.year, this.month, 0).getDate();
      totalDays = !totalDays || totalDays < 1 ? 1 : totalDays;
      this.days = this.generateArray(1, totalDays);
    },
    setHours() {
      if (this.hoursData && this.hoursData.length > 0) {
        this.hours = this.hoursData;
      } else {
        this.hours = this.generateArray(0, 23);
      }
    },
    setMinutes() {
      if (this.minutesData && this.minutesData.length > 0) {
        this.minutes = this.minutesData;
      } else {
        this.minutes = this.generateArray(0, 59);
      }
    },
    setSeconds() {
      if (this.secondsData && this.secondsData.length > 0) {
        this.seconds = this.secondsData;
      } else {
        this.seconds = this.generateArray(0, 59);
      }
    },
    show() {
      this.firstShow = true;
      setTimeout(() => {
        this.isShow = true;
      }, 50);
    },
    hide() {
      this.isShow = false;
      this.$emit("cancel", {});
    },
    maskClick() {
      if (!this.maskClosable)
        return;
      this.hide();
    },
    change(e) {
      if (!this.firstShow)
        return;
      this.value = e.detail.value;
      const type = Number(this.type);
      switch (type) {
        case 0:
          this.year = this.years[this.value[0]];
          break;
        case 1:
          this.year = this.years[this.value[0]];
          this.month = this.months[this.value[1]];
          this.day = this.days[this.value[2]];
          this.hour = this.hours[this.value[3]];
          this.minute = this.minutes[this.value[4]];
          break;
        case 2:
          this.year = this.years[this.value[0]];
          this.month = this.months[this.value[1]];
          this.day = this.days[this.value[2]];
          break;
        case 3:
          this.year = this.years[this.value[0]];
          this.month = this.months[this.value[1]];
          break;
        case 4:
          this.hour = this.hours[this.value[0]];
          this.minute = this.minutes[this.value[1]];
          break;
        case 5:
          this.hour = this.hours[this.value[0]];
          this.minute = this.minutes[this.value[1]];
          this.second = this.seconds[this.value[2]];
          break;
        case 6:
          this.minute = this.minutes[this.value[0]];
          this.second = this.seconds[this.value[1]];
          break;
        case 7:
          this.year = this.years[this.value[0]];
          this.month = this.months[this.value[1]];
          this.day = this.days[this.value[2]];
          this.hour = this.hours[this.value[3]];
          this.minute = this.minutes[this.value[4]];
          this.second = this.seconds[this.value[5]];
          break;
        case 8:
          this.year = this.years[this.value[0]];
          this.month = this.months[this.value[1]];
          this.day = this.days[this.value[2]];
          this.hour = this.hours[this.value[3]];
          break;
      }
      this.isEnd = true;
    },
    selectResult() {
      let result = {};
      let year = this.year;
      let month = this.formatNum(this.month || 0);
      let day = this.formatNum(this.day || 0);
      let hour = this.formatNum(this.hour || 0);
      let minute = this.formatNum(this.minute || 0);
      let second = this.formatNum(this.second || 0);
      const type = Number(this.type);
      switch (type) {
        case 0:
          result = {
            year,
            result: year
          };
          break;
        case 1:
          result = {
            year,
            month,
            day,
            hour,
            minute,
            result: `${year}-${month}-${day} ${hour}:${minute}`
          };
          break;
        case 2:
          result = {
            year,
            month,
            day,
            result: `${year}-${month}-${day}`
          };
          break;
        case 3:
          result = {
            year,
            month,
            result: `${year}-${month}`
          };
          break;
        case 4:
          result = {
            hour,
            minute,
            result: `${hour}:${minute}`
          };
          break;
        case 5:
          result = {
            hour,
            minute,
            second,
            result: `${hour}:${minute}:${second}`
          };
          break;
        case 6:
          result = {
            minute,
            second,
            result: `${minute}:${second}`
          };
          break;
        case 7:
          result = {
            year,
            month,
            day,
            hour,
            minute,
            second,
            result: `${year}-${month}-${day} ${hour}:${minute}:${second}`
          };
          break;
        case 8:
          result = {
            year,
            month,
            day,
            hour,
            result: `${year}-${month}-${day} ${hour}:00`
          };
          break;
      }
      this.$emit("confirm", result);
    },
    waitFix() {
      if (this.isEnd) {
        this.selectResult();
      } else {
        setTimeout(() => {
          this.waitFix();
        }, 50);
      }
    },
    btnFix() {
      setTimeout(() => {
        this.waitFix();
        this.hide();
      }, 80);
    },
    pickerstart() {
      this.isEnd = false;
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return common_vendor.e({
    a: $data.isShow ? 1 : "",
    b: $options.getMaskZIndex,
    c: common_vendor.o((...args) => $options.stop && $options.stop(...args)),
    d: common_vendor.o((...args) => $options.maskClick && $options.maskClick(...args)),
    e: $props.cancelColor,
    f: common_vendor.o((...args) => $options.hide && $options.hide(...args)),
    g: common_vendor.t($props.title),
    h: $props.titleSize + "rpx",
    i: $props.titleColor,
    j: $options.getColor,
    k: common_vendor.o((...args) => $options.btnFix && $options.btnFix(...args)),
    l: $props.radius ? 1 : "",
    m: $props.headerBackground,
    n: common_vendor.o((...args) => $options.stop && $options.stop(...args)),
    o: $props.unitTop
  }, $props.unitTop ? common_vendor.e({
    p: $props.type < 4 || $props.type == 7 || $props.type == 8
  }, $props.type < 4 || $props.type == 7 || $props.type == 8 ? {} : {}, {
    q: $props.type < 4 && $props.type > 0 || $props.type == 7 || $props.type == 8
  }, $props.type < 4 && $props.type > 0 || $props.type == 7 || $props.type == 8 ? {} : {}, {
    r: $props.type == 1 || $props.type == 2 || $props.type == 7 || $props.type == 8
  }, $props.type == 1 || $props.type == 2 || $props.type == 7 || $props.type == 8 ? {} : {}, {
    s: $props.type == 1 || $props.type == 4 || $props.type == 5 || $props.type == 7 || $props.type == 8
  }, $props.type == 1 || $props.type == 4 || $props.type == 5 || $props.type == 7 || $props.type == 8 ? {} : {}, {
    t: ($props.type == 1 || $props.type > 3) && $props.type != 8
  }, ($props.type == 1 || $props.type > 3) && $props.type != 8 ? {} : {}, {
    v: $props.type > 4 && $props.type != 8
  }, $props.type > 4 && $props.type != 8 ? {} : {}, {
    w: $props.unitBackground
  }) : {}, {
    x: $props.type < 4 || $props.type == 7 || $props.type == 8
  }, $props.type < 4 || $props.type == 7 || $props.type == 8 ? {
    y: common_vendor.f($data.years, (item, index, i0) => {
      return common_vendor.e({
        a: common_vendor.t(item)
      }, !$props.unitTop ? {} : {}, {
        b: index
      });
    }),
    z: !$props.unitTop,
    A: !$props.unitTop && $props.type == 7 ? 1 : ""
  } : {}, {
    B: $props.type < 4 && $props.type > 0 || $props.type == 7 || $props.type == 8
  }, $props.type < 4 && $props.type > 0 || $props.type == 7 || $props.type == 8 ? {
    C: common_vendor.f($data.months, (item, index, i0) => {
      return common_vendor.e({
        a: common_vendor.t($options.formatNum(item))
      }, !$props.unitTop ? {} : {}, {
        b: index
      });
    }),
    D: !$props.unitTop,
    E: !$props.unitTop && $props.type == 7 ? 1 : ""
  } : {}, {
    F: $props.type == 1 || $props.type == 2 || $props.type == 7 || $props.type == 8
  }, $props.type == 1 || $props.type == 2 || $props.type == 7 || $props.type == 8 ? {
    G: common_vendor.f($data.days, (item, index, i0) => {
      return common_vendor.e({
        a: common_vendor.t($options.formatNum(item))
      }, !$props.unitTop ? {} : {}, {
        b: index
      });
    }),
    H: !$props.unitTop,
    I: !$props.unitTop && $props.type == 7 ? 1 : ""
  } : {}, {
    J: $props.type == 1 || $props.type == 4 || $props.type == 5 || $props.type == 7 || $props.type == 8
  }, $props.type == 1 || $props.type == 4 || $props.type == 5 || $props.type == 7 || $props.type == 8 ? {
    K: common_vendor.f($data.hours, (item, index, i0) => {
      return common_vendor.e({
        a: common_vendor.t($options.formatNum(item))
      }, !$props.unitTop ? {} : {}, {
        b: index
      });
    }),
    L: !$props.unitTop,
    M: !$props.unitTop && $props.type == 7 ? 1 : ""
  } : {}, {
    N: ($props.type == 1 || $props.type > 3) && $props.type != 8
  }, ($props.type == 1 || $props.type > 3) && $props.type != 8 ? {
    O: common_vendor.f($data.minutes, (item, index, i0) => {
      return common_vendor.e({
        a: common_vendor.t($options.formatNum(item))
      }, !$props.unitTop ? {} : {}, {
        b: index
      });
    }),
    P: !$props.unitTop,
    Q: !$props.unitTop && $props.type == 7 ? 1 : ""
  } : {}, {
    R: $props.type > 4 && $props.type != 8
  }, $props.type > 4 && $props.type != 8 ? {
    S: common_vendor.f($data.seconds, (item, index, i0) => {
      return common_vendor.e({
        a: common_vendor.t($options.formatNum(item))
      }, !$props.unitTop ? {} : {}, {
        b: index
      });
    }),
    T: !$props.unitTop,
    U: !$props.unitTop && $props.type == 7 ? 1 : ""
  } : {}, {
    V: $props.type,
    W: $data.immediate,
    X: $data.value,
    Y: common_vendor.o((...args) => $options.change && $options.change(...args)),
    Z: common_vendor.o((...args) => $options.pickerstart && $options.pickerstart(...args)),
    aa: $props.bodyBackground,
    ab: $props.height + "rpx",
    ac: $data.isShow ? 1 : "",
    ad: $options.getPickerZIndex,
    ae: $props.zIndex
  });
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-bd7b091c"], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/components/thorui/tui-datetime/tui-datetime.vue"]]);
wx.createComponent(Component);
