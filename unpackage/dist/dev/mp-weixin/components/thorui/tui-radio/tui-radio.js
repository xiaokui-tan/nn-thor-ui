"use strict";
const common_vendor = require("../../../common/vendor.js");
const _sfc_main = {
  name: "tui-radio",
  emits: ["change"],
  behaviors: ["uni://form-field"],
  options: {
    virtualHost: true
  },
  props: {
    value: {
      type: String,
      default: ""
    },
    checked: {
      type: Boolean,
      default: false
    },
    disabled: {
      type: Boolean,
      default: false
    },
    //radio选中背景颜色
    color: {
      type: String,
      default: ""
    },
    //radio未选中时边框颜色
    borderColor: {
      type: String,
      default: "#ccc"
    },
    //是否只展示对号，无边框背景
    isCheckMark: {
      type: Boolean,
      default: false
    },
    //对号颜色
    checkMarkColor: {
      type: String,
      default: "#fff"
    },
    scaleRatio: {
      type: [Number, String],
      default: 1
    }
  },
  computed: {
    getColor() {
      return this.color || common_vendor.index && common_vendor.index.$tui && common_vendor.index.$tui.color.primary || "#5677fc";
    }
  },
  created() {
    this.val = this.checked;
    this.group = this.getParent();
    if (this.group) {
      this.group.childrens.push(this);
      if (this.group.value) {
        this.val = this.value === this.group.value;
      }
      if (this.group.modelValue) {
        this.val = this.value === this.group.modelValue;
      }
    }
    this.label = this.getParent("tui-label");
    if (this.label) {
      this.label.childrens.push(this);
    }
  },
  watch: {
    checked(newVal) {
      this.val = newVal;
    },
    val(newVal) {
      if (newVal && this.group) {
        this.group.changeValue(this.value, this);
      }
    }
  },
  data() {
    let nvue = false;
    return {
      val: false,
      nvue
    };
  },
  methods: {
    getBackgroundStyle(val, isCheckMark) {
      let color = val ? this.getColor : "#fff";
      if (isCheckMark) {
        color = "transparent";
      }
      return color;
    },
    getBorderStyle(val, isCheckMark) {
      let color = val ? this.getColor : this.borderColor;
      if (isCheckMark) {
        color = "transparent";
      }
      return `1px solid ${color}`;
    },
    radioChange(e) {
      if (this.disabled || this.val)
        return;
      this.val = true;
      this.$emit("change", {
        checked: this.val,
        value: this.value
      });
    },
    getParent(name = "tui-radio-group") {
      let parent = this.$parent;
      let parentName = parent.$options.name;
      while (parentName !== name) {
        parent = parent.$parent;
        if (!parent)
          return false;
        parentName = parent.$options.name;
      }
      return parent;
    },
    labelClick() {
      this.radioChange();
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return common_vendor.e({
    a: $data.val
  }, $data.val ? {
    b: $props.checkMarkColor,
    c: $props.checkMarkColor
  } : {}, {
    d: $options.getColor,
    e: $props.disabled,
    f: $props.value,
    g: $data.val,
    h: $props.disabled ? 1 : "",
    i: $options.getBackgroundStyle($data.val, $props.isCheckMark),
    j: $options.getBorderStyle($data.val, $props.isCheckMark),
    k: $data.nvue ? 1 : $props.scaleRatio,
    l: `scale(${$data.nvue ? $props.scaleRatio : 1})`,
    m: common_vendor.o((...args) => $options.radioChange && $options.radioChange(...args))
  });
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-999d2439"], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/components/thorui/tui-radio/tui-radio.vue"]]);
wx.createComponent(Component);
