"use strict";
const common_vendor = require("../../../common/vendor.js");
const _sfc_main = {
  name: "tuiSearchbar",
  emits: ["clear", "focus", "blur", "click", "cancel", "input", "search"],
  props: {
    //搜索栏背景色
    backgroundColor: {
      type: String,
      default: "#ededed"
    },
    //搜索栏padding
    padding: {
      type: String,
      default: "16rpx 20rpx"
    },
    //input框高度
    height: {
      type: String,
      default: "72rpx"
    },
    radius: {
      type: String,
      default: "8rpx"
    },
    //input框背景色
    inputBgColor: {
      type: String,
      default: "#fff"
    },
    focus: {
      type: Boolean,
      default: false
    },
    placeholder: {
      type: String,
      default: "请输入搜索关键词"
    },
    value: {
      type: String,
      default: ""
    },
    //input是否禁用
    disabled: {
      type: Boolean,
      default: false
    },
    cancelText: {
      type: String,
      default: "取消"
    },
    cancelColor: {
      type: String,
      default: "#888"
    },
    cancel: {
      type: Boolean,
      default: true
    },
    searchText: {
      type: String,
      default: "搜索"
    },
    searchColor: {
      type: String,
      default: ""
    },
    //是否显示占位标签
    showLabel: {
      type: Boolean,
      default: true
    },
    //是否显示输入框
    showInput: {
      type: Boolean,
      default: true
    }
  },
  computed: {
    getSearchColor() {
      return this.searchColor || common_vendor.index && common_vendor.index.$tui && common_vendor.index.$tui.color.primary || "#5677fc";
    }
  },
  created() {
    this.valueTxt = this.value;
    this.isFocus = this.focus;
    if (this.focus || this.valueTxt.length > 0) {
      this.searchState = true;
    }
    this.clearSize = Math.ceil(common_vendor.index.upx2px(30));
    this.searchSize = Math.ceil(common_vendor.index.upx2px(26));
  },
  watch: {
    value(val) {
      this.valueTxt = val;
    }
  },
  data() {
    return {
      searchState: false,
      isFocus: false,
      valueTxt: "",
      clearSize: 15,
      searchSize: 13
    };
  },
  methods: {
    clearInput() {
      this.valueTxt = "";
      this.isFocus = false;
      common_vendor.index.hideKeyboard();
      this.$emit("clear");
    },
    inputFocus(e) {
      if (!this.showLabel) {
        this.searchState = true;
      }
      this.$emit("focus", e.detail);
    },
    inputBlur(e) {
      this.isFocus = false;
      this.$emit("blur", e.detail);
    },
    tapShowInput() {
      if (!this.disabled && this.showInput) {
        this.searchState = true;
        setTimeout(() => {
          this.isFocus = true;
        }, 20);
      }
      this.$emit("click", {});
    },
    hideInput() {
      this.searchState = false;
      this.isFocus = false;
      common_vendor.index.hideKeyboard();
      this.$emit("cancel", {});
    },
    inputChange(e) {
      this.valueTxt = e.detail.value;
      this.$emit("input", e.detail);
    },
    search() {
      this.$emit("search", {
        value: this.valueTxt
      });
    },
    reset() {
      this.searchState = false;
      this.isFocus = false;
      this.valueTxt = "";
      common_vendor.index.hideKeyboard();
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return common_vendor.e({
    a: $props.showInput
  }, $props.showInput ? common_vendor.e({
    b: $data.searchSize,
    c: $props.placeholder,
    d: $data.valueTxt,
    e: $data.isFocus,
    f: $props.disabled,
    g: common_vendor.o((...args) => $options.inputBlur && $options.inputBlur(...args)),
    h: common_vendor.o((...args) => $options.inputFocus && $options.inputFocus(...args)),
    i: common_vendor.o((...args) => $options.inputChange && $options.inputChange(...args)),
    j: common_vendor.o((...args) => $options.search && $options.search(...args)),
    k: $data.valueTxt.length > 0 && !$props.disabled
  }, $data.valueTxt.length > 0 && !$props.disabled ? {
    l: $data.clearSize,
    m: common_vendor.o((...args) => $options.clearInput && $options.clearInput(...args))
  } : {}, {
    n: !$data.isFocus && !$data.searchState && $props.showLabel && !$props.disabled ? 1 : "",
    o: $props.height,
    p: $props.radius,
    q: $props.inputBgColor
  }) : {}, {
    r: !$data.isFocus && !$data.searchState && $props.showLabel
  }, !$data.isFocus && !$data.searchState && $props.showLabel ? {
    s: $data.searchSize,
    t: common_vendor.t($props.placeholder),
    v: $props.height,
    w: $props.radius,
    x: $props.inputBgColor,
    y: common_vendor.o((...args) => $options.tapShowInput && $options.tapShowInput(...args))
  } : {}, {
    z: $props.height,
    A: $props.cancel && $data.searchState && !$data.valueTxt
  }, $props.cancel && $data.searchState && !$data.valueTxt ? {
    B: common_vendor.t($props.cancelText),
    C: $props.cancelColor,
    D: common_vendor.o((...args) => $options.hideInput && $options.hideInput(...args))
  } : {}, {
    E: $data.valueTxt && !$props.disabled && $data.searchState
  }, $data.valueTxt && !$props.disabled && $data.searchState ? {
    F: common_vendor.t($props.searchText),
    G: $options.getSearchColor,
    H: common_vendor.o((...args) => $options.search && $options.search(...args))
  } : {}, {
    I: $props.backgroundColor,
    J: $props.padding
  });
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-7cf4891b"], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/components/thorui/tui-searchbar/tui-searchbar.vue"]]);
wx.createComponent(Component);
