"use strict";
const common_vendor = require("../../../common/vendor.js");
const _sfc_main = {
  name: "tuiBottomPopup",
  emits: ["close"],
  props: {
    //是否需要mask
    mask: {
      type: Boolean,
      default: true
    },
    //控制显示
    show: {
      type: Boolean,
      default: false
    },
    //背景颜色
    backgroundColor: {
      type: String,
      default: "#fff"
    },
    //高度 rpx
    height: {
      type: Number,
      default: 0
    },
    //设置圆角
    radius: {
      type: Boolean,
      default: true
    },
    zIndex: {
      type: [Number, String],
      default: 997
    },
    maskZIndex: {
      type: [Number, String],
      default: 996
    },
    //弹层显示时，垂直方向移动的距离
    translateY: {
      type: String,
      default: "0"
    },
    //是否需要判断底部安全区域（主要针对iphonex以上机型）
    isSafeArea: {
      type: Boolean,
      default: true
    }
  },
  methods: {
    handleClose() {
      if (!this.show) {
        return;
      }
      this.$emit("close", {});
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return common_vendor.e({
    a: $props.show ? 1 : "",
    b: $props.radius ? 1 : "",
    c: $props.isSafeArea ? 1 : "",
    d: $props.backgroundColor,
    e: $props.height ? $props.height + "rpx" : "auto",
    f: $props.zIndex,
    g: `translate3d(0, ${$props.show ? $props.translateY : "100%"}, 0)`,
    h: $props.mask
  }, $props.mask ? {
    i: common_vendor.n($props.show ? "tui-mask-show" : ""),
    j: $props.maskZIndex,
    k: common_vendor.o((...args) => $options.handleClose && $options.handleClose(...args))
  } : {}, {
    l: common_vendor.o(() => {
    })
  });
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-80e2c574"], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/components/thorui/tui-bottom-popup/tui-bottom-popup.vue"]]);
wx.createComponent(Component);
