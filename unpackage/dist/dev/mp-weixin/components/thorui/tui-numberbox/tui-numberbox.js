"use strict";
const common_vendor = require("../../../common/vendor.js");
const _sfc_main = {
  name: "tuiNumberbox",
  emits: ["change"],
  props: {
    value: {
      type: [Number, String],
      default: 1
    },
    //最小值
    min: {
      type: Number,
      default: 1
    },
    //最大值
    max: {
      type: Number,
      default: 99
    },
    //迈步大小 1 1.1 10...
    step: {
      type: Number,
      default: 1
    },
    //是否禁用操作
    disabled: {
      type: Boolean,
      default: false
    },
    iconBgColor: {
      type: String,
      default: "transparent"
    },
    radius: {
      type: String,
      default: "50%"
    },
    //加减图标大小 rpx
    iconSize: {
      type: Number,
      default: 22
    },
    iconColor: {
      type: String,
      default: "#666666"
    },
    //input 高度
    height: {
      type: Number,
      default: 42
    },
    //input 宽度
    width: {
      type: Number,
      default: 80
    },
    size: {
      type: Number,
      default: 28
    },
    //input 背景颜色
    backgroundColor: {
      type: String,
      default: "#F5F5F5"
    },
    //input 字体颜色
    color: {
      type: String,
      default: "#333"
    },
    //索引值，列表中使用
    index: {
      type: [Number, String],
      default: 0
    },
    //自定义参数
    custom: {
      type: [Number, String],
      default: 0
    }
  },
  created() {
    this.inputValue = +this.value;
  },
  data() {
    return {
      inputValue: 0
    };
  },
  watch: {
    value(val) {
      this.inputValue = +val;
    }
  },
  methods: {
    getScale(val, step) {
      let scale = 1;
      let scaleVal = 1;
      if (!Number.isInteger(step)) {
        scale = Math.pow(10, (step + "").split(".")[1].length);
      }
      if (!Number.isInteger(val)) {
        scaleVal = Math.pow(10, (val + "").split(".")[1].length);
      }
      return Math.max(scale, scaleVal);
    },
    calcNum: function(type) {
      if (this.disabled || this.inputValue == this.min && type === "reduce" || this.inputValue == this.max && type === "plus") {
        return;
      }
      const scale = this.getScale(Number(this.inputValue), Number(this.step));
      let num = Number(this.inputValue) * scale;
      let step = this.step * scale;
      if (type === "reduce") {
        num -= step;
      } else if (type === "plus") {
        num += step;
      }
      let value = Number((num / scale).toFixed(String(scale).length - 1));
      if (value < this.min) {
        value = this.min;
      } else if (value > this.max) {
        value = this.max;
      }
      this.handleChange(value, type);
    },
    plus: function() {
      this.calcNum("plus");
    },
    reduce: function() {
      this.calcNum("reduce");
    },
    blur: function(e) {
      let value = e.detail.value;
      if (value) {
        if (~value.indexOf(".") && Number.isInteger(this.step) && Number.isInteger(Number(value))) {
          value = value.split(".")[0];
        }
        value = Number(value);
        if (value > this.max) {
          value = this.max;
        } else if (value < this.min) {
          value = this.min;
        }
      } else {
        value = this.min;
      }
      if (value == this.value && value != this.inputValue || !e.detail.value) {
        this.inputValue = value;
      }
      this.handleChange(value, "blur");
    },
    handleChange(value, type) {
      if (this.disabled)
        return;
      this.$emit("change", {
        value: Number(value),
        type,
        index: this.index,
        custom: this.custom
      });
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: $props.iconColor,
    b: $props.iconSize + "rpx",
    c: $props.iconSize + "rpx",
    d: $props.iconBgColor,
    e: $props.radius,
    f: common_vendor.o((...args) => $options.reduce && $options.reduce(...args)),
    g: common_vendor.n($props.disabled || $props.min >= $data.inputValue ? "tui-disabled" : ""),
    h: $props.disabled,
    i: common_vendor.o((...args) => $options.blur && $options.blur(...args)),
    j: $props.color,
    k: $props.size + "rpx",
    l: $props.backgroundColor,
    m: $props.height + "rpx",
    n: $props.height + "rpx",
    o: $props.width + "rpx",
    p: $data.inputValue,
    q: common_vendor.o(($event) => $data.inputValue = $event.detail.value),
    r: $props.iconColor,
    s: $props.iconSize + "rpx",
    t: $props.iconSize + "rpx",
    v: $props.iconBgColor,
    w: $props.radius,
    x: common_vendor.o((...args) => $options.plus && $options.plus(...args)),
    y: common_vendor.n($props.disabled || $data.inputValue >= $props.max ? "tui-disabled" : "")
  };
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-fd01c0e4"], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/components/thorui/tui-numberbox/tui-numberbox.vue"]]);
wx.createComponent(Component);
