"use strict";
const common_vendor = require("../../../common/vendor.js");
const _sfc_main = {
  name: "tui-form-button",
  emits: ["click", "getuserinfo", "contact", "getphonenumber", "error", "opensetting", "chooseavatar", "launchapp"],
  behaviors: ["wx://form-field-button"],
  props: {
    //按钮背景色
    background: {
      type: String,
      default: ""
    },
    //按钮显示文本
    text: {
      type: String,
      default: ""
    },
    //按钮字体颜色
    color: {
      type: String,
      default: ""
    },
    //按钮禁用背景色
    disabledBackground: {
      type: String,
      default: ""
    },
    //按钮禁用字体颜色
    disabledColor: {
      type: String,
      default: ""
    },
    borderWidth: {
      type: String,
      default: "1rpx"
    },
    borderColor: {
      type: String,
      default: ""
    },
    //宽度
    width: {
      type: String,
      default: "100%"
    },
    //高度
    height: {
      type: String,
      default: ""
    },
    //medium 368*80 / small 240*80/ mini 116*64
    btnSize: {
      type: String,
      default: ""
    },
    //字体大小，单位rpx
    size: {
      type: [Number, String],
      default: 0
    },
    bold: {
      type: Boolean,
      default: false
    },
    margin: {
      type: String,
      default: "0"
    },
    //圆角
    radius: {
      type: String,
      default: ""
    },
    plain: {
      type: Boolean,
      default: false
    },
    link: {
      type: Boolean,
      default: false
    },
    disabled: {
      type: Boolean,
      default: false
    },
    loading: {
      type: Boolean,
      default: false
    },
    formType: {
      type: String,
      default: ""
    },
    openType: {
      type: String,
      default: ""
    },
    //支付宝小程序 
    //当 open-type 为 getAuthorize 时，可以设置 scope 为：phoneNumber、userInfo
    scope: {
      type: String,
      default: ""
    },
    appParameter: {
      type: String,
      default: ""
    },
    index: {
      type: [Number, String],
      default: 0
    }
  },
  computed: {
    getWidth() {
      let width = this.width;
      if (this.btnSize && this.btnSize !== true) {
        width = {
          "medium": "368rpx",
          "small": "240rpx",
          "mini": "116rpx"
        }[this.btnSize] || this.width;
      }
      return width;
    },
    getHeight() {
      let height = this.height || common_vendor.index && common_vendor.index.$tui && common_vendor.index.$tui.tuiFormButton.height || "96rpx";
      if (this.btnSize && this.btnSize !== true) {
        height = {
          "medium": "80rpx",
          "small": "80rpx",
          "mini": "64rpx"
        }[this.btnSize] || height;
      }
      return height;
    },
    getBackground() {
      return this.background || common_vendor.index && common_vendor.index.$tui && common_vendor.index.$tui.tuiFormButton.background || "#5677fc";
    },
    getColor() {
      return this.color || common_vendor.index && common_vendor.index.$tui && common_vendor.index.$tui.tuiFormButton.color || "#fff";
    },
    getRadius() {
      return this.radius || common_vendor.index && common_vendor.index.$tui && common_vendor.index.$tui.tuiFormButton.radius || "6rpx";
    },
    getSize() {
      return this.size || common_vendor.index && common_vendor.index.$tui && common_vendor.index.$tui.tuiFormButton.size || 32;
    }
  },
  data() {
    return {
      time: 0,
      trigger: false,
      tap: false
    };
  },
  methods: {
    handleStart() {
      if (this.disabled)
        return;
      this.trigger = false;
      this.tap = true;
      if ((/* @__PURE__ */ new Date()).getTime() - this.time <= 150)
        return;
      this.trigger = true;
      this.time = (/* @__PURE__ */ new Date()).getTime();
    },
    handleClick() {
      if (this.disabled || !this.trigger)
        return;
      this.time = 0;
    },
    handleTap() {
      if (this.disabled)
        return;
      this.$emit("click", {
        index: Number(this.index)
      });
    },
    handleEnd() {
      if (this.disabled)
        return;
      setTimeout(() => {
        this.time = 0;
      }, 150);
    },
    bindgetuserinfo({
      detail = {}
    } = {}) {
      this.$emit("getuserinfo", detail);
    },
    bindcontact({
      detail = {}
    } = {}) {
      this.$emit("contact", detail);
    },
    bindgetphonenumber({
      detail = {}
    } = {}) {
      this.$emit("getphonenumber", detail);
    },
    binderror({
      detail = {}
    } = {}) {
      this.$emit("error", detail);
    },
    bindopensetting({
      detail = {}
    } = {}) {
      this.$emit("opensetting", detail);
    },
    bindchooseavatar({
      detail = {}
    } = {}) {
      this.$emit("chooseavatar", detail);
    },
    bindlaunchapp({
      detail = {}
    } = {}) {
      this.$emit("launchapp", detail);
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return common_vendor.e({
    a: $props.text
  }, $props.text ? {
    b: common_vendor.t($props.text),
    c: $props.bold ? 1 : "",
    d: $options.getSize + "rpx",
    e: $options.getSize + "rpx",
    f: $props.disabled && $props.disabledBackground ? $props.disabledColor : $options.getColor
  } : {}, {
    g: common_vendor.n($props.bold ? "tui-text__bold" : ""),
    h: common_vendor.n($data.time && ($props.plain || $props.link) ? "tui-button__opacity" : ""),
    i: common_vendor.n($props.disabled && !$props.disabledBackground ? "tui-button__opacity" : ""),
    j: common_vendor.n((!$props.width || $props.width === "100%" || $props.width === true) && (!$props.btnSize || $props.btnSize === true) ? "tui-button__flex-1" : ""),
    k: common_vendor.n($data.time && !$props.plain && !$props.link ? "tui-button__active" : ""),
    l: $options.getWidth,
    m: $options.getHeight,
    n: $options.getHeight,
    o: $props.disabled && $props.disabledBackground ? $props.disabledBackground : $props.plain ? "transparent" : $options.getBackground,
    p: $props.borderWidth,
    q: $props.borderColor ? $props.borderColor : $props.disabled && $props.disabledBackground ? $props.disabledBackground : $props.link ? "transparent" : $options.getBackground,
    r: $options.getRadius,
    s: $options.getSize + "rpx",
    t: $props.disabled && $props.disabledBackground ? $props.disabledColor : $options.getColor,
    v: $props.loading,
    w: $props.formType,
    x: $props.openType,
    y: $props.appParameter,
    z: common_vendor.o((...args) => $options.bindgetuserinfo && $options.bindgetuserinfo(...args)),
    A: common_vendor.o((...args) => $options.bindgetphonenumber && $options.bindgetphonenumber(...args)),
    B: common_vendor.o((...args) => $options.bindcontact && $options.bindcontact(...args)),
    C: common_vendor.o((...args) => $options.binderror && $options.binderror(...args)),
    D: common_vendor.o((...args) => $options.bindopensetting && $options.bindopensetting(...args)),
    E: common_vendor.o((...args) => $options.bindchooseavatar && $options.bindchooseavatar(...args)),
    F: common_vendor.o((...args) => $options.bindlaunchapp && $options.bindlaunchapp(...args)),
    G: $props.disabled,
    H: $props.scope,
    I: common_vendor.o((...args) => $options.handleTap && $options.handleTap(...args)),
    J: $options.getWidth,
    K: $props.height,
    L: $props.margin,
    M: $options.getRadius,
    N: common_vendor.o((...args) => $options.handleStart && $options.handleStart(...args)),
    O: common_vendor.o((...args) => $options.handleClick && $options.handleClick(...args)),
    P: common_vendor.o((...args) => $options.handleEnd && $options.handleEnd(...args))
  });
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-fc40545d"], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/components/thorui/tui-form-button/tui-form-button.vue"]]);
wx.createComponent(Component);
