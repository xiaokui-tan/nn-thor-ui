"use strict";
const common_vendor = require("../../../common/vendor.js");
const _sfc_main = {
  name: "tui-radio-group",
  emits: ["change", "input", "update:modelValue"],
  behaviors: ["wx://form-field-group"],
  options: {
    virtualHost: true
  },
  props: {
    name: {
      type: String,
      default: ""
    },
    modelValue: {
      type: String,
      default: ""
    },
    value: {
      type: String,
      default: ""
    }
  },
  data() {
    return {
      val: ""
    };
  },
  watch: {
    modelValue(val) {
      this.modelChange(val);
    },
    value(val) {
      this.modelChange(val);
    }
  },
  created() {
    this.childrens = [];
  },
  methods: {
    radioChange(e) {
      this.$emit("change", e);
      this.$emit("input", e.detail.value);
      this.$emit("update:modelValue", e.detail.value);
    },
    changeValue(value, target) {
      if (this.val === value)
        return;
      this.val = value;
      this.childrens.forEach((item) => {
        if (item !== target) {
          item.val = false;
        }
      });
      let e = {
        detail: {
          value
        }
      };
      this.radioChange(e);
    },
    modelChange(value) {
      this.childrens.forEach((item) => {
        if (item.value === value) {
          item.val = true;
        } else {
          item.val = false;
        }
      });
    }
  }
};
if (!Array) {
  const _easycom_tui_form_field2 = common_vendor.resolveComponent("tui-form-field");
  _easycom_tui_form_field2();
}
const _easycom_tui_form_field = () => "../tui-form-field/tui-form-field.js";
if (!Math) {
  _easycom_tui_form_field();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.o(($event) => $data.val = $event),
    b: common_vendor.p({
      name: $props.name,
      modelValue: $data.val
    })
  };
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/components/thorui/tui-radio-group/tui-radio-group.vue"]]);
wx.createComponent(Component);
