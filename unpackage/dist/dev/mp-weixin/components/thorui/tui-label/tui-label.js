"use strict";
const common_vendor = require("../../../common/vendor.js");
const _sfc_main = {
  name: "tui-label",
  props: {
    padding: {
      type: String,
      default: "0"
    },
    margin: {
      type: String,
      default: "0"
    },
    isFull: {
      type: Boolean,
      default: false
    }
  },
  created() {
    this.childrens = [];
  },
  methods: {
    onClick() {
      if (this.childrens && this.childrens.length > 0) {
        for (let child of this.childrens) {
          child.labelClick();
        }
      }
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: $props.isFull ? 1 : "",
    b: $props.padding,
    c: $props.margin,
    d: common_vendor.o((...args) => $options.onClick && $options.onClick(...args))
  };
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-b75e1699"], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/components/thorui/tui-label/tui-label.vue"]]);
wx.createComponent(Component);
