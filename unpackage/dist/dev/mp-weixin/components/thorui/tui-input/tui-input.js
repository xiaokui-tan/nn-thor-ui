"use strict";
const common_vendor = require("../../../common/vendor.js");
const _sfc_main = {
  name: "tui-input",
  emits: ["input", "update:modelValue", "focus", "blur", "confirm", "click", "keyboardheightchange"],
  //这里加group是为了避免在表单中使用时给组件加value属性
  behaviors: ["wx://form-field-group"],
  options: {
    addGlobalClass: true,
    virtualHost: true
  },
  props: {
    //是否为必填项
    required: {
      type: Boolean,
      default: false
    },
    requiredColor: {
      type: String,
      default: ""
    },
    //左侧标题
    label: {
      type: String,
      default: ""
    },
    //标题字体大小
    labelSize: {
      type: [Number, String],
      default: 0
    },
    labelColor: {
      type: String,
      default: ""
    },
    //label 最小宽度 rpx
    labelWidth: {
      type: Number,
      default: 140
    },
    clearable: {
      type: Boolean,
      default: false
    },
    //px
    clearSize: {
      type: Number,
      default: 15
    },
    clearColor: {
      type: String,
      default: "#bfbfbf"
    },
    //获取焦点
    focus: {
      type: Boolean,
      default: false
    },
    placeholder: {
      type: String,
      default: ""
    },
    placeholderStyle: {
      type: String,
      default: ""
    },
    //输入框名称
    name: {
      type: String,
      default: ""
    },
    //输入框值
    value: {
      type: [Number, String],
      default: ""
    },
    //输入框值
    modelValue: {
      type: [Number, String],
      default: ""
    },
    modelModifiers: {
      default: () => ({})
    },
    //与官方input type属性一致
    type: {
      type: String,
      default: "text"
    },
    password: {
      type: Boolean,
      default: false
    },
    disabled: {
      type: Boolean,
      default: false
    },
    maxlength: {
      type: [Number, String],
      default: 140
    },
    min: {
      type: [Number, String],
      default: "NaN"
    },
    max: {
      type: [Number, String],
      default: "NaN"
    },
    cursorSpacing: {
      type: Number,
      default: 0
    },
    confirmType: {
      type: String,
      default: "done"
    },
    confirmHold: {
      type: Boolean,
      default: false
    },
    cursor: {
      type: Number,
      default: -1
    },
    selectionStart: {
      type: Number,
      default: -1
    },
    selectionEnd: {
      type: Number,
      default: -1
    },
    adjustPosition: {
      type: Boolean,
      default: true
    },
    holdKeyboard: {
      type: Boolean,
      default: false
    },
    autoBlur: {
      type: Boolean,
      default: false
    },
    //输入框字体大小 rpx
    size: {
      type: [Number, String],
      default: 0
    },
    //输入框字体颜色
    color: {
      type: String,
      default: ""
    },
    // 是否显示 input 边框
    inputBorder: {
      type: Boolean,
      default: false
    },
    borderColor: {
      type: String,
      default: "rgba(0, 0, 0, 0.1)"
    },
    //input是否显示为圆角
    isFillet: {
      type: Boolean,
      default: false
    },
    // 是否显示上边框
    borderTop: {
      type: Boolean,
      default: false
    },
    // 是否显示下边框
    borderBottom: {
      type: Boolean,
      default: true
    },
    //下边框线条是否有左偏移距离
    lineLeft: {
      type: Boolean,
      default: true
    },
    // 是否自动去除两端的空格
    trim: {
      type: Boolean,
      default: true
    },
    textRight: {
      type: Boolean,
      default: false
    },
    //输入框padding值
    padding: {
      type: String,
      default: ""
    },
    //输入框背景颜色
    backgroundColor: {
      type: String,
      default: ""
    },
    radius: {
      type: [Number, String],
      default: -1
    },
    //输入框margin-top值 rpx
    marginTop: {
      type: [Number, String],
      default: 0
    }
  },
  computed: {
    getLabelSize() {
      return this.labelSize || common_vendor.index && common_vendor.index.$tui && common_vendor.index.$tui.tuiInput.labelSize || 32;
    },
    getLabelColor() {
      return this.labelColor || common_vendor.index && common_vendor.index.$tui && common_vendor.index.$tui.tuiInput.labelColor || "#333";
    },
    getSize() {
      return this.size || common_vendor.index && common_vendor.index.$tui && common_vendor.index.$tui.tuiInput.size || 32;
    },
    getColor() {
      return this.color || common_vendor.index && common_vendor.index.$tui && common_vendor.index.$tui.tuiInput.color || "#333";
    },
    getRadius() {
      let radius = this.radius;
      if (radius === -1 || radius === true) {
        radius = common_vendor.index && common_vendor.index.$tui && common_vendor.index.$tui.tuiInput.radius;
      }
      return Number(radius || 0);
    },
    getStyles() {
      const padding = this.padding || common_vendor.index && common_vendor.index.$tui && common_vendor.index.$tui.tuiInput.padding || "26rpx 30rpx";
      const bgColor = this.backgroundColor || common_vendor.index && common_vendor.index.$tui && common_vendor.index.$tui.tuiInput.backgroundColor || "#FFFFFF";
      let radius = this.getRadius;
      let styles = `padding:${padding};background:${bgColor};margin-top:${this.marginTop}rpx;`;
      if (radius && radius !== true && radius !== -1) {
        styles += `border-radius:${radius}rpx;`;
      }
      if (this.borderTop || this.borderBottom || this.inputBorder) {
        styles += `border-color:${this.borderColor};`;
      }
      return styles;
    },
    getRequiredColor() {
      return this.requiredColor || common_vendor.index && common_vendor.index.$tui && common_vendor.index.$tui.tuiInput.requiredColor || "#EB0909";
    }
  },
  data() {
    return {
      placeholderStyl: "",
      focused: false,
      inputVal: ""
    };
  },
  watch: {
    focus(val) {
      this.$nextTick(() => {
        setTimeout(() => {
          this.focused = val;
        }, 50);
      });
    },
    placeholderStyle() {
      this.fieldPlaceholderStyle();
    },
    modelValue(newVal) {
      this.inputVal = newVal;
    },
    value(newVal) {
      this.inputVal = newVal;
    }
  },
  created() {
    this.fieldPlaceholderStyle();
    setTimeout(() => {
      if (this.value && !this.modelValue) {
        this.inputVal = this.value;
      } else {
        this.inputVal = this.modelValue;
      }
    }, 50);
  },
  mounted() {
    this.$nextTick(() => {
      setTimeout(() => {
        this.focused = this.focus;
      }, 300);
    });
  },
  methods: {
    fieldPlaceholderStyle() {
      if (this.placeholderStyle) {
        this.placeholderStyl = this.placeholderStyle;
      } else {
        const size = common_vendor.index.upx2px(this.size || common_vendor.index && common_vendor.index.$tui && common_vendor.index.$tui.tuiInput.size || 32);
        this.placeholderStyl = `font-size:${size}px`;
      }
    },
    onInput(event) {
      let value = event.detail.value;
      if (this.trim)
        value = this.trimStr(value);
      this.inputVal = value;
      const cVal = Number(value);
      if ((this.modelModifiers.number || this.type === "digit" || this.type === "number") && !isNaN(cVal) && Number.isSafeInteger(cVal)) {
        let eVal = this.type === "digit" ? value : cVal;
        if (typeof cVal === "number") {
          const min = Number(this.min);
          const max = Number(this.max);
          if (typeof min === "number" && cVal < min) {
            eVal = min;
          } else if (typeof max === "number" && max < cVal) {
            eVal = max;
          }
        }
        value = isNaN(eVal) ? value : eVal;
      }
      this.$nextTick(() => {
        event.detail.value !== "" && (this.inputVal = value);
      });
      const inputValue = event.detail.value !== "" ? value : "";
      this.$emit("input", inputValue);
      this.$emit("update:modelValue", inputValue);
    },
    onFocus(event) {
      this.$emit("focus", event);
    },
    onBlur(event) {
      this.$emit("blur", event);
    },
    onConfirm(e) {
      this.$emit("confirm", e);
    },
    onClear(event) {
      if (this.disabled)
        return;
      common_vendor.index.hideKeyboard();
      this.inputVal = "";
      this.$emit("input", "");
      this.$emit("update:modelValue", "");
    },
    fieldClick() {
      this.$emit("click", {
        name: this.name
      });
    },
    onKeyboardheightchange(e) {
      this.$emit("keyboardheightchange", e.detail);
    },
    trimStr(str) {
      return str.replace(/^\s+|\s+$/g, "");
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return common_vendor.e({
    a: $props.borderTop && !$props.inputBorder
  }, $props.borderTop && !$props.inputBorder ? {
    b: $props.borderColor
  } : {}, {
    c: $props.borderBottom && !$props.inputBorder
  }, $props.borderBottom && !$props.inputBorder ? {
    d: $props.lineLeft ? 1 : "",
    e: $props.borderColor
  } : {}, {
    f: $props.inputBorder
  }, $props.inputBorder ? {
    g: $props.isFillet && !$options.getRadius ? 1 : "",
    h: $props.borderColor,
    i: $options.getRadius * 2 + "rpx"
  } : {}, {
    j: $props.required
  }, $props.required ? {
    k: $options.getRequiredColor
  } : {}, {
    l: $props.label
  }, $props.label ? {
    m: common_vendor.t($props.label),
    n: $options.getLabelSize + "rpx",
    o: $options.getLabelColor,
    p: $options.getLabelSize + "rpx",
    q: $options.getLabelColor,
    r: $props.labelWidth + "rpx"
  } : {}, {
    s: $props.textRight ? 1 : "",
    t: $props.disabled ? 1 : "",
    v: $options.getSize + "rpx",
    w: $props.color,
    x: $props.type,
    y: $props.name,
    z: $data.inputVal,
    A: $props.password,
    B: $data.inputVal ? "" : $props.placeholder,
    C: $data.placeholderStyl,
    D: $props.disabled,
    E: $props.cursorSpacing,
    F: $props.maxlength,
    G: $data.focused,
    H: $props.confirmType,
    I: $props.confirmHold,
    J: $props.cursor,
    K: $props.selectionStart,
    L: $props.selectionEnd,
    M: $props.adjustPosition,
    N: $props.holdKeyboard,
    O: $props.autoBlur,
    P: common_vendor.o((...args) => $options.onFocus && $options.onFocus(...args)),
    Q: common_vendor.o((...args) => $options.onBlur && $options.onBlur(...args)),
    R: common_vendor.o((...args) => $options.onInput && $options.onInput(...args)),
    S: common_vendor.o((...args) => $options.onConfirm && $options.onConfirm(...args)),
    T: common_vendor.o((...args) => $options.onKeyboardheightchange && $options.onKeyboardheightchange(...args)),
    U: $props.clearable && $data.inputVal != ""
  }, $props.clearable && $data.inputVal != "" ? {
    V: $props.clearSize,
    W: $props.clearColor,
    X: common_vendor.o((...args) => $options.onClear && $options.onClear(...args))
  } : {}, {
    Y: $props.borderTop && !$props.inputBorder ? 1 : "",
    Z: $props.borderBottom && !$props.inputBorder ? 1 : "",
    aa: $props.isFillet && !$options.getRadius ? 1 : "",
    ab: $props.inputBorder ? 1 : "",
    ac: common_vendor.s($options.getStyles),
    ad: common_vendor.o((...args) => $options.fieldClick && $options.fieldClick(...args))
  });
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-0df87698"], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/components/thorui/tui-input/tui-input.vue"]]);
wx.createComponent(Component);
