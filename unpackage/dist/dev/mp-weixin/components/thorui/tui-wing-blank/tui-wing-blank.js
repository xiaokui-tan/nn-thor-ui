"use strict";
const common_vendor = require("../../../common/vendor.js");
const _sfc_main = {
  name: "tuiWingBlank",
  emits: ["click"],
  props: {
    //small、default、large
    size: {
      type: String,
      default: "default"
    },
    gap: {
      type: [Number, String],
      default: 0
    },
    background: {
      type: String,
      default: "transparent"
    },
    radius: {
      type: [Number, String],
      default: 0
    },
    marginTop: {
      type: [Number, String],
      default: 0
    },
    marginBottom: {
      type: [Number, String],
      default: 0
    }
  },
  computed: {
    getPadding() {
      let styles = "";
      const padding = Number(this.gap);
      if (padding && padding > 0) {
        styles += `padding:0 ${padding}rpx;`;
      }
      return styles;
    },
    getStyles() {
      let styles = `background:${this.background};border-radius:${this.radius}rpx;margin-top:${this.marginTop}rpx;margin-bottom:${this.marginBottom}rpx;`;
      styles += this.getPadding;
      return styles;
    }
  },
  methods: {
    handleClick() {
      this.$emit("click");
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.n($props.radius ? "tui-wingblank__hidden" : ""),
    b: common_vendor.n($options.getPadding ? "" : `tui-wingblank__${$props.size}`),
    c: common_vendor.s($options.getStyles),
    d: common_vendor.o((...args) => $options.handleClick && $options.handleClick(...args))
  };
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-4d610ac0"], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/components/thorui/tui-wing-blank/tui-wing-blank.vue"]]);
wx.createComponent(Component);
