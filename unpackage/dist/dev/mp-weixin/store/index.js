"use strict";
const common_vendor = require("../common/vendor.js");
const common_httpRequest = require("../common/httpRequest.js");
const store = common_vendor.createStore({
  state: {
    //用户登录手机号
    mobile: common_vendor.index.getStorageSync("thorui_mobile") || "echo.",
    //是否登录 项目中改为真实登录信息判断，如token
    isLogin: common_vendor.index.getStorageSync("thorui_mobile") ? true : false,
    //登录后跳转的页面路径 + 页面参数
    returnUrl: "",
    //app版本
    version: "2.9.5",
    //当前是否有网络连接
    networkConnected: true,
    isOnline: true,
    userAvatar: "/static/images/my/mine_avatar_3x.jpg"
  },
  mutations: {
    login(state, payload) {
      if (payload) {
        state.mobile = payload.mobile;
      }
      state.isLogin = true;
    },
    logout(state) {
      state.mobile = "";
      state.isLogin = false;
      state.returnUrl = "";
    },
    setReturnUrl(state, returnUrl) {
      state.returnUrl = returnUrl;
    },
    networkChange(state, payload) {
      state.networkConnected = payload.isConnected;
    },
    setOnline(state, payload) {
      state.isOnline = payload.isOnline;
    },
    setAvatar(state, payload) {
      state.userAvatar = payload.avatar;
    }
  },
  actions: {
    getOnlineStatus: async function({
      commit,
      state
    }) {
      return await new Promise((resolve, reject) => {
        if (state.isOnline) {
          resolve(state.isOnline);
        } else {
          common_httpRequest.tui.request("/Home/GetStatus", "GET", {}, false, true, true).then(
            (res) => {
              if (res.code == 100 && res.data == 1) {
                commit("setOnline", {
                  isOnline: true
                });
                resolve(true);
              } else {
                commit("setOnline", {
                  isOnline: false
                });
                resolve(false);
              }
            }
          ).catch((res) => {
            reject(false);
          });
        }
      });
    }
  }
});
exports.store = store;
