"use strict";
Object.defineProperty(exports, Symbol.toStringTag, { value: "Module" });
const common_vendor = require("./common/vendor.js");
const store_index = require("./store/index.js");
const common_httpRequest = require("./common/httpRequest.js");
const components_common_tuiRequest_index = require("./components/common/tui-request/index.js");
const components_thorui_tuiConfig_index = require("./components/thorui/tui-config/index.js");
if (!Math) {
  "./pages/ann/index/index.js";
  "./pages/ann/index/ditu.js";
  "./pages/ann/index/maps/maps.js";
  "./pages/ann/index1/index1.js";
  "./pages/ann/dangan/empty.js";
  "./pages/ann/dangan/dindex.js";
  "./pages/ann/dangan/daadd.js";
  "./pages/ann/my/my.js";
  "./pages/ann/supply/index.js";
  "./pages/ann/demand/index.js";
  "./pages/ann/orders/index.js";
  "./pages/ann/storeManagement/index.js";
  "./pages/ann/reviews/index.js";
  "./pages/ann/help/index.js";
}
const _sfc_main = {
  onLaunch: function() {
    let that = this;
    if (common_vendor.wx$1.canIUse("getUpdateManager")) {
      const updateManager = common_vendor.wx$1.getUpdateManager();
      updateManager.onCheckForUpdate(function(res) {
        if (res.hasUpdate) {
          updateManager.onUpdateReady(function() {
            that.tui.modal("更新提示", "新版本已经上线啦~，为了获得更好的体验，建议立即更新", false, (res2) => {
              updateManager.applyUpdate();
            });
          });
          updateManager.onUpdateFailed(function() {
            that.tui.modal("更新失败", "新版本更新失败，为了获得更好的体验，请您删除当前小程序，重新搜索打开", false, (res2) => {
            });
          });
        }
      });
    }
  },
  onShow: function() {
  },
  onHide: function() {
  }
};
const App = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/App.vue"]]);
components_common_tuiRequest_index.http.create({
  host: "https://www.thorui.cn",
  header: {
    "content-type": "application/x-www-form-urlencoded"
  }
});
components_common_tuiRequest_index.http.interceptors.request.use((config) => {
  let token = common_vendor.index.getStorageSync("thorui_token");
  if (config.header) {
    config.header["Authorization"] = token;
  } else {
    config.header = {
      "Authorization": token
    };
  }
  return config;
});
components_common_tuiRequest_index.http.interceptors.response.use((response) => {
  return response;
});
common_vendor.index.$tui = components_thorui_tuiConfig_index.propsConfig;
function createApp() {
  const app = common_vendor.createSSRApp(App);
  app.use(store_index.store);
  app.config.globalProperties.tui = common_httpRequest.tui;
  app.config.globalProperties.http = components_common_tuiRequest_index.http;
  return {
    app
  };
}
createApp().app.mount("#app");
exports.createApp = createApp;
