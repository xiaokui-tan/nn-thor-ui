if (typeof Promise !== "undefined" && !Promise.prototype.finally) {
  Promise.prototype.finally = function(callback) {
    const promise = this.constructor;
    return this.then(
      (value) => promise.resolve(callback()).then(() => value),
      (reason) => promise.resolve(callback()).then(() => {
        throw reason;
      })
    );
  };
}
;
if (typeof uni !== "undefined" && uni && uni.requireGlobal) {
  const global2 = uni.requireGlobal();
  ArrayBuffer = global2.ArrayBuffer;
  Int8Array = global2.Int8Array;
  Uint8Array = global2.Uint8Array;
  Uint8ClampedArray = global2.Uint8ClampedArray;
  Int16Array = global2.Int16Array;
  Uint16Array = global2.Uint16Array;
  Int32Array = global2.Int32Array;
  Uint32Array = global2.Uint32Array;
  Float32Array = global2.Float32Array;
  Float64Array = global2.Float64Array;
  BigInt64Array = global2.BigInt64Array;
  BigUint64Array = global2.BigUint64Array;
}
;
if (uni.restoreGlobal) {
  uni.restoreGlobal(Vue, weex, plus, setTimeout, clearTimeout, setInterval, clearInterval);
}
(function(vue) {
  "use strict";
  const _export_sfc = (sfc, props) => {
    const target = sfc.__vccOpts || sfc;
    for (const [key, val] of props) {
      target[key] = val;
    }
    return target;
  };
  const _sfc_main$u = {
    name: "tuiSearchbar",
    emits: ["clear", "focus", "blur", "click", "cancel", "input", "search"],
    props: {
      //搜索栏背景色
      backgroundColor: {
        type: String,
        default: "#ededed"
      },
      //搜索栏padding
      padding: {
        type: String,
        default: "16rpx 20rpx"
      },
      //input框高度
      height: {
        type: String,
        default: "72rpx"
      },
      radius: {
        type: String,
        default: "8rpx"
      },
      //input框背景色
      inputBgColor: {
        type: String,
        default: "#fff"
      },
      focus: {
        type: Boolean,
        default: false
      },
      placeholder: {
        type: String,
        default: "请输入搜索关键词"
      },
      value: {
        type: String,
        default: ""
      },
      //input是否禁用
      disabled: {
        type: Boolean,
        default: false
      },
      cancelText: {
        type: String,
        default: "取消"
      },
      cancelColor: {
        type: String,
        default: "#888"
      },
      cancel: {
        type: Boolean,
        default: true
      },
      searchText: {
        type: String,
        default: "搜索"
      },
      searchColor: {
        type: String,
        default: ""
      },
      //是否显示占位标签
      showLabel: {
        type: Boolean,
        default: true
      },
      //是否显示输入框
      showInput: {
        type: Boolean,
        default: true
      }
    },
    computed: {
      getSearchColor() {
        return this.searchColor || uni && uni.$tui && uni.$tui.color.primary || "#5677fc";
      }
    },
    created() {
      this.valueTxt = this.value;
      this.isFocus = this.focus;
      if (this.focus || this.valueTxt.length > 0) {
        this.searchState = true;
      }
      this.clearSize = Math.ceil(uni.upx2px(30));
      this.searchSize = Math.ceil(uni.upx2px(26));
    },
    watch: {
      value(val) {
        this.valueTxt = val;
      }
    },
    data() {
      return {
        searchState: false,
        isFocus: false,
        valueTxt: "",
        clearSize: 15,
        searchSize: 13
      };
    },
    methods: {
      clearInput() {
        this.valueTxt = "";
        this.isFocus = true;
        this.$emit("clear");
      },
      inputFocus(e) {
        if (!this.showLabel) {
          this.searchState = true;
        }
        this.$emit("focus", e.detail);
      },
      inputBlur(e) {
        this.isFocus = false;
        this.$emit("blur", e.detail);
      },
      tapShowInput() {
        if (!this.disabled && this.showInput) {
          this.searchState = true;
          setTimeout(() => {
            this.isFocus = true;
          }, 20);
        }
        this.$emit("click", {});
      },
      hideInput() {
        this.searchState = false;
        this.isFocus = false;
        uni.hideKeyboard();
        this.$emit("cancel", {});
      },
      inputChange(e) {
        this.valueTxt = e.detail.value;
        this.$emit("input", e.detail);
      },
      search() {
        this.$emit("search", {
          value: this.valueTxt
        });
      },
      reset() {
        this.searchState = false;
        this.isFocus = false;
        this.valueTxt = "";
        uni.hideKeyboard();
      }
    }
  };
  function _sfc_render$t(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock(
      "view",
      {
        class: "tui-searchbar__box",
        style: vue.normalizeStyle({ backgroundColor: $props.backgroundColor, padding: $props.padding })
      },
      [
        vue.renderSlot(_ctx.$slots, "default", {}, void 0, true),
        vue.createElementVNode(
          "view",
          {
            class: "tui-search-bar__form",
            style: vue.normalizeStyle({ height: $props.height })
          },
          [
            $props.showInput ? (vue.openBlock(), vue.createElementBlock(
              "view",
              {
                key: 0,
                class: vue.normalizeClass(["tui-search-bar__box", { "tui-searchbar__blur": !$data.isFocus && !$data.searchState && $props.showLabel && !$props.disabled }]),
                style: vue.normalizeStyle({ height: $props.height, borderRadius: $props.radius, backgroundColor: $props.inputBgColor })
              },
              [
                vue.createElementVNode("icon", {
                  class: "tui-icon-search",
                  type: "search",
                  size: $data.searchSize
                }, null, 8, ["size"]),
                vue.createElementVNode("input", {
                  type: "text",
                  class: "tui-search-bar__input",
                  placeholder: $props.placeholder,
                  value: $data.valueTxt,
                  focus: $data.isFocus,
                  disabled: $props.disabled,
                  "confirm-type": "search",
                  onBlur: _cache[0] || (_cache[0] = (...args) => $options.inputBlur && $options.inputBlur(...args)),
                  onFocus: _cache[1] || (_cache[1] = (...args) => $options.inputFocus && $options.inputFocus(...args)),
                  onInput: _cache[2] || (_cache[2] = (...args) => $options.inputChange && $options.inputChange(...args)),
                  onConfirm: _cache[3] || (_cache[3] = (...args) => $options.search && $options.search(...args))
                }, null, 40, ["placeholder", "value", "focus", "disabled"]),
                $data.valueTxt.length > 0 && !$props.disabled ? (vue.openBlock(), vue.createElementBlock("icon", {
                  key: 0,
                  type: "clear",
                  size: $data.clearSize,
                  class: "tui-icon-clear",
                  onClick: _cache[4] || (_cache[4] = vue.withModifiers((...args) => $options.clearInput && $options.clearInput(...args), ["stop"]))
                }, null, 8, ["size"])) : vue.createCommentVNode("v-if", true)
              ],
              6
              /* CLASS, STYLE */
            )) : vue.createCommentVNode("v-if", true),
            !$data.isFocus && !$data.searchState && $props.showLabel ? (vue.openBlock(), vue.createElementBlock(
              "view",
              {
                key: 1,
                class: "tui-search-bar__label",
                style: vue.normalizeStyle({ height: $props.height, borderRadius: $props.radius, backgroundColor: $props.inputBgColor }),
                onClick: _cache[5] || (_cache[5] = (...args) => $options.tapShowInput && $options.tapShowInput(...args))
              },
              [
                vue.createElementVNode("icon", {
                  class: "tui-icon-search",
                  type: "search",
                  size: $data.searchSize
                }, null, 8, ["size"]),
                vue.createElementVNode(
                  "text",
                  { class: "tui-search-bar__text" },
                  vue.toDisplayString($props.placeholder),
                  1
                  /* TEXT */
                )
              ],
              4
              /* STYLE */
            )) : vue.createCommentVNode("v-if", true)
          ],
          4
          /* STYLE */
        ),
        $props.cancel && $data.searchState && !$data.valueTxt ? (vue.openBlock(), vue.createElementBlock(
          "view",
          {
            key: 0,
            class: "tui-search-bar__cancel-btn",
            style: vue.normalizeStyle({ color: $props.cancelColor }),
            onClick: _cache[6] || (_cache[6] = (...args) => $options.hideInput && $options.hideInput(...args))
          },
          vue.toDisplayString($props.cancelText),
          5
          /* TEXT, STYLE */
        )) : vue.createCommentVNode("v-if", true),
        $data.valueTxt && !$props.disabled && $data.searchState ? (vue.openBlock(), vue.createElementBlock(
          "view",
          {
            key: 1,
            class: "tui-search-bar__cancel-btn",
            style: vue.normalizeStyle({ color: $options.getSearchColor }),
            onClick: _cache[7] || (_cache[7] = (...args) => $options.search && $options.search(...args))
          },
          vue.toDisplayString($props.searchText),
          5
          /* TEXT, STYLE */
        )) : vue.createCommentVNode("v-if", true)
      ],
      4
      /* STYLE */
    );
  }
  const __easycom_0$5 = /* @__PURE__ */ _export_sfc(_sfc_main$u, [["render", _sfc_render$t], ["__scopeId", "data-v-7cf4891b"], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/components/thorui/tui-searchbar/tui-searchbar.vue"]]);
  function formatAppLog(type, filename, ...args) {
    if (uni.__log__) {
      uni.__log__(type, filename, ...args);
    } else {
      console[type].apply(console, [...args, filename]);
    }
  }
  function resolveEasycom(component, easycom) {
    return typeof component === "string" ? easycom : component;
  }
  const icons = {
    "about": "",
    "about-fill": "",
    "add": "",
    "add-fill": "",
    "addmessage": "",
    "addressbook": "",
    "agree": "",
    "agree-fill": "",
    "alarm": "",
    "alarm-fill": "",
    "alipay": "",
    "android": "",
    "applets": "",
    "arrowdown": "",
    "arrowleft": "",
    "arrowright": "",
    "arrowup": "",
    "attestation": "",
    "back": "",
    "bag": "",
    "bag-fill": "",
    "balloon": "",
    "bankcard": "",
    "bankcard-fill": "",
    "bottom": "",
    "calendar": "",
    "camera": "",
    "camera-fill": "",
    "camera-add": "",
    "card": "",
    "card-fill": "",
    "cart": "",
    "cart-fill": "",
    "category": "",
    "category-fill": "",
    "check": "",
    "circle": "",
    "circle-fill": "",
    "circle-selected": "",
    "clock": "",
    "clock-fill": "",
    "close": "",
    "close-fill": "",
    "community": "",
    "community-fill": "",
    "computer": "",
    "computer-fill": "",
    "coupon": "",
    "delete": "",
    "deletekey": "",
    "dingtalk": "",
    "dissatisfied": "",
    "down": "",
    "download": "",
    "edit": "",
    "ellipsis": "",
    "enlarge": "",
    "evaluate": "",
    "exchange": "",
    "explain": "",
    "explain-fill": "",
    "explore": "",
    "explore-fill": "",
    "eye": "",
    "feedback": "",
    "fingerprint": "",
    "friendadd": "",
    "friendadd-fill": "",
    "gps": "",
    "histogram": "",
    "home": "",
    "home-fill": "",
    "house": "",
    "imface": "",
    "imkeyboard": "",
    "immore": "",
    "imvoice": "",
    "ios": "",
    "kefu": "",
    "label": "",
    "label-fill": "",
    "like": "",
    "like-fill": "",
    "link": "",
    "listview": "",
    "loading": "",
    "location": "",
    "mail": "",
    "mail-fill": "",
    "manage": "",
    "manage-fill": "",
    "member": "",
    "member-fill": "",
    "message": "",
    "message-fill": "",
    "mobile": "",
    "moments": "",
    "more": "",
    "more-fill": "",
    "narrow": "",
    "news": "",
    "news-fill": "",
    "nodata": "",
    "notice": "",
    "notice-fill": "",
    "offline": "",
    "offline-fill": "",
    "oppose": "",
    "oppose-fill": "",
    "order": "",
    "partake": "",
    "people": "",
    "people-fill": "",
    "pic": "",
    "pic-fill": "",
    "picture": "",
    "pie": "",
    "plus": "",
    "polygonal": "",
    "position": "",
    "pwd": "",
    "qq": "",
    "qrcode": "",
    "redpacket": "",
    "redpacket-fill": "",
    "reduce": "",
    "refresh": "",
    "revoke": "",
    "satisfied": "",
    "screen": "",
    "search": "",
    "search-2": "",
    "send": "",
    "service": "",
    "service-fill": "",
    "setup": "",
    "setup-fill": "",
    "share": "",
    "share-fill": "",
    "shield": "",
    "shop": "",
    "shop-fill": "",
    "shut": "",
    "signin": "",
    "sina": "",
    "skin": "",
    "soso": "",
    "square": "",
    "square-fill": "",
    "square-selected": "",
    "star": "",
    "star-fill": "",
    "strategy": "",
    "sweep": "",
    "time": "",
    "time-fill": "",
    "todown": "",
    "toleft": "",
    "tool": "",
    "top": "",
    "toright": "",
    "towardsleft": "",
    "towardsright": "",
    "towardsright-fill": "",
    "transport": "",
    "transport-fill": "",
    "turningdown": "",
    "turningleft": "",
    "turningright": "",
    "turningup": "",
    "unreceive": "",
    "seen": "",
    "unseen": "",
    "up": "",
    "upload": "",
    "video": "",
    "voice": "",
    "voice-fill": "",
    "voipphone": "",
    "wallet": "",
    "warning": "",
    "wealth": "",
    "wealth-fill": "",
    "weather": "",
    "wechat": "",
    "wifi": "",
    "play": "",
    "suspend": ""
  };
  const _sfc_main$t = {
    name: "tuiIcon",
    emits: ["click"],
    props: {
      name: {
        type: String,
        default: ""
      },
      customPrefix: {
        type: String,
        default: ""
      },
      size: {
        type: [Number, String],
        default: 0
      },
      //px或者rpx
      unit: {
        type: String,
        default: ""
      },
      color: {
        type: String,
        default: ""
      },
      bold: {
        type: Boolean,
        default: false
      },
      margin: {
        type: String,
        default: "0"
      },
      index: {
        type: Number,
        default: 0
      }
    },
    computed: {
      getColor() {
        return this.color || uni && uni.$tui && uni.$tui.tuiIcon.color || "#999";
      },
      getSize() {
        const size = this.size || uni && uni.$tui && uni.$tui.tuiIcon.size || 32;
        const unit = this.unit || uni && uni.$tui && uni.$tui.tuiIcon.unit || "px";
        return size + unit;
      }
    },
    data() {
      return {
        icons
      };
    },
    methods: {
      handleClick() {
        this.$emit("click", {
          index: this.index
        });
      }
    }
  };
  function _sfc_render$s(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock(
      "text",
      {
        class: vue.normalizeClass(["tui-icon", [$props.customPrefix, $props.customPrefix ? $props.name : ""]]),
        style: vue.normalizeStyle({ color: $options.getColor, fontSize: $options.getSize, fontWeight: $props.bold ? "bold" : "normal", margin: $props.margin }),
        onClick: _cache[0] || (_cache[0] = (...args) => $options.handleClick && $options.handleClick(...args))
      },
      vue.toDisplayString($data.icons[$props.name] || ""),
      7
      /* TEXT, CLASS, STYLE */
    );
  }
  const __easycom_0$4 = /* @__PURE__ */ _export_sfc(_sfc_main$t, [["render", _sfc_render$s], ["__scopeId", "data-v-096cf6db"], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/components/thorui/tui-icon/tui-icon.vue"]]);
  const _sfc_main$s = {
    name: "tuiListCell",
    emits: ["click"],
    props: {
      //是否有箭头
      arrow: {
        type: Boolean,
        default: false
      },
      //V2.3.0+
      arrowColor: {
        type: String,
        default: ""
      },
      //是否有点击效果
      hover: {
        type: Boolean,
        default: true
      },
      //隐藏线条
      unlined: {
        type: Boolean,
        default: false
      },
      //V2.3.0+
      lineColor: {
        type: String,
        default: ""
      },
      //线条左偏移距离
      lineLeft: {
        type: [Number, String],
        default: -1
      },
      //线条右偏移距离
      lineRight: {
        type: [Number, String],
        default: 0
      },
      padding: {
        type: String,
        default: ""
      },
      marginTop: {
        type: [Number, String],
        default: 0
      },
      marginBottom: {
        type: [Number, String],
        default: 0
      },
      //背景颜色
      backgroundColor: {
        type: String,
        default: "#fff"
      },
      //字体大小
      size: {
        type: Number,
        default: 0
      },
      //字体颜色
      color: {
        type: String,
        default: ""
      },
      //圆角值
      radius: {
        type: [Number, String],
        default: 0
      },
      //箭头偏移距离
      arrowRight: {
        type: [Number, String],
        default: 30
      },
      index: {
        type: Number,
        default: 0
      }
    },
    computed: {
      getArrowColor() {
        return this.arrowColor || uni && uni.$tui && uni.$tui.tuiListCell.arrowColor || "#c0c0c0";
      },
      getLineColor() {
        return this.lineColor || uni && uni.$tui && uni.$tui.tuiListCell.lineColor || "#eaeef1";
      },
      getLineLeft() {
        let left = this.lineLeft;
        if (left === -1) {
          left = uni && uni.$tui && uni.$tui.tuiListCell.lineLeft;
        }
        return left === void 0 || left === null ? 30 : left;
      },
      getPadding() {
        return this.padding || uni && uni.$tui && uni.$tui.tuiListCell.padding || "26rpx 30rpx";
      },
      getColor() {
        return this.color || uni && uni.$tui && uni.$tui.tuiListCell.color || "#333";
      },
      getSize() {
        return this.size || uni && uni.$tui && uni.$tui.tuiListCell.size || 28;
      }
    },
    methods: {
      handleClick() {
        this.$emit("click", {
          index: this.index
        });
      }
    }
  };
  function _sfc_render$r(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock(
      "view",
      {
        class: vue.normalizeClass(["tui-list-class tui-list-cell", [
          $props.radius && $props.radius != "0" ? "tui-radius" : "",
          $props.hover ? "tui-cell-hover" : ""
        ]]),
        style: vue.normalizeStyle({ backgroundColor: $props.backgroundColor, fontSize: $options.getSize + "rpx", color: $options.getColor, padding: $options.getPadding, borderRadius: $props.radius + "rpx", marginTop: $props.marginTop + "rpx", marginBottom: $props.marginBottom + "rpx" }),
        onClick: _cache[0] || (_cache[0] = (...args) => $options.handleClick && $options.handleClick(...args))
      },
      [
        vue.renderSlot(_ctx.$slots, "default", {}, void 0, true),
        !$props.unlined ? (vue.openBlock(), vue.createElementBlock(
          "view",
          {
            key: 0,
            class: "tui-cell__line",
            style: vue.normalizeStyle({ borderBottomColor: $options.getLineColor, left: $options.getLineLeft + "rpx", right: $props.lineRight + "rpx" })
          },
          null,
          4
          /* STYLE */
        )) : vue.createCommentVNode("v-if", true),
        $props.arrow ? (vue.openBlock(), vue.createElementBlock(
          "view",
          {
            key: 1,
            class: "tui-cell__arrow",
            style: vue.normalizeStyle({ borderColor: $options.getArrowColor, right: $props.arrowRight + "rpx" })
          },
          null,
          4
          /* STYLE */
        )) : vue.createCommentVNode("v-if", true)
      ],
      6
      /* CLASS, STYLE */
    );
  }
  const __easycom_0$3 = /* @__PURE__ */ _export_sfc(_sfc_main$s, [["render", _sfc_render$r], ["__scopeId", "data-v-5d44a8e3"], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/components/thorui/tui-list-cell/tui-list-cell.vue"]]);
  const _sfc_main$r = {
    name: "tuiListView",
    props: {
      title: {
        type: String,
        default: ""
      },
      color: {
        type: String,
        default: "#666"
      },
      //rpx
      size: {
        type: Number,
        default: 30
      },
      backgroundColor: {
        type: String,
        default: "transparent"
      },
      unlined: {
        type: String,
        default: ""
        //top,bottom,all
      },
      marginTop: {
        type: String,
        default: "0"
      },
      //圆角值
      radius: {
        type: [Number, String],
        default: 0
      }
    }
  };
  function _sfc_render$q(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock(
      "view",
      {
        class: vue.normalizeClass(["tui-list-view tui-view-class", { "tui-radius": $props.radius && $props.radius != "0" }]),
        style: vue.normalizeStyle({ backgroundColor: $props.backgroundColor, marginTop: $props.marginTop, borderRadius: $props.radius + "rpx" })
      },
      [
        $props.title ? (vue.openBlock(), vue.createElementBlock(
          "view",
          {
            key: 0,
            class: "tui-list-title",
            style: vue.normalizeStyle({ color: $props.color, fontSize: $props.size + "rpx", lineHeight: "30rpx" })
          },
          vue.toDisplayString($props.title),
          5
          /* TEXT, STYLE */
        )) : vue.createCommentVNode("v-if", true),
        vue.createElementVNode(
          "view",
          {
            class: vue.normalizeClass(["tui-list-content", [$props.unlined ? "tui-border-" + $props.unlined : ""]])
          },
          [
            vue.renderSlot(_ctx.$slots, "default", {}, void 0, true)
          ],
          2
          /* CLASS */
        )
      ],
      6
      /* CLASS, STYLE */
    );
  }
  const __easycom_3$1 = /* @__PURE__ */ _export_sfc(_sfc_main$r, [["render", _sfc_render$q], ["__scopeId", "data-v-88f4ea34"], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/components/thorui/tui-list-view/tui-list-view.vue"]]);
  const _sfc_main$q = {
    name: "tui-form-button",
    emits: ["click", "getuserinfo", "contact", "getphonenumber", "error", "opensetting", "chooseavatar", "launchapp"],
    props: {
      //按钮背景色
      background: {
        type: String,
        default: ""
      },
      //按钮显示文本
      text: {
        type: String,
        default: ""
      },
      //按钮字体颜色
      color: {
        type: String,
        default: ""
      },
      //按钮禁用背景色
      disabledBackground: {
        type: String,
        default: ""
      },
      //按钮禁用字体颜色
      disabledColor: {
        type: String,
        default: ""
      },
      borderWidth: {
        type: String,
        default: "1rpx"
      },
      borderColor: {
        type: String,
        default: ""
      },
      //宽度
      width: {
        type: String,
        default: "100%"
      },
      //高度
      height: {
        type: String,
        default: ""
      },
      //medium 368*80 / small 240*80/ mini 116*64
      btnSize: {
        type: String,
        default: ""
      },
      //字体大小，单位rpx
      size: {
        type: [Number, String],
        default: 0
      },
      bold: {
        type: Boolean,
        default: false
      },
      margin: {
        type: String,
        default: "0"
      },
      //圆角
      radius: {
        type: String,
        default: ""
      },
      plain: {
        type: Boolean,
        default: false
      },
      link: {
        type: Boolean,
        default: false
      },
      disabled: {
        type: Boolean,
        default: false
      },
      loading: {
        type: Boolean,
        default: false
      },
      formType: {
        type: String,
        default: ""
      },
      openType: {
        type: String,
        default: ""
      },
      //支付宝小程序 
      //当 open-type 为 getAuthorize 时，可以设置 scope 为：phoneNumber、userInfo
      scope: {
        type: String,
        default: ""
      },
      appParameter: {
        type: String,
        default: ""
      },
      index: {
        type: [Number, String],
        default: 0
      }
    },
    computed: {
      getWidth() {
        let width = this.width;
        if (this.btnSize && this.btnSize !== true) {
          width = {
            "medium": "368rpx",
            "small": "240rpx",
            "mini": "116rpx"
          }[this.btnSize] || this.width;
        }
        return width;
      },
      getHeight() {
        let height = this.height || uni && uni.$tui && uni.$tui.tuiFormButton.height || "96rpx";
        if (this.btnSize && this.btnSize !== true) {
          height = {
            "medium": "80rpx",
            "small": "80rpx",
            "mini": "64rpx"
          }[this.btnSize] || height;
        }
        return height;
      },
      getBackground() {
        return this.background || uni && uni.$tui && uni.$tui.tuiFormButton.background || "#5677fc";
      },
      getColor() {
        return this.color || uni && uni.$tui && uni.$tui.tuiFormButton.color || "#fff";
      },
      getRadius() {
        return this.radius || uni && uni.$tui && uni.$tui.tuiFormButton.radius || "6rpx";
      },
      getSize() {
        return this.size || uni && uni.$tui && uni.$tui.tuiFormButton.size || 32;
      }
    },
    data() {
      return {
        time: 0,
        trigger: false,
        tap: false
      };
    },
    methods: {
      handleStart() {
        if (this.disabled)
          return;
        this.trigger = false;
        this.tap = true;
        if ((/* @__PURE__ */ new Date()).getTime() - this.time <= 150)
          return;
        this.trigger = true;
        this.time = (/* @__PURE__ */ new Date()).getTime();
      },
      handleClick() {
        if (this.disabled || !this.trigger)
          return;
        this.time = 0;
      },
      handleTap() {
        if (this.disabled)
          return;
        this.$emit("click", {
          index: Number(this.index)
        });
      },
      handleEnd() {
        if (this.disabled)
          return;
        setTimeout(() => {
          this.time = 0;
        }, 150);
      },
      bindgetuserinfo({
        detail = {}
      } = {}) {
        this.$emit("getuserinfo", detail);
      },
      bindcontact({
        detail = {}
      } = {}) {
        this.$emit("contact", detail);
      },
      bindgetphonenumber({
        detail = {}
      } = {}) {
        this.$emit("getphonenumber", detail);
      },
      binderror({
        detail = {}
      } = {}) {
        this.$emit("error", detail);
      },
      bindopensetting({
        detail = {}
      } = {}) {
        this.$emit("opensetting", detail);
      },
      bindchooseavatar({
        detail = {}
      } = {}) {
        this.$emit("chooseavatar", detail);
      },
      bindlaunchapp({
        detail = {}
      } = {}) {
        this.$emit("launchapp", detail);
      }
    }
  };
  function _sfc_render$p(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock(
      "view",
      {
        class: "tui-button__container",
        style: vue.normalizeStyle({ width: $options.getWidth, height: $props.height, margin: $props.margin, borderRadius: $options.getRadius }),
        onTouchstart: _cache[8] || (_cache[8] = (...args) => $options.handleStart && $options.handleStart(...args)),
        onTouchend: _cache[9] || (_cache[9] = (...args) => $options.handleClick && $options.handleClick(...args)),
        onTouchcancel: _cache[10] || (_cache[10] = (...args) => $options.handleEnd && $options.handleEnd(...args))
      },
      [
        vue.createElementVNode("button", {
          class: vue.normalizeClass(["tui-button", [
            $props.bold ? "tui-text__bold" : "",
            $data.time && ($props.plain || $props.link) ? "tui-button__opacity" : "",
            $props.disabled && !$props.disabledBackground ? "tui-button__opacity" : "",
            (!$props.width || $props.width === "100%" || $props.width === true) && (!$props.btnSize || $props.btnSize === true) ? "tui-button__flex-1" : "",
            $data.time && !$props.plain && !$props.link ? "tui-button__active" : ""
          ]]),
          style: vue.normalizeStyle({
            width: $options.getWidth,
            height: $options.getHeight,
            lineHeight: $options.getHeight,
            background: $props.disabled && $props.disabledBackground ? $props.disabledBackground : $props.plain ? "transparent" : $options.getBackground,
            borderWidth: $props.borderWidth,
            borderColor: $props.borderColor ? $props.borderColor : $props.disabled && $props.disabledBackground ? $props.disabledBackground : $props.link ? "transparent" : $options.getBackground,
            borderRadius: $options.getRadius,
            fontSize: $options.getSize + "rpx",
            color: $props.disabled && $props.disabledBackground ? $props.disabledColor : $options.getColor
          }),
          loading: $props.loading,
          "form-type": $props.formType,
          "open-type": $props.openType,
          "app-parameter": $props.appParameter,
          onGetuserinfo: _cache[0] || (_cache[0] = (...args) => $options.bindgetuserinfo && $options.bindgetuserinfo(...args)),
          onGetphonenumber: _cache[1] || (_cache[1] = (...args) => $options.bindgetphonenumber && $options.bindgetphonenumber(...args)),
          onContact: _cache[2] || (_cache[2] = (...args) => $options.bindcontact && $options.bindcontact(...args)),
          onError: _cache[3] || (_cache[3] = (...args) => $options.binderror && $options.binderror(...args)),
          onOpensetting: _cache[4] || (_cache[4] = (...args) => $options.bindopensetting && $options.bindopensetting(...args)),
          onChooseavatar: _cache[5] || (_cache[5] = (...args) => $options.bindchooseavatar && $options.bindchooseavatar(...args)),
          onLaunchapp: _cache[6] || (_cache[6] = (...args) => $options.bindlaunchapp && $options.bindlaunchapp(...args)),
          disabled: $props.disabled,
          scope: $props.scope,
          onClick: _cache[7] || (_cache[7] = vue.withModifiers((...args) => $options.handleTap && $options.handleTap(...args), ["stop"]))
        }, [
          $props.text ? (vue.openBlock(), vue.createElementBlock(
            "text",
            {
              key: 0,
              class: vue.normalizeClass(["tui-button__text", { "tui-text__bold": $props.bold }]),
              style: vue.normalizeStyle({ fontSize: $options.getSize + "rpx", lineHeight: $options.getSize + "rpx", color: $props.disabled && $props.disabledBackground ? $props.disabledColor : $options.getColor })
            },
            vue.toDisplayString($props.text),
            7
            /* TEXT, CLASS, STYLE */
          )) : vue.createCommentVNode("v-if", true),
          vue.renderSlot(_ctx.$slots, "default", {}, void 0, true)
        ], 46, ["loading", "form-type", "open-type", "app-parameter", "disabled", "scope"])
      ],
      36
      /* STYLE, NEED_HYDRATION */
    );
  }
  const __easycom_4$1 = /* @__PURE__ */ _export_sfc(_sfc_main$q, [["render", _sfc_render$p], ["__scopeId", "data-v-fc40545d"], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/components/thorui/tui-form-button/tui-form-button.vue"]]);
  const _sfc_main$p = {
    name: "tuiScrollTop",
    emits: ["index", "share"],
    props: {
      //回顶部按钮距离底部距离 rpx
      bottom: {
        type: Number,
        default: 180
      },
      //回顶部按钮距离右侧距离 rpx
      right: {
        type: Number,
        default: 25
      },
      //距离顶部多少距离显示 px
      top: {
        type: Number,
        default: 200
      },
      //滚动距离
      scrollTop: {
        type: Number
      },
      //回顶部滚动时间
      duration: {
        type: Number,
        default: 120
      },
      //是否显示返回首页按钮
      isIndex: {
        type: Boolean,
        default: false
      },
      //是否显示分享图标
      isShare: {
        type: Boolean,
        default: false
      },
      //自定义分享(小程序可使用button=>open-type="share")
      customShare: {
        type: Boolean,
        default: false
      },
      //回顶部icon
      topIcon: {
        type: String,
        default: "/static/components/scroll-top/icon_top_3x.png"
      },
      //回首页icon
      indexIcon: {
        type: String,
        default: "/static/components/scroll-top/icon_index_3x.png"
      },
      //分享icon
      shareIcon: {
        type: String,
        default: "/static/components/scroll-top/icon_share_3x.png"
      }
    },
    watch: {
      scrollTop(newValue, oldValue) {
        this.change();
      }
    },
    data() {
      return {
        //判断是否显示
        visible: false,
        //控制显示，主要解决调用api滚到顶部fixed元素抖动的问题
        toggle: true
      };
    },
    methods: {
      goTop: function() {
        this.toggle = false;
        uni.pageScrollTo({
          scrollTop: 0,
          duration: this.duration
        });
        setTimeout(() => {
          this.toggle = true;
        }, 220);
      },
      goIndex: function() {
        this.$emit("index", {});
      },
      share() {
        this.$emit("share", {});
      },
      change() {
        let show = this.scrollTop > this.top;
        if (show && this.visible || !show && !this.visible) {
          return;
        }
        this.visible = show;
      }
    }
  };
  function _sfc_render$o(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.withDirectives((vue.openBlock(), vue.createElementBlock(
      "view",
      {
        class: "tui-scroll-top_box",
        style: vue.normalizeStyle({ bottom: $props.bottom + "rpx", right: $props.right + "rpx" })
      },
      [
        $props.isIndex ? (vue.openBlock(), vue.createElementBlock("view", {
          key: 0,
          class: "tui-scroll-top_item",
          onClick: _cache[0] || (_cache[0] = vue.withModifiers((...args) => $options.goIndex && $options.goIndex(...args), ["stop"]))
        }, [
          vue.createElementVNode("image", {
            class: "tui-scroll-top_img",
            src: $props.indexIcon
          }, null, 8, ["src"]),
          vue.createElementVNode("view", { class: "tui-scroll-top_text" }, "首页")
        ])) : vue.createCommentVNode("v-if", true),
        $props.isShare && !$props.customShare ? (vue.openBlock(), vue.createElementBlock("button", {
          key: 1,
          "open-type": "share",
          class: "tui-share-btn"
        }, [
          vue.createElementVNode(
            "view",
            {
              class: vue.normalizeClass(["tui-scroll-top_item", { "tui-scroll-item_top": $props.isIndex }])
            },
            [
              vue.createElementVNode("image", {
                class: "tui-scroll-top_img",
                src: $props.shareIcon
              }, null, 8, ["src"])
            ],
            2
            /* CLASS */
          )
        ])) : vue.createCommentVNode("v-if", true),
        $props.isShare && $props.customShare ? (vue.openBlock(), vue.createElementBlock(
          "view",
          {
            key: 2,
            class: vue.normalizeClass(["tui-scroll-top_item", { "tui-scroll-item_top": $props.isIndex }]),
            onClick: _cache[1] || (_cache[1] = vue.withModifiers((...args) => $options.share && $options.share(...args), ["stop"]))
          },
          [
            vue.createElementVNode("image", {
              class: "tui-scroll-top_img",
              src: $props.shareIcon
            }, null, 8, ["src"])
          ],
          2
          /* CLASS */
        )) : vue.createCommentVNode("v-if", true),
        vue.withDirectives(vue.createElementVNode(
          "view",
          {
            class: vue.normalizeClass(["tui-scroll-top_item", { "tui-scroll-item_top": $props.isIndex || $props.isShare }]),
            onClick: _cache[2] || (_cache[2] = vue.withModifiers((...args) => $options.goTop && $options.goTop(...args), ["stop"]))
          },
          [
            vue.createElementVNode("image", {
              class: "tui-scroll-top_img",
              src: $props.topIcon
            }, null, 8, ["src"]),
            vue.createElementVNode("view", { class: "tui-scroll-top_text tui-color-white" }, "顶部")
          ],
          2
          /* CLASS */
        ), [
          [vue.vShow, $data.visible && $data.toggle]
        ])
      ],
      4
      /* STYLE */
    )), [
      [vue.vShow, $props.isIndex || $props.isShare || $data.visible && $data.toggle]
    ]);
  }
  const __easycom_5$1 = /* @__PURE__ */ _export_sfc(_sfc_main$p, [["render", _sfc_render$o], ["__scopeId", "data-v-8cc2dd62"], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/components/thorui/tui-scroll-top/tui-scroll-top.vue"]]);
  const json$1 = {
    tabBar: [
      {
        pagePath: "/pages/ann/index/index",
        text: "首页",
        iconPath: "/static/ann/tabbar/index.png",
        selectedIconPath: "/static/ann/tabbar/index-active.png",
        iconSize: 80
      },
      {
        pagePath: "/pages/ann/dangan/dindex",
        text: "档案",
        iconPath: "/static/ann/tabbar/dangan1.png",
        selectedIconPath: "/static/ann/tabbar/dangan1-active.png"
      },
      {
        pagePath: "/pages/ann/dangan/dindex",
        text: "发布",
        iconPath: "/static/ann/tabbar/fabu.png",
        hump: true,
        selectedIconPath: "/static/ann/tabbar/fabu-active.png"
      },
      {
        pagePath: "/pages/ann/dangan/dindex",
        text: "记账",
        iconPath: "/static/ann/tabbar/jizhang1.png",
        selectedIconPath: "/static/ann/tabbar/jizhang1-active.png"
      },
      {
        pagePath: "/pages/ann/my/my",
        text: "我的",
        iconPath: "/static/ann/tabbar/my1.png",
        selectedIconPath: "/static/ann/tabbar/my1-active.png"
      }
    ],
    category: [
      {
        img: "../../../static/ann/index/dangan.png",
        name: "档案",
        page: "/pages/ann/dangan/dindex"
      },
      {
        img: "../../../static/ann/index/jizhang.png",
        name: "记账",
        page: "/pages/ann/dangan/dindex"
      }
    ],
    action: [{
      img: "../../../static/ann/index/gongying.png",
      name: "供应",
      page: "/pages/ann/supply/supply"
    }, {
      img: "../../../static/ann/index/qiugou.png",
      name: "求购",
      page: "/pages/ann/orders/order"
    }, {
      img: "../../../static/ann/index/ditu.png",
      name: "地图",
      page: "/pages/ann/index/maps"
    }, {
      img: "../../../static/ann/index/hangqing.png",
      name: "行情",
      page: "/pages/ann/market/market"
    }],
    items: [
      {
        id: 0,
        text: "全部",
        isScreen: false,
        down: false
      },
      {
        id: 1,
        text: "推荐",
        isScreen: false,
        down: false
      },
      {
        id: 2,
        text: "供应",
        isScreen: false,
        down: false
      },
      {
        id: 3,
        text: "求购",
        isScreen: false,
        down: false
      },
      {
        id: 4,
        text: "综合",
        isScreen: false,
        down: false
      }
    ],
    comsorts: [
      {
        id: "1",
        value: "按时间从近到远",
        selected: false
      },
      {
        id: "2",
        value: "按时间从远到近",
        selected: false
      }
      // {
      // 	id: '3',
      // 	value: '按位置从近到远',
      // 	selected: false
      // }, {
      // 	id: '4',
      // 	value: '按总价从低到高',
      // 	selected: false
      // }, {
      // 	id: '5',
      // 	value: '按分类',
      // 	selected: false
      // }, 
    ],
    indexdetails: [
      {
        "id": "1",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡",
        "actiontype": 0,
        //0供应 1求购
        "isrecommend": 0,
        //是否推荐，0否1是
        "address": "广东省深圳市南山区XX街XX号",
        "hasphonenum": true,
        //有联系电话
        "allowonlineconnect": false,
        //可以在线沟通
        "fabudate": "2024-05-01"
        //发布日期
      },
      {
        "id": "2",
        "image": "../../../static/ann/images/neimeng-cattle.png",
        "title": "草原牛",
        "actiontype": 1,
        "isrecommend": 1,
        //是否推荐，0否1是
        "address": "内蒙古自治区呼和浩特市赛罕区XX路XX号",
        "hasphonenum": false,
        "allowonlineconnect": true,
        "fabudate": "2024-05-07"
      },
      {
        "id": "3",
        "image": "../../../static/ann/images/goat.png",
        "title": "山羊",
        "actiontype": 0,
        "isrecommend": 1,
        //是否推荐，0否1是
        "address": "河南省郑州市二七区XX大道XX号",
        "hasphonenum": true,
        "allowonlineconnect": true,
        "fabudate": "2024-05-04"
      },
      {
        "id": "4",
        "image": "../../../static/ann/images/hetian-horse.png",
        "title": "和田马",
        "actiontype": 1,
        "isrecommend": 0,
        //是否推荐，0否1是
        "address": "浙江省杭州市西湖区XX路XX号",
        "hasphonenum": false,
        "allowonlineconnect": false
      },
      {
        "id": "6",
        "image": "../../../static/ann/images/rabbit.png",
        "title": "兔子",
        "actiontype": 1,
        "isrecommend": 1,
        //是否推荐，0否1是
        "address": "广东省广州市天河区XX路XX号",
        "hasphonenum": false,
        "allowonlineconnect": true,
        "fabudate": "2024-05-09"
      },
      {
        "id": "7",
        "image": "../../../static/ann/images/jiayang-duck.png",
        "title": "家养鸭",
        "actiontype": 0,
        "isrecommend": 0,
        //是否推荐，0否1是
        "address": "福建省厦门市思明区XX街XX号",
        "hasphonenum": true,
        "allowonlineconnect": true,
        "fabudate": "2024-05-03"
      },
      {
        "id": "8",
        "image": "../../../static/ann/images/yufei-goose.png",
        "title": "育肥鹅",
        "actiontype": 1,
        "isrecommend": 1,
        //是否推荐，0否1是
        "address": "四川省成都市武侯区XX路XX号",
        "hasphonenum": false,
        "allowonlineconnect": false,
        "fabudate": "2024-05-02"
      },
      {
        "id": "9",
        "image": "../../../static/ann/images/hemu-dove.png",
        "title": "食用贺姆鸽",
        "actiontype": 0,
        "isrecommend": 0,
        //是否推荐，0否1是
        "address": "湖南省长沙市芙蓉区XX街XX号",
        "hasphonenum": true,
        "allowonlineconnect": true,
        "fabudate": "2024-05-11"
      },
      {
        "id": "10",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡1",
        "actiontype": 0,
        //0供应 1求购
        "isrecommend": 0,
        //是否推荐，0否1是
        "address": "广东省深圳市南山区XX街XX号",
        "hasphonenum": true,
        //有联系电话
        "allowonlineconnect": false,
        //可以在线沟通
        "fabudate": "2024-01-01"
        //发布日期
      },
      {
        "id": "11",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡11",
        "actiontype": 1,
        "isrecommend": 0,
        "address": "陕西省西安市长安区杨庄街道",
        "hasphonenum": false,
        "allowonlineconnect": true,
        "fabudate": "2023-04-21"
      },
      {
        "id": "12",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡12",
        "actiontype": 0,
        "isrecommend": 1,
        "address": "浙江省杭州市萧山区宁围镇",
        "hasphonenum": true,
        "allowonlineconnect": true,
        "fabudate": "2024-01-12"
      },
      {
        "id": "13",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡13",
        "actiontype": 0,
        "isrecommend": 0,
        "address": "四川省成都市青羊区草市街",
        "hasphonenum": false,
        "allowonlineconnect": false,
        "fabudate": "2023-06-05"
      },
      {
        "id": "14",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡14",
        "actiontype": 1,
        "isrecommend": 1,
        "address": "湖南省长沙市望城区金星村",
        "hasphonenum": true,
        "allowonlineconnect": true,
        "fabudate": "2023-11-17"
      },
      {
        "id": "15",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡15",
        "actiontype": 1,
        "isrecommend": 0,
        "address": "广东省深圳市宝安区新安街道",
        "hasphonenum": false,
        "allowonlineconnect": false,
        "fabudate": "2024-03-22"
      },
      {
        "id": "16",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡16",
        "actiontype": 0,
        "isrecommend": 1,
        "address": "江苏省南京市鼓楼区华侨路",
        "hasphonenum": true,
        "allowonlineconnect": true,
        "fabudate": "2023-07-08"
      },
      {
        "id": "17",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡17",
        "actiontype": 0,
        "isrecommend": 0,
        "address": "福建省厦门市思明区莲前街道",
        "hasphonenum": false,
        "allowonlineconnect": false,
        "fabudate": "2023-09-19"
      },
      {
        "id": "18",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡18",
        "actiontype": 1,
        "isrecommend": 1,
        "address": "辽宁省沈阳市沈河区北站路",
        "hasphonenum": true,
        "allowonlineconnect": true,
        "fabudate": "2024-05-30"
      },
      {
        "id": "19",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡19",
        "actiontype": 1,
        "isrecommend": 0,
        "address": "内蒙古自治区呼和浩特市回民区",
        "hasphonenum": false,
        "allowonlineconnect": true,
        "fabudate": "2023-12-01"
      },
      {
        "id": "20",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡20",
        "actiontype": 0,
        "isrecommend": 1,
        "address": "山东省青岛市市南区香港中路",
        "hasphonenum": true,
        "allowonlineconnect": false,
        "fabudate": "2023-08-15"
      },
      {
        "id": "21",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡21",
        "actiontype": 1,
        "isrecommend": 0,
        "address": "北京市东城区王府井",
        "hasphonenum": true,
        "allowonlineconnect": false,
        "fabudate": "2023-05-09"
      },
      {
        "id": "22",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡22",
        "actiontype": 0,
        "isrecommend": 1,
        "address": "上海市黄浦区南京东路",
        "hasphonenum": false,
        "allowonlineconnect": true,
        "fabudate": "2023-10-23"
      },
      {
        "id": "23",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡23",
        "actiontype": 1,
        "isrecommend": 1,
        "address": "天津市和平区劝业场",
        "hasphonenum": true,
        "allowonlineconnect": false,
        "fabudate": "2024-02-17"
      },
      {
        "id": "24",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡24",
        "actiontype": 0,
        "isrecommend": 0,
        "address": "重庆市渝中区解放碑",
        "hasphonenum": false,
        "allowonlineconnect": true,
        "fabudate": "2023-03-12"
      },
      {
        "id": "25",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡25",
        "actiontype": 1,
        "isrecommend": 1,
        "address": "河北省石家庄市长安区",
        "hasphonenum": true,
        "allowonlineconnect": false,
        "fabudate": "2024-07-28"
      },
      {
        "id": "26",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡26",
        "actiontype": 0,
        "isrecommend": 0,
        "address": "山西省太原市迎泽区",
        "hasphonenum": false,
        "allowonlineconnect": true,
        "fabudate": "2023-08-11"
      },
      {
        "id": "27",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡27",
        "actiontype": 1,
        "isrecommend": 1,
        "address": "内蒙古自治区呼和浩特市新城区",
        "hasphonenum": true,
        "allowonlineconnect": false,
        "fabudate": "2023-12-20"
      },
      {
        "id": "28",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡28",
        "actiontype": 0,
        "isrecommend": 0,
        "address": "辽宁省大连市中山区",
        "hasphonenum": false,
        "allowonlineconnect": true,
        "fabudate": "2024-06-05"
      },
      {
        "id": "29",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡29",
        "actiontype": 1,
        "isrecommend": 1,
        "address": "吉林省长春市南关区",
        "hasphonenum": true,
        "allowonlineconnect": false,
        "fabudate": "2023-09-07"
      },
      {
        "id": "30",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡30",
        "actiontype": 0,
        "isrecommend": 0,
        "address": "黑龙江省哈尔滨市道里区",
        "hasphonenum": false,
        "allowonlineconnect": true,
        "fabudate": "2024-01-15"
      },
      {
        "id": "31",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡31",
        "actiontype": 1,
        "isrecommend": 1,
        "address": "上海市浦东新区张江镇",
        "hasphonenum": true,
        "allowonlineconnect": false,
        "fabudate": "2023-10-05"
      },
      {
        "id": "32",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡32",
        "actiontype": 0,
        "isrecommend": 0,
        "address": "江苏省苏州市工业园区",
        "hasphonenum": false,
        "allowonlineconnect": true,
        "fabudate": "2023-11-12"
      },
      {
        "id": "33",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡33",
        "actiontype": 1,
        "isrecommend": 1,
        "address": "浙江省杭州市西湖区",
        "hasphonenum": true,
        "allowonlineconnect": false,
        "fabudate": "2024-01-20"
      },
      {
        "id": "34",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡34",
        "actiontype": 0,
        "isrecommend": 0,
        "address": "安徽省合肥市瑶海区",
        "hasphonenum": false,
        "allowonlineconnect": true,
        "fabudate": "2023-12-15"
      },
      {
        "id": "35",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡35",
        "actiontype": 1,
        "isrecommend": 1,
        "address": "福建省福州市台江区",
        "hasphonenum": true,
        "allowonlineconnect": false,
        "fabudate": "2024-02-28"
      },
      {
        "id": "36",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡36",
        "actiontype": 0,
        "isrecommend": 0,
        "address": "江西省南昌市东湖区",
        "hasphonenum": false,
        "allowonlineconnect": true,
        "fabudate": "2023-08-19"
      },
      {
        "id": "37",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡37",
        "actiontype": 1,
        "isrecommend": 1,
        "address": "山东省济南市历下区",
        "hasphonenum": true,
        "allowonlineconnect": false,
        "fabudate": "2023-09-10"
      },
      {
        "id": "38",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡38",
        "actiontype": 0,
        "isrecommend": 0,
        "address": "河南省郑州市中原区",
        "hasphonenum": false,
        "allowonlineconnect": true,
        "fabudate": "2024-03-05"
      },
      {
        "id": "39",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡39",
        "actiontype": 1,
        "isrecommend": 1,
        "address": "湖北省武汉市江岸区",
        "hasphonenum": true,
        "allowonlineconnect": false,
        "fabudate": "2023-07-30"
      },
      {
        "id": "40",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡40",
        "actiontype": 0,
        "isrecommend": 0,
        "address": "湖南省长沙市芙蓉区",
        "hasphonenum": false,
        "allowonlineconnect": true,
        "fabudate": "2024-04-22"
      },
      {
        "id": "41",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡41",
        "actiontype": 1,
        "isrecommend": 1,
        "address": "广东省广州市天河区",
        "hasphonenum": true,
        "allowonlineconnect": false,
        "fabudate": "2023-10-18"
      },
      {
        "id": "42",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡42",
        "actiontype": 0,
        "isrecommend": 0,
        "address": "广西壮族自治区南宁市青秀区",
        "hasphonenum": false,
        "allowonlineconnect": true,
        "fabudate": "2024-01-12"
      },
      {
        "id": "43",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡43",
        "actiontype": 1,
        "isrecommend": 1,
        "address": "海南省海口市龙华区",
        "hasphonenum": true,
        "allowonlineconnect": false,
        "fabudate": "2023-11-25"
      },
      {
        "id": "44",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡44",
        "actiontype": 0,
        "isrecommend": 0,
        "address": "重庆市江北区",
        "hasphonenum": false,
        "allowonlineconnect": true,
        "fabudate": "2024-02-03"
      },
      {
        "id": "45",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡45",
        "actiontype": 1,
        "isrecommend": 1,
        "address": "四川省成都市武侯区",
        "hasphonenum": true,
        "allowonlineconnect": false,
        "fabudate": "2023-12-08"
      },
      {
        "id": "46",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡46",
        "actiontype": 0,
        "isrecommend": 0,
        "address": "贵州省贵阳市南明区",
        "hasphonenum": false,
        "allowonlineconnect": true,
        "fabudate": "2024-03-17"
      },
      {
        "id": "47",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡47",
        "actiontype": 1,
        "isrecommend": 1,
        "address": "云南省昆明市五华区",
        "hasphonenum": true,
        "allowonlineconnect": false,
        "fabudate": "2023-08-25"
      },
      {
        "id": "48",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡48",
        "actiontype": 0,
        "isrecommend": 0,
        "address": "西藏自治区拉萨市城关区",
        "hasphonenum": false,
        "allowonlineconnect": true,
        "fabudate": "2024-05-10"
      },
      {
        "id": "49",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡49",
        "actiontype": 1,
        "isrecommend": 1,
        "address": "陕西省西安市新城区",
        "hasphonenum": true,
        "allowonlineconnect": false,
        "fabudate": "2023-09-15"
      },
      {
        "id": "50",
        "image": "../../../static/ann/images/free-range-chicken7.png",
        "title": "农家土鸡50",
        "actiontype": 0,
        "isrecommend": 0,
        "address": "甘肃省兰州市城关区",
        "hasphonenum": false,
        "allowonlineconnect": true,
        "fabudate": "2024-06-20"
      }
    ]
  };
  const index = {
    components: {
      tuiScrollTop: __easycom_5$1
    },
    data() {
      return {
        current: 0,
        scrollTop: 0,
        isIndex: false,
        isShare: false,
        hideTop: false,
        customShare: true,
        //tabBar: json.tabBar,
        searchText: "",
        category: json$1.category,
        action: json$1.action,
        visible: false,
        isdown: false,
        items: json$1.items,
        comsorts: json$1.comsorts,
        indexdetails: [],
        convertIndexDetails: []
      };
    },
    created() {
    },
    mounted() {
      this.initshow();
    },
    onShow() {
      this.initshow();
    },
    activated() {
      this.initshow();
    },
    onLoad() {
    },
    onPageScroll(e) {
      if (!this.hideTop) {
        this.scrollTop = e.scrollTop;
      }
    },
    methods: {
      tabbarSwitch(e) {
        this.current = e.index;
      },
      switchTab(item) {
        uni.switchTab({
          url: item.page
        });
      },
      navigateTo(item) {
        if (item.page.length > 0) {
          uni.navigateTo({
            url: item.page
          });
        }
      },
      onPageScroll(e) {
        if (!this.hideTop) {
          this.scrollTop = e.scrollTop;
        }
      },
      convertActionType(actionType) {
        return actionType === 0 ? "供应" : "求购";
      },
      updatedIndexDetails() {
        this.indexdetails = json$1.indexdetails;
        this.convertIndexDetails = this.indexdetails.map((item) => {
          return {
            ...item,
            // 展开原始对象
            actionTypeText: this.convertActionType(item.actiontype)
            // 添加新属性
          };
        });
      },
      initshow() {
        formatAppLog("log", "at pages/ann/index/index.js:90", this.$checkPlatform());
        if (this.$checkPlatform) {
          if (this.$checkPlatform() !== "web") {
            this.visible = false;
            this.isdown = false;
            this.setItemsScreenStatus();
            this.updatedIndexDetails();
            this.sortby(1);
          }
        }
        const fakeScrollEvent = {
          scrollTop: 100
        };
        this.onPageScroll(fakeScrollEvent);
      },
      setItemsScreenStatus() {
        this.items.forEach((item) => {
          item.isScreen = item.id === 0;
        });
      },
      //搜索栏输入后点击搜索
      handleInput(event) {
        this.searchText = event.value;
        this.search(this.searchText);
      },
      //搜索
      search(searchText) {
        this.updatedIndexDetails();
        if (this.searchText.length != 0) {
          this.searchItems(searchText);
        }
      },
      //模糊搜索
      searchItems(query) {
        const lowerCaseQuery = query.toLowerCase();
        this.convertIndexDetails = this.convertIndexDetails.filter((item) => {
          return item.title.toLowerCase().includes(lowerCaseQuery) || item.actionTypeText.toLowerCase().includes(lowerCaseQuery) || item.address.toLowerCase().includes(lowerCaseQuery);
        });
      },
      //搜索页签切换
      screen(item) {
        this.items.forEach((item2) => {
          item2.isScreen = false;
        });
        item.isScreen = true;
        if (item.id == 4) {
          this.isdown = !this.isdown;
          this.visible = this.isdown;
        } else {
          this.isdown = false;
          this.visible = false;
        }
        this.screensearch(item);
        this.sortby(1);
      },
      screensearch(item) {
        if (item.id != 4) {
          this.search(this.searchText);
        }
        if (item.id == 0)
          ;
        else if (item.id == 1) {
          this.convertIndexDetails = this.convertIndexDetails.filter((item2) => item2.isrecommend === 1);
        } else if (item.id == 2) {
          this.convertIndexDetails = this.convertIndexDetails.filter((item2) => item2.actiontype === 0);
        } else if (item.id == 3) {
          this.convertIndexDetails = this.convertIndexDetails.filter((item2) => item2.actiontype === 1);
        }
      },
      comorderby(item) {
        this.search(this.searchText);
        if (item.id == 1) {
          this.sortby(item.id);
          this.visible = false;
          this.isdown = false;
        } else if (item.id == 2) {
          this.sortby(item.id);
          this.visible = false;
          this.isdown = false;
        } else
          ;
      },
      sortby(order) {
        this.convertIndexDetails.sort((a, b) => {
          const dateA = new Date(a.fabudate);
          const dateB = new Date(b.fabudate);
          return order == 1 ? dateB - dateA : dateA - dateB;
        });
      },
      goIndex() {
        uni.switchTab({
          url: "/pages/ann/index/index"
        });
      }
    }
  };
  const _sfc_main$o = {
    mixins: [index]
  };
  function _sfc_render$n(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_tui_searchbar = resolveEasycom(vue.resolveDynamicComponent("tui-searchbar"), __easycom_0$5);
    const _component_tui_icon = resolveEasycom(vue.resolveDynamicComponent("tui-icon"), __easycom_0$4);
    const _component_tui_list_cell = resolveEasycom(vue.resolveDynamicComponent("tui-list-cell"), __easycom_0$3);
    const _component_tui_list_view = resolveEasycom(vue.resolveDynamicComponent("tui-list-view"), __easycom_3$1);
    const _component_tui_form_button = resolveEasycom(vue.resolveDynamicComponent("tui-form-button"), __easycom_4$1);
    const _component_tui_scroll_top = resolveEasycom(vue.resolveDynamicComponent("tui-scroll-top"), __easycom_5$1);
    return vue.openBlock(), vue.createElementBlock(
      vue.Fragment,
      null,
      [
        vue.createVNode(_component_tui_searchbar, {
          radius: "8rpx",
          height: "50rpx",
          padding: "5rpx",
          placeholder: "搜索",
          cancel: "false",
          searchColor: "#00aa00",
          style: { "position": "fixed", "top": "0", "left": "0", "z-index": "9999", "right": "0", "padding": "1rpx 0rpx 2rpx 0rpx !important" },
          value: _ctx.searchText,
          onSearch: _ctx.handleInput,
          onCancel: _cache[0] || (_cache[0] = ($event) => _ctx.search(""))
        }, null, 8, ["value", "onSearch"]),
        vue.createElementVNode("view", { class: "view-back" }, [
          vue.createElementVNode("view", { class: "tui-product-category" }, [
            (vue.openBlock(true), vue.createElementBlock(
              vue.Fragment,
              null,
              vue.renderList(_ctx.category, (item, index2) => {
                return vue.openBlock(), vue.createElementBlock("view", {
                  class: "tui-category-item",
                  key: index2,
                  "data-key": item.name,
                  onClick: ($event) => _ctx.switchTab(item)
                }, [
                  vue.createElementVNode("image", {
                    src: item.img,
                    class: "tui-category-img",
                    mode: "scaleToFill"
                  }, null, 8, ["src"]),
                  vue.createElementVNode(
                    "view",
                    { class: "span-dajz" },
                    vue.toDisplayString(item.name),
                    1
                    /* TEXT */
                  )
                ], 8, ["data-key", "onClick"]);
              }),
              128
              /* KEYED_FRAGMENT */
            ))
          ]),
          vue.createElementVNode("view", { class: "tui-product-action" }, [
            (vue.openBlock(true), vue.createElementBlock(
              vue.Fragment,
              null,
              vue.renderList(_ctx.action, (item, index2) => {
                return vue.openBlock(), vue.createElementBlock("view", {
                  class: "tui-action-item",
                  key: index2,
                  "data-key": item.name,
                  onClick: ($event) => _ctx.navigateTo(item)
                }, [
                  vue.createElementVNode("image", {
                    src: item.img,
                    class: "tui-action-img",
                    mode: "scaleToFill"
                  }, null, 8, ["src"]),
                  vue.createElementVNode(
                    "view",
                    { class: "tui-action-text" },
                    vue.toDisplayString(item.name),
                    1
                    /* TEXT */
                  )
                ], 8, ["data-key", "onClick"]);
              }),
              128
              /* KEYED_FRAGMENT */
            ))
          ])
        ]),
        vue.createElementVNode("view", { class: "container" }, [
          vue.createElementVNode("view", { class: "tui-header" }, [
            vue.createElementVNode("view", { class: "tui-header-top" }, [
              (vue.openBlock(true), vue.createElementBlock(
                vue.Fragment,
                null,
                vue.renderList(_ctx.items, (item, index2) => {
                  return vue.openBlock(), vue.createElementBlock(
                    "view",
                    {
                      key: index2,
                      class: vue.normalizeClass({ "tui-top-item": true, "tui-screen": item.isScreen })
                    },
                    [
                      vue.createElementVNode("text", {
                        class: "tui-bold",
                        onClick: ($event) => _ctx.screen(item)
                      }, vue.toDisplayString(item.text), 9, ["onClick"]),
                      vue.withDirectives(vue.createVNode(_component_tui_icon, {
                        name: "arrowup",
                        size: 12,
                        color: "#5677fc",
                        bold: true,
                        onClick: ($event) => _ctx.screen(item)
                      }, null, 8, ["onClick"]), [
                        [vue.vShow, item.id == 4 && !_ctx.isdown]
                      ]),
                      vue.withDirectives(vue.createVNode(_component_tui_icon, {
                        name: "arrowdown",
                        size: 12,
                        color: "#5677fc",
                        bold: true,
                        onClick: ($event) => _ctx.screen(item)
                      }, null, 8, ["onClick"]), [
                        [vue.vShow, item.id == 4 && _ctx.isdown]
                      ])
                    ],
                    2
                    /* CLASS */
                  );
                }),
                128
                /* KEYED_FRAGMENT */
              ))
            ])
          ]),
          vue.withDirectives(vue.createVNode(
            _component_tui_list_view,
            {
              title: "",
              class: "tui-list-view-item"
            },
            {
              default: vue.withCtx(() => [
                (vue.openBlock(true), vue.createElementBlock(
                  vue.Fragment,
                  null,
                  vue.renderList(_ctx.comsorts, (item, id) => {
                    return vue.openBlock(), vue.createBlock(_component_tui_list_cell, {
                      hover: false,
                      arrow: false,
                      key: item.id,
                      "data-key": item.value,
                      style: { "padding-top": "15rpx", "padding-bottom": "15rpx" }
                    }, {
                      default: vue.withCtx(() => [
                        vue.createElementVNode("view", {
                          class: "tui-action-text",
                          onClick: ($event) => _ctx.comorderby(item)
                        }, vue.toDisplayString(item.value), 9, ["onClick"])
                      ]),
                      _: 2
                      /* DYNAMIC */
                    }, 1032, ["data-key"]);
                  }),
                  128
                  /* KEYED_FRAGMENT */
                ))
              ]),
              _: 1
              /* STABLE */
            },
            512
            /* NEED_PATCH */
          ), [
            [vue.vShow, _ctx.visible]
          ]),
          vue.createVNode(_component_tui_list_view, {
            title: "",
            color: "#777",
            style: { "margin-top": "10px" }
          }, {
            default: vue.withCtx(() => [
              (vue.openBlock(true), vue.createElementBlock(
                vue.Fragment,
                null,
                vue.renderList(_ctx.convertIndexDetails, (item, id) => {
                  return vue.openBlock(), vue.createBlock(_component_tui_list_cell, {
                    hover: false,
                    arrow: false,
                    key: item.id,
                    "data-key": item.value,
                    style: { "padding-top": "15rpx", "padding-bottom": "15rpx" }
                  }, {
                    default: vue.withCtx(() => [
                      vue.createElementVNode("view", { class: "cell-content" }, [
                        vue.createElementVNode("image", {
                          src: item.image,
                          class: "left-image"
                        }, null, 8, ["src"]),
                        vue.createElementVNode("view", { class: "right-content" }, [
                          vue.createElementVNode("view", { class: "title-action-type" }, [
                            vue.createElementVNode(
                              "view",
                              { class: "title" },
                              vue.toDisplayString(item.title),
                              1
                              /* TEXT */
                            ),
                            item.actiontype === 0 ? (vue.openBlock(), vue.createElementBlock("view", {
                              key: 0,
                              class: "action-type1"
                            }, "供")) : (vue.openBlock(), vue.createElementBlock("view", {
                              key: 1,
                              class: "action-type2"
                            }, "购"))
                          ]),
                          vue.createElementVNode("view", { class: "address" }, [
                            vue.createVNode(_component_tui_icon, {
                              name: "gps",
                              size: "15"
                            }),
                            vue.createTextVNode(
                              vue.toDisplayString(item.address),
                              1
                              /* TEXT */
                            )
                          ]),
                          vue.createElementVNode("view", { class: "contact-info" }, [
                            item.hasphonenum ? (vue.openBlock(), vue.createBlock(_component_tui_form_button, {
                              key: 0,
                              class: "contact-button",
                              background: "#abfce4",
                              color: "#333333",
                              type: "primary",
                              width: "160rpx",
                              size: "20",
                              height: "36rpx",
                              radius: "50px"
                            }, {
                              default: vue.withCtx(() => [
                                vue.createVNode(_component_tui_icon, {
                                  name: "kefu",
                                  size: "15"
                                }),
                                vue.createTextVNode("联系电话")
                              ]),
                              _: 1
                              /* STABLE */
                            })) : vue.createCommentVNode("v-if", true),
                            item.allowonlineconnect ? (vue.openBlock(), vue.createBlock(_component_tui_form_button, {
                              key: 1,
                              class: "contact-button",
                              background: "#abfce4",
                              color: "#333333",
                              type: "primary",
                              width: "160rpx",
                              size: "20",
                              height: "36rpx",
                              radius: "50px"
                            }, {
                              default: vue.withCtx(() => [
                                vue.createVNode(_component_tui_icon, {
                                  name: "message-fill",
                                  size: "15"
                                }),
                                vue.createTextVNode("线上沟通")
                              ]),
                              _: 1
                              /* STABLE */
                            })) : vue.createCommentVNode("v-if", true)
                          ])
                        ])
                      ])
                    ]),
                    _: 2
                    /* DYNAMIC */
                  }, 1032, ["data-key"]);
                }),
                128
                /* KEYED_FRAGMENT */
              ))
            ]),
            _: 1
            /* STABLE */
          })
        ]),
        vue.createElementVNode("view", { class: "tui-bottom-text" }, "到达底部~"),
        vue.createVNode(_component_tui_scroll_top, {
          scrollTop: _ctx.scrollTop,
          isIndex: _ctx.isIndex,
          isShare: _ctx.isShare,
          customShare: _ctx.customShare,
          onIndex: _ctx.goIndex
        }, null, 8, ["scrollTop", "isIndex", "isShare", "customShare", "onIndex"]),
        vue.createCommentVNode(' <tui-tabbar :current="current" backdropFilter backgroundColor="#f8f8f8" :tabBar="tabBar" color="#777"\r\n		selectedColor="#AC9157" @click="tabbarSwitch"></tui-tabbar> ')
      ],
      64
      /* STABLE_FRAGMENT */
    );
  }
  const PagesAnnIndexIndex = /* @__PURE__ */ _export_sfc(_sfc_main$o, [["render", _sfc_render$n], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/pages/ann/index/index.vue"]]);
  const sjson = {
    items: [
      {
        id: 0,
        text: "全部",
        isScreen: false,
        down: false
      },
      {
        id: 1,
        text: "推荐",
        isScreen: false,
        down: false
      },
      {
        id: 4,
        text: "综合",
        isScreen: false,
        down: false
      }
    ]
  };
  const supply = {
    components: {
      tuiScrollTop: __easycom_5$1
    },
    props: {
      type: {
        type: Number,
        default: 0
        // 默认值为0，代表供应
      }
    },
    data() {
      return {
        scrollTop: 0,
        isIndex: true,
        isShare: false,
        hideTop: false,
        customShare: true,
        visible: false,
        isdown: false,
        platform: "",
        isPC: false,
        //type: 0, //供应
        searchText: "",
        items: sjson.items,
        comsorts: json$1.comsorts,
        indexdetails: [],
        convertIndexDetails: []
      };
    },
    created() {
    },
    mounted() {
      this.platform = this.$checkPlatform();
      this.initshow();
    },
    onShow() {
      this.platform = this.$checkPlatform();
      this.initshow();
    },
    activated() {
      this.platform = this.$checkPlatform();
      this.initshow();
    },
    mounted() {
      const item = this.items.find((item2) => item2.id === 0);
      if (item) {
        item.isScreen = true;
      }
      this.updatedIndexDetails();
      this.sortby(1);
    },
    onPageScroll(e) {
      if (!this.hideTop) {
        this.scrollTop = e.scrollTop;
      }
    },
    computed: {
      // searchBarStyle() {
      // 	//const systemInfo = uni.getSystemInfoSync();
      // 	const topValue = (this.platform === 'android' || this.platform === 'ios') ? '80rpx' :
      // 		'70rpx';
      // 	__f__('log','at pages/ann/supply/supply.js:71','searchBarStyle executed', topValue);
      // 	return {
      // 		position: 'fixed',
      // 		top: topValue,
      // 		left: '0',
      // 		right: '0',
      // 		zIndex: '9999'
      // 	};
      // },
      // listviewStyle() {
      // 	const topValue = (this.platform === 'android' || this.platform === 'ios') ? '80rpx' : '70rpx';
      // 	return {
      // 		marginTop: topValue
      // 	};
      // }
    },
    methods: {
      convertActionType(actionType) {
        return actionType === 0 ? "供应" : "求购";
      },
      updatedIndexDetails() {
        this.indexdetails = json$1.indexdetails.filter((item) => item.actiontype === this.type);
        this.convertIndexDetails = this.indexdetails.map((item) => {
          return {
            ...item,
            // 展开原始对象
            actionTypeText: this.convertActionType(item.actiontype)
            // 添加新属性
          };
        });
      },
      initshow() {
        formatAppLog("log", "at pages/ann/supply/supply.js:101", "Platform：" + this.platform);
        if (this.$checkPlatform) {
          if (this.platform !== "web") {
            this.visible = false;
            this.isdown = false;
            this.setItemsScreenStatus();
            this.updatedIndexDetails();
            this.sortby(1);
          }
        }
      },
      setItemsScreenStatus() {
        this.items.forEach((item) => {
          item.isScreen = item.id === 0;
        });
      },
      //搜索栏输入后点击搜索
      handleInput(event) {
        this.searchText = event.value;
        this.search(this.searchText);
      },
      //搜索
      search(searchText) {
        this.updatedIndexDetails();
        if (this.searchText.length != 0) {
          this.searchItems(searchText);
        }
      },
      //模糊搜索
      searchItems(query) {
        const lowerCaseQuery = query.toLowerCase();
        this.convertIndexDetails = this.convertIndexDetails.filter((item) => {
          return item.title.toLowerCase().includes(lowerCaseQuery) || item.actionTypeText.toLowerCase().includes(lowerCaseQuery) || item.address.toLowerCase().includes(lowerCaseQuery);
        });
      },
      //搜索页签切换
      screen(item) {
        this.items.forEach((item2) => {
          item2.isScreen = false;
        });
        item.isScreen = true;
        if (item.id == 4) {
          this.isdown = !this.isdown;
          this.visible = this.isdown;
        } else {
          this.isdown = false;
          this.visible = false;
        }
        this.screensearch(item);
        this.sortby(1);
      },
      screensearch(item) {
        if (item.id != 4) {
          this.search(this.searchText);
        }
        if (item.id == 0)
          ;
        else if (item.id == 1) {
          this.convertIndexDetails = this.convertIndexDetails.filter((item2) => item2.isrecommend === 1);
        } else if (item.id == 2) {
          this.convertIndexDetails = this.convertIndexDetails.filter((item2) => item2.actiontype === 0);
        }
      },
      comorderby(item) {
        this.search(this.searchText);
        if (item.id == 1) {
          this.sortby(item.id);
          this.visible = false;
          this.isdown = false;
        } else if (item.id == 2) {
          this.sortby(item.id);
          this.visible = false;
          this.isdown = false;
        }
      },
      sortby(order) {
        this.convertIndexDetails.sort((a, b) => {
          const dateA = new Date(a.fabudate);
          const dateB = new Date(b.fabudate);
          return order == 1 ? dateB - dateA : dateA - dateB;
        });
      },
      goIndex() {
        uni.switchTab({
          url: "/pages/ann/index/index"
        });
      }
    }
  };
  const _sfc_main$n = {
    mixins: [supply]
  };
  function _sfc_render$m(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_tui_searchbar = resolveEasycom(vue.resolveDynamicComponent("tui-searchbar"), __easycom_0$5);
    const _component_tui_icon = resolveEasycom(vue.resolveDynamicComponent("tui-icon"), __easycom_0$4);
    const _component_tui_list_cell = resolveEasycom(vue.resolveDynamicComponent("tui-list-cell"), __easycom_0$3);
    const _component_tui_list_view = resolveEasycom(vue.resolveDynamicComponent("tui-list-view"), __easycom_3$1);
    const _component_tui_form_button = resolveEasycom(vue.resolveDynamicComponent("tui-form-button"), __easycom_4$1);
    const _component_tui_scroll_top = resolveEasycom(vue.resolveDynamicComponent("tui-scroll-top"), __easycom_5$1);
    return vue.openBlock(), vue.createElementBlock(
      vue.Fragment,
      null,
      [
        vue.createVNode(_component_tui_searchbar, {
          radius: "8rpx",
          height: "50rpx",
          placeholder: "搜索",
          cancel: "false",
          searchColor: "#00aa00",
          value: _ctx.searchText,
          onSearch: _ctx.handleInput,
          onCancel: _cache[0] || (_cache[0] = ($event) => _ctx.search("")),
          style: { "padding": "1rpx 0rpx 2rpx 0rpx !important" }
        }, null, 8, ["value", "onSearch"]),
        vue.createElementVNode("view", { class: "tui-header" }, [
          vue.createElementVNode("view", { class: "tui-header-top" }, [
            (vue.openBlock(true), vue.createElementBlock(
              vue.Fragment,
              null,
              vue.renderList(_ctx.items, (item, index2) => {
                return vue.openBlock(), vue.createElementBlock(
                  "view",
                  {
                    key: index2,
                    class: vue.normalizeClass({ "tui-top-item": true, "tui-screen": item.isScreen })
                  },
                  [
                    vue.createElementVNode("text", {
                      class: "tui-bold",
                      onClick: ($event) => _ctx.screen(item)
                    }, vue.toDisplayString(item.text), 9, ["onClick"]),
                    vue.withDirectives(vue.createVNode(_component_tui_icon, {
                      name: "arrowup",
                      size: 12,
                      color: "#5677fc",
                      bold: true,
                      onClick: ($event) => _ctx.screen(item)
                    }, null, 8, ["onClick"]), [
                      [vue.vShow, item.id == 4 && !_ctx.isdown]
                    ]),
                    vue.withDirectives(vue.createVNode(_component_tui_icon, {
                      name: "arrowdown",
                      size: 12,
                      color: "#5677fc",
                      bold: true,
                      onClick: ($event) => _ctx.screen(item)
                    }, null, 8, ["onClick"]), [
                      [vue.vShow, item.id == 4 && _ctx.isdown]
                    ])
                  ],
                  2
                  /* CLASS */
                );
              }),
              128
              /* KEYED_FRAGMENT */
            ))
          ])
        ]),
        vue.withDirectives(vue.createVNode(
          _component_tui_list_view,
          {
            title: "",
            class: "tui-list-view-item"
          },
          {
            default: vue.withCtx(() => [
              (vue.openBlock(true), vue.createElementBlock(
                vue.Fragment,
                null,
                vue.renderList(_ctx.comsorts, (item, id) => {
                  return vue.openBlock(), vue.createBlock(_component_tui_list_cell, {
                    hover: false,
                    arrow: false,
                    key: item.id,
                    "data-key": item.value,
                    style: { "padding-top": "15rpx", "padding-bottom": "15rpx" }
                  }, {
                    default: vue.withCtx(() => [
                      vue.createElementVNode("view", {
                        class: "tui-action-text",
                        onClick: ($event) => _ctx.comorderby(item)
                      }, vue.toDisplayString(item.value), 9, ["onClick"])
                    ]),
                    _: 2
                    /* DYNAMIC */
                  }, 1032, ["data-key"]);
                }),
                128
                /* KEYED_FRAGMENT */
              ))
            ]),
            _: 1
            /* STABLE */
          },
          512
          /* NEED_PATCH */
        ), [
          [vue.vShow, _ctx.visible]
        ]),
        vue.createVNode(_component_tui_list_view, {
          title: "",
          color: "#777",
          style: { "margin-top": "10rpx" }
        }, {
          default: vue.withCtx(() => [
            (vue.openBlock(true), vue.createElementBlock(
              vue.Fragment,
              null,
              vue.renderList(_ctx.convertIndexDetails, (item, id) => {
                return vue.openBlock(), vue.createBlock(_component_tui_list_cell, {
                  hover: false,
                  arrow: false,
                  key: item.id,
                  "data-key": item.value,
                  style: { "padding-top": "15rpx", "padding-bottom": "15rpx" }
                }, {
                  default: vue.withCtx(() => [
                    vue.createElementVNode("view", { class: "cell-content" }, [
                      vue.createElementVNode("image", {
                        src: item.image,
                        class: "left-image"
                      }, null, 8, ["src"]),
                      vue.createElementVNode("view", { class: "right-content" }, [
                        vue.createElementVNode("view", { class: "title-action-type" }, [
                          vue.createElementVNode(
                            "view",
                            { class: "title" },
                            vue.toDisplayString(item.title),
                            1
                            /* TEXT */
                          ),
                          item.actiontype === 0 ? (vue.openBlock(), vue.createElementBlock("view", {
                            key: 0,
                            class: "action-type1"
                          }, "供")) : (vue.openBlock(), vue.createElementBlock("view", {
                            key: 1,
                            class: "action-type2"
                          }, "购"))
                        ]),
                        vue.createElementVNode("view", { class: "address" }, [
                          vue.createVNode(_component_tui_icon, {
                            name: "gps",
                            size: "15"
                          }),
                          vue.createTextVNode(
                            vue.toDisplayString(item.address),
                            1
                            /* TEXT */
                          )
                        ]),
                        vue.createElementVNode("view", { class: "contact-info" }, [
                          item.hasphonenum ? (vue.openBlock(), vue.createBlock(_component_tui_form_button, {
                            key: 0,
                            class: "contact-button",
                            background: "#abfce4",
                            color: "#333333",
                            type: "primary",
                            width: "160rpx",
                            size: "20",
                            height: "36rpx",
                            radius: "50px"
                          }, {
                            default: vue.withCtx(() => [
                              vue.createVNode(_component_tui_icon, {
                                name: "kefu",
                                size: "15"
                              }),
                              vue.createTextVNode("联系电话")
                            ]),
                            _: 1
                            /* STABLE */
                          })) : vue.createCommentVNode("v-if", true),
                          item.allowonlineconnect ? (vue.openBlock(), vue.createBlock(_component_tui_form_button, {
                            key: 1,
                            class: "contact-button",
                            background: "#abfce4",
                            color: "#333333",
                            type: "primary",
                            width: "160rpx",
                            size: "20",
                            height: "36rpx",
                            radius: "50px"
                          }, {
                            default: vue.withCtx(() => [
                              vue.createVNode(_component_tui_icon, {
                                name: "message-fill",
                                size: "15"
                              }),
                              vue.createTextVNode("线上沟通")
                            ]),
                            _: 1
                            /* STABLE */
                          })) : vue.createCommentVNode("v-if", true)
                        ])
                      ])
                    ])
                  ]),
                  _: 2
                  /* DYNAMIC */
                }, 1032, ["data-key"]);
              }),
              128
              /* KEYED_FRAGMENT */
            ))
          ]),
          _: 1
          /* STABLE */
        }),
        vue.createElementVNode("view", { class: "tui-bottom-text" }, "到达底部~"),
        vue.createVNode(_component_tui_scroll_top, {
          scrollTop: _ctx.scrollTop,
          isIndex: _ctx.isIndex,
          isShare: _ctx.isShare,
          customShare: _ctx.customShare,
          onIndex: _ctx.goIndex
        }, null, 8, ["scrollTop", "isIndex", "isShare", "customShare", "onIndex"])
      ],
      64
      /* STABLE_FRAGMENT */
    );
  }
  const PagesAnnSupplySupply = /* @__PURE__ */ _export_sfc(_sfc_main$n, [["render", _sfc_render$m], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/pages/ann/supply/supply.vue"]]);
  const _sfc_main$m = {
    components: {
      supply: PagesAnnSupplySupply
    }
  };
  function _sfc_render$l(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_supply = vue.resolveComponent("supply");
    return vue.openBlock(), vue.createBlock(_component_supply, { type: 1 });
  }
  const PagesAnnOrdersOrder = /* @__PURE__ */ _export_sfc(_sfc_main$m, [["render", _sfc_render$l], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/pages/ann/orders/order.vue"]]);
  const _sfc_main$l = {
    data() {
      return {
        latitude: 39.984245,
        longitude: 116.407408,
        scale: 12,
        searchKeyword: "",
        markers: [],
        searching: false,
        isMapZooming: false,
        accuracy: "",
        selectedLocation: "",
        selectedPlace: "",
        infoWindow: null,
        map: null
      };
    },
    mounted() {
      this.loadAMapModule().then(() => {
        this.initMap();
        this.getLocation();
      });
    },
    methods: {
      loadAMapModule() {
        return new Promise((resolve, reject) => {
          uni.requireModule("amap").load({
            key: "0b9c1cb014eca86e069e00c64cc6f4d4",
            version: "1.4.15",
            plugins: []
          }, () => {
            resolve();
          }, (error) => {
            reject(error);
          });
        });
      },
      getLocation() {
        this.map = new AMap.Map("map", {
          resizeEnable: true
        });
        this.map.plugin("AMap.Geolocation", function() {
          var geolocation = new AMap.Geolocation({
            enableHighAccuracy: true,
            //是否使用高精度定位，默认:true
            timeout: 1e4
            //超过10秒后停止定位，默认：5s
          });
          geolocation.getCurrentPosition();
          AMap.event.addListener(geolocation, "complete", (data) => {
            this.latitude = data.position.lat;
            this.longitude = data.position.lng;
          });
          AMap.event.addListener(geolocation, "error", (err) => {
            formatAppLog("error", "at pages/ann/index/maps.vue:93", "定位失败：", err);
          });
        });
      },
      // 使用高德地图的搜索方法
      async search() {
        if (this.searching || !this.searchKeyword.trim())
          return;
        this.searching = true;
        try {
          const response = await fetch(
            `https://restapi.amap.com/v3/place/text?key=0b9c1cb014eca86e069e00c64cc6f4d4&keywords=${this.searchKeyword}&city=武汉`
          );
          const data = await response.json();
          this.markers = data.pois.map((poi) => ({
            latitude: poi.location.lat,
            longitude: poi.location.lng,
            title: poi.name
          }));
        } catch (error) {
          formatAppLog("error", "at pages/ann/index/maps.vue:113", "搜索出错：", error);
        } finally {
          this.searching = false;
        }
      },
      // 处理地图标记点击事件
      markerTap(e) {
        const marker = this.markers.find((marker2) => marker2.latitude === e.marker.latitude && marker2.longitude === e.marker.longitude);
        if (marker) {
          formatAppLog("log", "at pages/ann/index/maps.vue:124", marker.title);
        }
      },
      // 处理地图点击事件
      handleMapTap(event) {
        const {
          lng,
          lat
        } = event.lnglat;
        this.longitude = lng.toFixed(6);
        this.latitude = lat.toFixed(6);
        this.accuracy = "10000";
        this.map.clearMap();
        new AMap.Marker({
          position: [lng, lat],
          map: this.map
        });
        const geocoder = new AMap.Geocoder();
        geocoder.getAddress([lng, lat], (status, result) => {
          if (status === "complete" && result.regeocode) {
            this.selectedLocation = result.regeocode.formattedAddress;
            this.selectedPlace = result.regeocode.formattedAddress;
            this.openInfoWindow([lng, lat], this.selectedLocation);
          }
        });
      },
      // 使用高德地图的初始化方法
      initMap() {
        this.map = new AMap.Map("map", {
          resizeEnable: true
        });
        AMap.plugin("AMap.Geolocation", () => {
          const geolocation = new AMap.Geolocation({
            enableHighAccuracy: true,
            timeout: 1e4,
            buttonOffset: new AMap.Pixel(10, 20)
          });
          this.map.addControl(geolocation);
          geolocation.getCurrentPosition();
          AMap.event.addListener(geolocation, "complete", (data) => {
            this.longitude = data.position.lng;
            this.latitude = data.position.lat;
          });
          AMap.event.addListener(geolocation, "error", (err) => {
            formatAppLog("error", "at pages/ann/index/maps.vue:176", "定位失败：", err);
          });
        });
      },
      // 打开信息窗口方法
      openInfoWindow(point, content) {
        if (this.infoWindow) {
          this.infoWindow.close();
        }
        this.infoWindow = new AMap.InfoWindow({
          content,
          offset: new AMap.Pixel(0, -20)
        });
        this.infoWindow.open(this.map, point);
      },
      // 定位到位置方法
      locateOnMap() {
        if (this.longitude && this.latitude) {
          this.map.setCenter([this.longitude, this.latitude]);
        }
      },
      // 设置定位方法
      setLocation() {
        const center = this.map.getCenter();
        this.longitude = center.lng.toFixed(6);
        this.latitude = center.lat.toFixed(6);
        this.accuracy = "10000";
      }
    }
  };
  function _sfc_render$k(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock("view", { class: "container" }, [
      vue.createElementVNode("map", {
        class: "map",
        longitude: $data.longitude,
        latitude: $data.latitude,
        scale: $data.scale,
        onTap: _cache[0] || (_cache[0] = (...args) => $options.handleMapTap && $options.handleMapTap(...args)),
        onMarkertap: _cache[1] || (_cache[1] = (...args) => $options.markerTap && $options.markerTap(...args)),
        onTouchstart: _cache[2] || (_cache[2] = (...args) => _ctx.handleMapTouchStart && _ctx.handleMapTouchStart(...args)),
        onTouchend: _cache[3] || (_cache[3] = (...args) => _ctx.handleMapTouchEnd && _ctx.handleMapTouchEnd(...args))
      }, null, 40, ["longitude", "latitude", "scale"]),
      vue.createElementVNode("view", { class: "search-bar" }, [
        vue.withDirectives(vue.createElementVNode(
          "input",
          {
            type: "text",
            "onUpdate:modelValue": _cache[4] || (_cache[4] = ($event) => $data.searchKeyword = $event),
            placeholder: "搜索地点"
          },
          null,
          512
          /* NEED_PATCH */
        ), [
          [vue.vModelText, $data.searchKeyword]
        ]),
        vue.createElementVNode("button", {
          onClick: _cache[5] || (_cache[5] = (...args) => $options.search && $options.search(...args)),
          disabled: $data.searching || !$data.searchKeyword.trim()
        }, [
          !$data.searching ? (vue.openBlock(), vue.createElementBlock("text", { key: 0 }, "搜索")) : (vue.openBlock(), vue.createElementBlock("text", { key: 1 }, "搜索中..."))
        ], 8, ["disabled"])
      ]),
      vue.createElementVNode("view", { class: "input-container" }, [
        vue.createElementVNode("view", { class: "input-wrapper" }, [
          vue.createElementVNode("text", { class: "input-label" }, "经度："),
          vue.withDirectives(vue.createElementVNode(
            "input",
            {
              "onUpdate:modelValue": _cache[6] || (_cache[6] = ($event) => $data.longitude = $event),
              type: "text",
              class: "input-field",
              placeholder: "请输入经度"
            },
            null,
            512
            /* NEED_PATCH */
          ), [
            [vue.vModelText, $data.longitude]
          ])
        ]),
        vue.createElementVNode("view", { class: "input-wrapper" }, [
          vue.createElementVNode("text", { class: "input-label" }, "纬度："),
          vue.withDirectives(vue.createElementVNode(
            "input",
            {
              "onUpdate:modelValue": _cache[7] || (_cache[7] = ($event) => $data.latitude = $event),
              type: "text",
              class: "input-field",
              placeholder: "请输入纬度"
            },
            null,
            512
            /* NEED_PATCH */
          ), [
            [vue.vModelText, $data.latitude]
          ])
        ]),
        vue.createElementVNode("view", { class: "input-wrapper" }, [
          vue.createElementVNode("text", { class: "input-label" }, "精度："),
          vue.withDirectives(vue.createElementVNode(
            "input",
            {
              "onUpdate:modelValue": _cache[8] || (_cache[8] = ($event) => $data.accuracy = $event),
              type: "text",
              class: "input-field",
              placeholder: "请输入精度"
            },
            null,
            512
            /* NEED_PATCH */
          ), [
            [vue.vModelText, $data.accuracy]
          ])
        ]),
        vue.createElementVNode("view", { class: "input-wrapper" }, [
          vue.createElementVNode("text", { class: "input-label" }, "地名："),
          vue.createElementVNode(
            "text",
            { class: "input-field selected-place" },
            vue.toDisplayString($data.selectedPlace),
            1
            /* TEXT */
          )
        ]),
        vue.createElementVNode("view", { class: "btn-container" }, [
          vue.createElementVNode("button", {
            class: "locate-btn",
            onClick: _cache[9] || (_cache[9] = (...args) => $options.locateOnMap && $options.locateOnMap(...args))
          }, "定位到位置"),
          vue.createElementVNode("button", {
            class: "set-location-btn",
            onClick: _cache[10] || (_cache[10] = (...args) => $options.setLocation && $options.setLocation(...args))
          }, "设置定位")
        ])
      ])
    ]);
  }
  const PagesAnnIndexMaps = /* @__PURE__ */ _export_sfc(_sfc_main$l, [["render", _sfc_render$k], ["__scopeId", "data-v-475e55d3"], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/pages/ann/index/maps.vue"]]);
  const _sfc_main$k = {};
  function _sfc_render$j(_ctx, _cache) {
    return " 行情 ";
  }
  const PagesAnnMarketMarket = /* @__PURE__ */ _export_sfc(_sfc_main$k, [["render", _sfc_render$j], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/pages/ann/market/market.vue"]]);
  const _sfc_main$j = {
    name: "tuiTabs",
    emits: ["change"],
    props: {
      //标签页
      tabs: {
        type: Array,
        default() {
          return [];
        }
      },
      //tabs宽度，不传值则默认使用windowWidth，单位px
      width: {
        type: Number,
        default: 0
      },
      //rpx
      height: {
        type: Number,
        default: 80
      },
      //rpx 只对左右padding起作用，上下为0
      padding: {
        type: Number,
        default: 30
      },
      //背景色
      backgroundColor: {
        type: String,
        default: "#FFFFFF"
      },
      //是否固定
      isFixed: {
        type: Boolean,
        default: false
      },
      //px
      top: {
        type: Number,
        default: 0
      },
      //是否去掉底部线条
      unlined: {
        type: Boolean,
        default: false
      },
      //当前选项卡
      currentTab: {
        type: Number,
        default: 0
      },
      isSlider: {
        type: Boolean,
        default: true
      },
      //滑块宽度
      sliderWidth: {
        type: Number,
        default: 68
      },
      //滑块高度
      sliderHeight: {
        type: Number,
        default: 6
      },
      //滑块背景颜色
      sliderBgColor: {
        type: String,
        default: ""
      },
      sliderRadius: {
        type: String,
        default: "50rpx"
      },
      //滑块bottom
      bottom: {
        type: String,
        default: "0"
      },
      //标签页宽度
      itemWidth: {
        type: String,
        default: ""
      },
      //字体颜色
      color: {
        type: String,
        default: "#666"
      },
      //选中后字体颜色
      selectedColor: {
        type: String,
        default: ""
      },
      //字体大小
      size: {
        type: Number,
        default: 28
      },
      //选中后 是否加粗 ，未选中则无效
      bold: {
        type: Boolean,
        default: false
      },
      //2.3.0+
      scale: {
        type: [Number, String],
        default: 1
      },
      //角标字体颜色
      badgeColor: {
        type: String,
        default: "#fff"
      },
      //角标背景颜色
      badgeBgColor: {
        type: String,
        default: ""
      },
      zIndex: {
        type: [Number, String],
        default: 996
      }
    },
    watch: {
      currentTab() {
        this.checkCor();
      },
      tabs() {
        this.checkCor();
      },
      width(val) {
        this.tabsWidth = val;
        this.checkCor();
      }
    },
    computed: {
      getItemWidth() {
        let width = 100 / (this.tabs.length || 4) + "%";
        return this.itemWidth ? this.itemWidth : width;
      },
      getSliderBgColor() {
        return this.sliderBgColor || uni && uni.$tui && uni.$tui.color.primary || "#5677fc";
      },
      getSelectedColor() {
        return this.selectedColor || uni && uni.$tui && uni.$tui.color.primary || "#5677fc";
      },
      getBadgeBgColor() {
        return this.badgeBgColor || uni && uni.$tui && uni.$tui.color.pink || "#f74d54";
      }
    },
    created() {
      setTimeout(() => {
        uni.getSystemInfo({
          success: (res) => {
            this.winWidth = res.windowWidth;
            this.tabsWidth = this.width == 0 ? this.winWidth : this.width;
            this.checkCor();
          }
        });
      }, 0);
    },
    data() {
      return {
        winWidth: 0,
        tabsWidth: 0,
        scrollLeft: 0
      };
    },
    methods: {
      checkCor: function() {
        let tabsNum = this.tabs.length;
        let padding = uni.upx2px(Number(this.padding));
        let width = this.tabsWidth - padding * 2;
        let left = (width / tabsNum - uni.upx2px(Number(this.sliderWidth))) / 2 + padding;
        let scrollLeft = left;
        if (this.currentTab > 0) {
          scrollLeft = scrollLeft + width / tabsNum * this.currentTab;
        }
        this.scrollLeft = scrollLeft;
      },
      // 点击标题切换当前页时改变样式
      swichTabs: function(index2) {
        let item = this.tabs[index2];
        if (item && item.disabled)
          return;
        if (this.currentTab == index2) {
          return false;
        } else {
          this.$emit("change", {
            index: Number(index2)
          });
        }
      }
    }
  };
  function _sfc_render$i(_ctx, _cache, $props, $setup, $data, $options) {
    return $data.tabsWidth > 0 ? (vue.openBlock(), vue.createElementBlock(
      "view",
      {
        key: 0,
        class: vue.normalizeClass(["tui-tabs-view", [$props.isFixed ? "tui-tabs-fixed" : "tui-tabs-relative", $props.unlined ? "tui-unlined" : ""]]),
        style: vue.normalizeStyle({
          width: $data.tabsWidth + "px",
          height: $props.height + "rpx",
          padding: `0 ${$props.padding}rpx`,
          background: $props.backgroundColor,
          top: $props.isFixed ? $props.top + "px" : "auto",
          zIndex: $props.isFixed ? $props.zIndex : "auto"
        })
      },
      [
        (vue.openBlock(true), vue.createElementBlock(
          vue.Fragment,
          null,
          vue.renderList($props.tabs, (item, index2) => {
            return vue.openBlock(), vue.createElementBlock("view", {
              key: index2,
              class: "tui-tabs-item",
              style: vue.normalizeStyle({ width: $options.getItemWidth, height: $props.height + "rpx" }),
              onClick: vue.withModifiers(($event) => $options.swichTabs(index2), ["stop"])
            }, [
              vue.createElementVNode(
                "view",
                {
                  class: vue.normalizeClass(["tui-tabs-title", { "tui-tabs-disabled": item.disabled }]),
                  style: vue.normalizeStyle({
                    color: $props.currentTab == index2 ? $options.getSelectedColor : $props.color,
                    fontSize: $props.size + "rpx",
                    fontWeight: $props.bold && $props.currentTab == index2 ? "bold" : "normal",
                    transform: `scale(${$props.currentTab == index2 ? $props.scale : 1})`
                  })
                },
                [
                  vue.createTextVNode(
                    vue.toDisplayString(item.name) + " ",
                    1
                    /* TEXT */
                  ),
                  item.num || item.isDot ? (vue.openBlock(), vue.createElementBlock(
                    "view",
                    {
                      key: 0,
                      class: vue.normalizeClass([item.isDot ? "tui-badge__dot" : "tui-tabs__badge"]),
                      style: vue.normalizeStyle({ color: $props.badgeColor, backgroundColor: $options.getBadgeBgColor })
                    },
                    vue.toDisplayString(item.isDot ? "" : item.num),
                    7
                    /* TEXT, CLASS, STYLE */
                  )) : vue.createCommentVNode("v-if", true)
                ],
                6
                /* CLASS, STYLE */
              )
            ], 12, ["onClick"]);
          }),
          128
          /* KEYED_FRAGMENT */
        )),
        $props.isSlider ? (vue.openBlock(), vue.createElementBlock(
          "view",
          {
            key: 0,
            class: "tui-tabs-slider",
            style: vue.normalizeStyle({
              transform: "translateX(" + $data.scrollLeft + "px)",
              width: $props.sliderWidth + "rpx",
              height: $props.sliderHeight + "rpx",
              borderRadius: $props.sliderRadius,
              bottom: $props.bottom,
              background: $options.getSliderBgColor,
              marginBottom: $props.bottom == "50%" ? "-" + $props.sliderHeight / 2 + "rpx" : 0
            })
          },
          null,
          4
          /* STYLE */
        )) : vue.createCommentVNode("v-if", true)
      ],
      6
      /* CLASS, STYLE */
    )) : vue.createCommentVNode("v-if", true);
  }
  const __easycom_0$2 = /* @__PURE__ */ _export_sfc(_sfc_main$j, [["render", _sfc_render$i], ["__scopeId", "data-v-00f3d717"], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/components/thorui/tui-tabs/tui-tabs.vue"]]);
  const _sfc_main$i = {
    name: "tuiBottomPopup",
    emits: ["close"],
    props: {
      //是否需要mask
      mask: {
        type: Boolean,
        default: true
      },
      //控制显示
      show: {
        type: Boolean,
        default: false
      },
      //背景颜色
      backgroundColor: {
        type: String,
        default: "#fff"
      },
      //高度 rpx
      height: {
        type: Number,
        default: 0
      },
      //设置圆角
      radius: {
        type: Boolean,
        default: true
      },
      zIndex: {
        type: [Number, String],
        default: 997
      },
      maskZIndex: {
        type: [Number, String],
        default: 996
      },
      //弹层显示时，垂直方向移动的距离
      translateY: {
        type: String,
        default: "0"
      },
      //是否需要判断底部安全区域（主要针对iphonex以上机型）
      isSafeArea: {
        type: Boolean,
        default: true
      }
    },
    methods: {
      handleClose() {
        if (!this.show) {
          return;
        }
        this.$emit("close", {});
      }
    }
  };
  function _sfc_render$h(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock(
      "view",
      {
        onTouchmove: _cache[1] || (_cache[1] = vue.withModifiers(() => {
        }, ["stop", "prevent"]))
      },
      [
        vue.createElementVNode(
          "view",
          {
            class: vue.normalizeClass(["tui-popup-class tui-bottom-popup", { "tui-popup-show": $props.show, "tui-popup-radius": $props.radius, "tui-bp__safearea": $props.isSafeArea }]),
            style: vue.normalizeStyle({ background: $props.backgroundColor, height: $props.height ? $props.height + "rpx" : "auto", zIndex: $props.zIndex, transform: `translate3d(0, ${$props.show ? $props.translateY : "100%"}, 0)` })
          },
          [
            vue.renderSlot(_ctx.$slots, "default", {}, void 0, true)
          ],
          6
          /* CLASS, STYLE */
        ),
        $props.mask ? (vue.openBlock(), vue.createElementBlock(
          "view",
          {
            key: 0,
            class: vue.normalizeClass(["tui-popup-mask", [$props.show ? "tui-mask-show" : ""]]),
            style: vue.normalizeStyle({ zIndex: $props.maskZIndex }),
            onClick: _cache[0] || (_cache[0] = (...args) => $options.handleClose && $options.handleClose(...args))
          },
          null,
          6
          /* CLASS, STYLE */
        )) : vue.createCommentVNode("v-if", true)
      ],
      32
      /* NEED_HYDRATION */
    );
  }
  const __easycom_1$2 = /* @__PURE__ */ _export_sfc(_sfc_main$i, [["render", _sfc_render$h], ["__scopeId", "data-v-80e2c574"], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/components/thorui/tui-bottom-popup/tui-bottom-popup.vue"]]);
  const _sfc_main$h = {
    name: "tuiWingBlank",
    emits: ["click"],
    props: {
      //small、default、large
      size: {
        type: String,
        default: "default"
      },
      gap: {
        type: [Number, String],
        default: 0
      },
      background: {
        type: String,
        default: "transparent"
      },
      radius: {
        type: [Number, String],
        default: 0
      },
      marginTop: {
        type: [Number, String],
        default: 0
      },
      marginBottom: {
        type: [Number, String],
        default: 0
      }
    },
    computed: {
      getPadding() {
        let styles = "";
        const padding = Number(this.gap);
        if (padding && padding > 0) {
          styles += `padding:0 ${padding}rpx;`;
        }
        return styles;
      },
      getStyles() {
        let styles = `background:${this.background};border-radius:${this.radius}rpx;margin-top:${this.marginTop}rpx;margin-bottom:${this.marginBottom}rpx;`;
        styles += this.getPadding;
        return styles;
      }
    },
    methods: {
      handleClick() {
        this.$emit("click");
      }
    }
  };
  function _sfc_render$g(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock(
      "view",
      {
        class: vue.normalizeClass(["tui-wing__blank", [$props.radius ? "tui-wingblank__hidden" : "", $options.getPadding ? "" : `tui-wingblank__${$props.size}`]]),
        style: vue.normalizeStyle($options.getStyles),
        onClick: _cache[0] || (_cache[0] = (...args) => $options.handleClick && $options.handleClick(...args))
      },
      [
        vue.renderSlot(_ctx.$slots, "default", {}, void 0, true)
      ],
      6
      /* CLASS, STYLE */
    );
  }
  const __easycom_0$1 = /* @__PURE__ */ _export_sfc(_sfc_main$h, [["render", _sfc_render$g], ["__scopeId", "data-v-4d610ac0"], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/components/thorui/tui-wing-blank/tui-wing-blank.vue"]]);
  const _sfc_main$g = {
    name: "Empty",
    components: {
      tuiWingBlank: __easycom_0$1
    },
    data() {
      return {
        itemimg: "../../../static/ann/index/danganadd.png"
      };
    }
  };
  function _sfc_render$f(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_tui_wing_blank = resolveEasycom(vue.resolveDynamicComponent("tui-wing-blank"), __easycom_0$1);
    return vue.openBlock(), vue.createBlock(_component_tui_wing_blank, {
      style: { "padding": "120rpx 200rpx" },
      gap: "64"
    }, {
      default: vue.withCtx(() => [
        vue.createElementVNode("view", { class: "mainview" }, [
          vue.createElementVNode("view", { class: "tui-placeholder" }, [
            vue.createElementVNode("view", null, [
              vue.createElementVNode("image", {
                src: $data.itemimg,
                class: "tui-category-img",
                mode: "scaleToFill"
              }, null, 8, ["src"])
            ]),
            vue.createElementVNode("view", { class: "addtype" }, "添加类型")
          ])
        ]),
        vue.createElementVNode("view", { class: "titlemsg" }, [
          vue.createTextVNode(" 还没有创建任何档案"),
          vue.createElementVNode("br"),
          vue.createTextVNode("可点击上方按钮进行添加～")
        ])
      ]),
      _: 1
      /* STABLE */
    });
  }
  const PagesAnnDanganEmpty = /* @__PURE__ */ _export_sfc(_sfc_main$g, [["render", _sfc_render$f], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/pages/ann/dangan/empty.vue"]]);
  const json = {
    category: [
      {
        id: 1,
        img: "../../../static/ann/index/dangan.png",
        name: "猪",
        page: "/pages/ann/dangan/dlist"
      },
      {
        id: 2,
        img: "../../../static/ann/index/jizhang.png",
        name: "牛",
        page: "/pages/ann/dangan/dlist"
      },
      {
        id: 3,
        img: "../../../static/ann/index/jizhang.png",
        name: "羊",
        page: "/pages/ann/dangan/dlist"
      },
      {
        id: 4,
        img: "../../../static/ann/index/jizhang.png",
        name: "鸡",
        page: "/pages/ann/dangan/dlist"
      }
    ],
    basecategorys: [
      {
        "id": 1,
        "animaltype": 1,
        "isbreedhome": 0,
        "img": "../../../static/ann/index/dangan.png",
        "name": "猪"
      },
      {
        "id": 2,
        "animaltype": 1,
        "isbreedhome": 0,
        "img": "../../../static/ann/index/dangan.png",
        "name": "牛"
      },
      {
        "id": 3,
        "animaltype": 1,
        "isbreedhome": 0,
        "img": "../../../static/ann/index/dangan.png",
        "name": "羊"
      },
      {
        "id": 4,
        "animaltype": 1,
        "isbreedhome": 0,
        "img": "../../../static/ann/index/dangan.png",
        "name": "马"
      },
      {
        "id": 5,
        "animaltype": 1,
        "isbreedhome": 0,
        "img": "../../../static/ann/index/dangan.png",
        "name": "驴"
      },
      {
        "id": 6,
        "animaltype": 1,
        "isbreedhome": 0,
        "img": "../../../static/ann/index/dangan.png",
        "name": "骡"
      },
      {
        "id": 7,
        "animaltype": 1,
        "isbreedhome": 0,
        "img": "../../../static/ann/index/dangan.png",
        "name": "兔"
      },
      {
        "id": 8,
        "animaltype": 1,
        "isbreedhome": 0,
        "img": "../../../static/ann/index/dangan.png",
        "name": "狗"
      },
      {
        "id": 9,
        "animaltype": 1,
        "isbreedhome": 1,
        "img": "../../../static/ann/index/dangan.png",
        "name": "骆驼"
      },
      {
        "id": 10,
        "animaltype": 1,
        "isbreedhome": 1,
        "img": "../../../static/ann/index/dangan.png",
        "name": "鹿"
      },
      {
        "id": 11,
        "animaltype": 1,
        "isbreedhome": 1,
        "img": "../../../static/ann/index/dangan.png",
        "name": "蛇"
      },
      {
        "id": 12,
        "animaltype": 2,
        "isbreedhome": 0,
        "img": "../../../static/ann/index/dangan.png",
        "name": "鸡"
      },
      {
        "id": 13,
        "animaltype": 2,
        "isbreedhome": 0,
        "img": "../../../static/ann/index/dangan.png",
        "name": "鸭"
      },
      {
        "id": 14,
        "animaltype": 2,
        "isbreedhome": 0,
        "img": "../../../static/ann/index/dangan.png",
        "name": "鹅"
      },
      {
        "id": 15,
        "animaltype": 2,
        "isbreedhome": 1,
        "img": "../../../static/ann/index/dangan.png",
        "name": "鸽"
      },
      {
        "id": 16,
        "animaltype": 2,
        "isbreedhome": 1,
        "img": "../../../static/ann/index/dangan.png",
        "name": "鹌鹑"
      },
      {
        "id": 17,
        "animaltype": 2,
        "isbreedhome": 1,
        "img": "../../../static/ann/index/dangan.png",
        "name": "鹦鹉"
      },
      {
        "id": 18,
        "animaltype": 2,
        "isbreedhome": 1,
        "img": "../../../static/ann/index/dangan.png",
        "name": "斑鸠"
      },
      {
        "id": 19,
        "animaltype": 2,
        "isbreedhome": 1,
        "img": "../../../static/ann/index/dangan.png",
        "name": "鸸鹋"
      },
      {
        "id": 20,
        "animaltype": 2,
        "isbreedhome": 1,
        "img": "../../../static/ann/index/dangan.png",
        "name": "鸵鸟"
      },
      {
        "id": 21,
        "animaltype": 3,
        "isbreedhome": 1,
        "img": "../../../static/ann/index/dangan.png",
        "name": "牛蛙"
      },
      {
        "id": 22,
        "animaltype": 3,
        "isbreedhome": 1,
        "img": "../../../static/ann/index/dangan.png",
        "name": "鳄鱼"
      },
      {
        "id": 23,
        "animaltype": 3,
        "isbreedhome": 1,
        "img": "../../../static/ann/index/dangan.png",
        "name": "貂"
      },
      {
        "id": 24,
        "animaltype": 3,
        "isbreedhome": 1,
        "img": "../../../static/ann/index/dangan.png",
        "name": "狐"
      },
      {
        "id": 25,
        "animaltype": 3,
        "isbreedhome": 1,
        "img": "../../../static/ann/index/dangan.png",
        "name": "貉"
      }
    ]
  };
  const dindex = {
    components: {
      tuiBottomPopup: __easycom_1$2,
      Empty: PagesAnnDanganEmpty
    },
    data() {
      return {
        visible: true,
        // 根据您的逻辑设定 visible 的值
        category: json.category,
        basecategorys: json.basecategorys,
        popupShow: false,
        // 弹出框显示状态
        itemimg: "../../../static/ann/index/danganadd.png",
        currentTab: 0,
        // 默认选中的页签索引
        tabs: [
          // 根据您的需求动态添加页签
          {
            name: "家畜",
            backgroundColor: "rgb(253 250 250)",
            items: []
          },
          // 每个页签包含一个 items 数组，用于存储该页签下的动物数据
          {
            name: "家禽",
            backgroundColor: "rgb(253 250 250)",
            items: []
          },
          {
            name: "水产",
            backgroundColor: "rgb(253 250 250)",
            items: []
          }
        ]
      };
    },
    mounted() {
      this.basecategorys.forEach((item) => {
        if (item.animaltype === 1) {
          this.tabs[0].items.push(item);
        } else if (item.animaltype === 2) {
          this.tabs[1].items.push(item);
        } else if (item.animaltype === 3) {
          this.tabs[2].items.push(item);
        }
      });
      if (this.category.length <= 0) {
        this.visible = false;
      }
    },
    methods: {
      //调用此方法显示弹层
      showPopup: function() {
        this.popupShow = true;
      },
      hiddenPopup: function() {
        this.popupShow = false;
      },
      selected: function() {
        this.hiddenPopup();
      },
      change(e) {
        this.currentTab = e.index;
      },
      navigateTo(item) {
        uni.navigateTo({
          url: item.page + "?name=" + item.id
        });
      }
    }
  };
  const _sfc_main$f = {
    mixins: [dindex]
  };
  function _sfc_render$e(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_tui_tabs = resolveEasycom(vue.resolveDynamicComponent("tui-tabs"), __easycom_0$2);
    const _component_tui_bottom_popup = resolveEasycom(vue.resolveDynamicComponent("tui-bottom-popup"), __easycom_1$2);
    const _component_Empty = vue.resolveComponent("Empty");
    return vue.openBlock(), vue.createElementBlock(
      vue.Fragment,
      null,
      [
        vue.withDirectives(vue.createElementVNode(
          "view",
          null,
          [
            vue.createCommentVNode(" 在 visible 为 true 时显示的内容 "),
            vue.createElementVNode("view", { class: "tui-product-category" }, [
              (vue.openBlock(true), vue.createElementBlock(
                vue.Fragment,
                null,
                vue.renderList(_ctx.category, (item, index2) => {
                  return vue.openBlock(), vue.createElementBlock("view", {
                    class: "tui-item-category",
                    key: index2,
                    "data-key": item.name,
                    onClick: ($event) => _ctx.navigateTo(item)
                  }, [
                    vue.createElementVNode(
                      "view",
                      { class: "span-dajz-category" },
                      vue.toDisplayString(item.name),
                      1
                      /* TEXT */
                    )
                  ], 8, ["data-key", "onClick"]);
                }),
                128
                /* KEYED_FRAGMENT */
              ))
            ]),
            vue.createElementVNode("view", { class: "tui-product-addcategory" }, [
              vue.createElementVNode("view", { class: "tui-item-addcategory" }, [
                vue.createElementVNode("view", { class: "span-dajz-addcategory" }, [
                  vue.createElementVNode("image", {
                    src: _ctx.itemimg,
                    class: "tui-img-addcategory",
                    onClick: _cache[0] || (_cache[0] = (...args) => _ctx.showPopup && _ctx.showPopup(...args))
                  }, null, 8, ["src"])
                ])
              ])
            ]),
            vue.createCommentVNode(" 弹出框 "),
            vue.createVNode(_component_tui_bottom_popup, {
              class: "popup",
              zIndex: 1002,
              maskZIndex: 1001,
              show: _ctx.popupShow,
              onClose: _ctx.hiddenPopup
            }, {
              default: vue.withCtx(() => [
                (vue.openBlock(true), vue.createElementBlock(
                  vue.Fragment,
                  null,
                  vue.renderList(_ctx.tabs, (tab, tabIndex) => {
                    return vue.openBlock(), vue.createElementBlock(
                      "view",
                      {
                        key: tabIndex,
                        class: "tui-product-popup",
                        style: vue.normalizeStyle({ display: _ctx.currentTab === tabIndex ? "flex" : "none", backgroundColor: tab.backgroundColor })
                      },
                      [
                        (vue.openBlock(true), vue.createElementBlock(
                          vue.Fragment,
                          null,
                          vue.renderList(tab.items, (item, itemIndex) => {
                            return vue.openBlock(), vue.createElementBlock("view", {
                              class: "tui-item-popup",
                              key: itemIndex,
                              "data-key": item.name,
                              onClick: ($event) => _ctx.selected(item)
                            }, [
                              vue.createElementVNode(
                                "view",
                                { class: "span-dajz-popup" },
                                vue.toDisplayString(item.name),
                                1
                                /* TEXT */
                              )
                            ], 8, ["data-key", "onClick"]);
                          }),
                          128
                          /* KEYED_FRAGMENT */
                        ))
                      ],
                      4
                      /* STYLE */
                    );
                  }),
                  128
                  /* KEYED_FRAGMENT */
                )),
                vue.createVNode(_component_tui_tabs, {
                  tabs: _ctx.tabs,
                  currentTab: _ctx.currentTab,
                  itemWidth: "50%",
                  onChange: _ctx.change
                }, null, 8, ["tabs", "currentTab", "onChange"])
              ]),
              _: 1
              /* STABLE */
            }, 8, ["show", "onClose"])
          ],
          512
          /* NEED_PATCH */
        ), [
          [vue.vShow, _ctx.visible]
        ]),
        vue.withDirectives(vue.createElementVNode(
          "view",
          null,
          [
            vue.createCommentVNode(" 在 visible 为 false 时显示的内容 "),
            vue.createVNode(_component_Empty)
          ],
          512
          /* NEED_PATCH */
        ), [
          [vue.vShow, !_ctx.visible]
        ])
      ],
      64
      /* STABLE_FRAGMENT */
    );
  }
  const PagesAnnDanganDindex = /* @__PURE__ */ _export_sfc(_sfc_main$f, [["render", _sfc_render$e], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/pages/ann/dangan/dindex.vue"]]);
  const _sfc_main$e = {
    name: "tui-radio",
    emits: ["change"],
    behaviors: ["uni://form-field"],
    options: {
      virtualHost: true
    },
    props: {
      value: {
        type: String,
        default: ""
      },
      checked: {
        type: Boolean,
        default: false
      },
      disabled: {
        type: Boolean,
        default: false
      },
      //radio选中背景颜色
      color: {
        type: String,
        default: ""
      },
      //radio未选中时边框颜色
      borderColor: {
        type: String,
        default: "#ccc"
      },
      //是否只展示对号，无边框背景
      isCheckMark: {
        type: Boolean,
        default: false
      },
      //对号颜色
      checkMarkColor: {
        type: String,
        default: "#fff"
      },
      scaleRatio: {
        type: [Number, String],
        default: 1
      }
    },
    computed: {
      getColor() {
        return this.color || uni && uni.$tui && uni.$tui.color.primary || "#5677fc";
      }
    },
    created() {
      this.val = this.checked;
      this.group = this.getParent();
      if (this.group) {
        this.group.childrens.push(this);
        if (this.group.value) {
          this.val = this.value === this.group.value;
        }
        if (this.group.modelValue) {
          this.val = this.value === this.group.modelValue;
        }
      }
      this.label = this.getParent("tui-label");
      if (this.label) {
        this.label.childrens.push(this);
      }
    },
    watch: {
      checked(newVal) {
        this.val = newVal;
      },
      val(newVal) {
        if (newVal && this.group) {
          this.group.changeValue(this.value, this);
        }
      }
    },
    data() {
      let nvue = false;
      return {
        val: false,
        nvue
      };
    },
    methods: {
      getBackgroundStyle(val, isCheckMark) {
        let color2 = val ? this.getColor : "#fff";
        if (isCheckMark) {
          color2 = "transparent";
        }
        return color2;
      },
      getBorderStyle(val, isCheckMark) {
        let color2 = val ? this.getColor : this.borderColor;
        if (isCheckMark) {
          color2 = "transparent";
        }
        return `1px solid ${color2}`;
      },
      radioChange(e) {
        if (this.disabled || this.val)
          return;
        this.val = true;
        this.$emit("change", {
          checked: this.val,
          value: this.value
        });
      },
      getParent(name = "tui-radio-group") {
        let parent = this.$parent;
        let parentName = parent.$options.name;
        while (parentName !== name) {
          parent = parent.$parent;
          if (!parent)
            return false;
          parentName = parent.$options.name;
        }
        return parent;
      },
      labelClick() {
        this.radioChange();
      }
    }
  };
  function _sfc_render$d(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock(
      "view",
      {
        class: vue.normalizeClass(["tui-checkbox__input", { "tui-checkbox__disabled": $props.disabled }]),
        style: vue.normalizeStyle({ backgroundColor: $options.getBackgroundStyle($data.val, $props.isCheckMark), border: $options.getBorderStyle($data.val, $props.isCheckMark), zoom: $data.nvue ? 1 : $props.scaleRatio, transform: `scale(${$data.nvue ? $props.scaleRatio : 1})` }),
        onClick: _cache[0] || (_cache[0] = vue.withModifiers((...args) => $options.radioChange && $options.radioChange(...args), ["stop"]))
      },
      [
        $data.val ? (vue.openBlock(), vue.createElementBlock(
          "view",
          {
            key: 0,
            class: "tui-check__mark",
            style: vue.normalizeStyle({ borderBottomColor: $props.checkMarkColor, borderRightColor: $props.checkMarkColor })
          },
          null,
          4
          /* STYLE */
        )) : vue.createCommentVNode("v-if", true),
        vue.createElementVNode("radio", {
          class: "tui-radio__hidden",
          style: { "position": "absolute", "opacity": "0" },
          hidden: "",
          color: $options.getColor,
          disabled: $props.disabled,
          value: $props.value,
          checked: $data.val
        }, null, 8, ["color", "disabled", "value", "checked"])
      ],
      6
      /* CLASS, STYLE */
    );
  }
  const __easycom_1$1 = /* @__PURE__ */ _export_sfc(_sfc_main$e, [["render", _sfc_render$d], ["__scopeId", "data-v-999d2439"], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/components/thorui/tui-radio/tui-radio.vue"]]);
  const _sfc_main$d = {
    name: "tui-label",
    props: {
      padding: {
        type: String,
        default: "0"
      },
      margin: {
        type: String,
        default: "0"
      },
      isFull: {
        type: Boolean,
        default: false
      }
    },
    created() {
      this.childrens = [];
    },
    methods: {
      onClick() {
        if (this.childrens && this.childrens.length > 0) {
          for (let child of this.childrens) {
            child.labelClick();
          }
        }
      }
    }
  };
  function _sfc_render$c(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock(
      "view",
      {
        class: vue.normalizeClass(["tui-label__box", { "tui-label__full": $props.isFull }]),
        style: vue.normalizeStyle({ padding: $props.padding, margin: $props.margin }),
        onClick: _cache[0] || (_cache[0] = vue.withModifiers((...args) => $options.onClick && $options.onClick(...args), ["stop"]))
      },
      [
        vue.renderSlot(_ctx.$slots, "default", {}, void 0, true)
      ],
      6
      /* CLASS, STYLE */
    );
  }
  const __easycom_2 = /* @__PURE__ */ _export_sfc(_sfc_main$d, [["render", _sfc_render$c], ["__scopeId", "data-v-b75e1699"], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/components/thorui/tui-label/tui-label.vue"]]);
  const _sfc_main$c = {
    name: "tui-radio-group",
    emits: ["change", "input", "update:modelValue"],
    props: {
      name: {
        type: String,
        default: ""
      },
      modelValue: {
        type: String,
        default: ""
      },
      value: {
        type: String,
        default: ""
      }
    },
    data() {
      return {
        val: ""
      };
    },
    watch: {
      modelValue(val) {
        this.modelChange(val);
      },
      value(val) {
        this.modelChange(val);
      }
    },
    created() {
      this.childrens = [];
    },
    methods: {
      radioChange(e) {
        this.$emit("change", e);
        this.$emit("input", e.detail.value);
        this.$emit("update:modelValue", e.detail.value);
      },
      changeValue(value, target) {
        if (this.val === value)
          return;
        this.val = value;
        this.childrens.forEach((item) => {
          if (item !== target) {
            item.val = false;
          }
        });
        let e = {
          detail: {
            value
          }
        };
        this.radioChange(e);
      },
      modelChange(value) {
        this.childrens.forEach((item) => {
          if (item.value === value) {
            item.val = true;
          } else {
            item.val = false;
          }
        });
      }
    }
  };
  function _sfc_render$b(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock("radio-group", { name: $props.name }, [
      vue.renderSlot(_ctx.$slots, "default")
    ], 8, ["name"]);
  }
  const __easycom_3 = /* @__PURE__ */ _export_sfc(_sfc_main$c, [["render", _sfc_render$b], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/components/thorui/tui-radio-group/tui-radio-group.vue"]]);
  const _sfc_main$b = {
    name: "tuiNumberbox",
    emits: ["change"],
    props: {
      value: {
        type: [Number, String],
        default: 1
      },
      //最小值
      min: {
        type: Number,
        default: 1
      },
      //最大值
      max: {
        type: Number,
        default: 99
      },
      //迈步大小 1 1.1 10...
      step: {
        type: Number,
        default: 1
      },
      //是否禁用操作
      disabled: {
        type: Boolean,
        default: false
      },
      iconBgColor: {
        type: String,
        default: "transparent"
      },
      radius: {
        type: String,
        default: "50%"
      },
      //加减图标大小 rpx
      iconSize: {
        type: Number,
        default: 22
      },
      iconColor: {
        type: String,
        default: "#666666"
      },
      //input 高度
      height: {
        type: Number,
        default: 42
      },
      //input 宽度
      width: {
        type: Number,
        default: 80
      },
      size: {
        type: Number,
        default: 28
      },
      //input 背景颜色
      backgroundColor: {
        type: String,
        default: "#F5F5F5"
      },
      //input 字体颜色
      color: {
        type: String,
        default: "#333"
      },
      //索引值，列表中使用
      index: {
        type: [Number, String],
        default: 0
      },
      //自定义参数
      custom: {
        type: [Number, String],
        default: 0
      }
    },
    created() {
      this.inputValue = +this.value;
    },
    data() {
      return {
        inputValue: 0
      };
    },
    watch: {
      value(val) {
        this.inputValue = +val;
      }
    },
    methods: {
      getScale(val, step) {
        let scale = 1;
        let scaleVal = 1;
        if (!Number.isInteger(step)) {
          scale = Math.pow(10, (step + "").split(".")[1].length);
        }
        if (!Number.isInteger(val)) {
          scaleVal = Math.pow(10, (val + "").split(".")[1].length);
        }
        return Math.max(scale, scaleVal);
      },
      calcNum: function(type) {
        if (this.disabled || this.inputValue == this.min && type === "reduce" || this.inputValue == this.max && type === "plus") {
          return;
        }
        const scale = this.getScale(Number(this.inputValue), Number(this.step));
        let num = Number(this.inputValue) * scale;
        let step = this.step * scale;
        if (type === "reduce") {
          num -= step;
        } else if (type === "plus") {
          num += step;
        }
        let value = Number((num / scale).toFixed(String(scale).length - 1));
        if (value < this.min) {
          value = this.min;
        } else if (value > this.max) {
          value = this.max;
        }
        this.handleChange(value, type);
      },
      plus: function() {
        this.calcNum("plus");
      },
      reduce: function() {
        this.calcNum("reduce");
      },
      blur: function(e) {
        let value = e.detail.value;
        if (value) {
          if (~value.indexOf(".") && Number.isInteger(this.step) && Number.isInteger(Number(value))) {
            value = value.split(".")[0];
          }
          value = Number(value);
          if (value > this.max) {
            value = this.max;
          } else if (value < this.min) {
            value = this.min;
          }
        } else {
          value = this.min;
        }
        if (value == this.value && value != this.inputValue || !e.detail.value) {
          this.inputValue = value;
        }
        this.handleChange(value, "blur");
      },
      handleChange(value, type) {
        if (this.disabled)
          return;
        this.$emit("change", {
          value: Number(value),
          type,
          index: this.index,
          custom: this.custom
        });
      }
    }
  };
  function _sfc_render$a(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock("view", { class: "tui-numberbox" }, [
      vue.createElementVNode(
        "view",
        {
          class: vue.normalizeClass(["tui-num__icon__box", [$props.disabled || $props.min >= $data.inputValue ? "tui-disabled" : ""]]),
          style: vue.normalizeStyle({ background: $props.iconBgColor, borderRadius: $props.radius }),
          onClick: _cache[0] || (_cache[0] = (...args) => $options.reduce && $options.reduce(...args))
        },
        [
          vue.createElementVNode(
            "text",
            {
              class: "tui-numbox-icon tui-num__icon-reduce",
              style: vue.normalizeStyle({ color: $props.iconColor, fontSize: $props.iconSize + "rpx", lineHeight: $props.iconSize + "rpx" })
            },
            null,
            4
            /* STYLE */
          )
        ],
        6
        /* CLASS, STYLE */
      ),
      vue.withDirectives(vue.createElementVNode("input", {
        type: "number",
        "onUpdate:modelValue": _cache[1] || (_cache[1] = ($event) => $data.inputValue = $event),
        disabled: $props.disabled,
        onBlur: _cache[2] || (_cache[2] = (...args) => $options.blur && $options.blur(...args)),
        class: "tui-num-input",
        style: vue.normalizeStyle({ color: $props.color, fontSize: $props.size + "rpx", background: $props.backgroundColor, height: $props.height + "rpx", minHeight: $props.height + "rpx", width: $props.width + "rpx" })
      }, null, 44, ["disabled"]), [
        [vue.vModelText, $data.inputValue]
      ]),
      vue.createElementVNode(
        "view",
        {
          class: vue.normalizeClass(["tui-num__icon__box", [$props.disabled || $data.inputValue >= $props.max ? "tui-disabled" : ""]]),
          style: vue.normalizeStyle({ background: $props.iconBgColor, borderRadius: $props.radius }),
          onClick: _cache[3] || (_cache[3] = (...args) => $options.plus && $options.plus(...args))
        },
        [
          vue.createElementVNode(
            "text",
            {
              class: "tui-numbox-icon tui-num__icon-plus",
              style: vue.normalizeStyle({ color: $props.iconColor, fontSize: $props.iconSize + "rpx", lineHeight: $props.iconSize + "rpx" })
            },
            null,
            4
            /* STYLE */
          )
        ],
        6
        /* CLASS, STYLE */
      )
    ]);
  }
  const __easycom_4 = /* @__PURE__ */ _export_sfc(_sfc_main$b, [["render", _sfc_render$a], ["__scopeId", "data-v-fd01c0e4"], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/components/thorui/tui-numberbox/tui-numberbox.vue"]]);
  const _sfc_main$a = {
    name: "tuiDatetime",
    emits: ["cancel", "confirm"],
    props: {
      //0-年 1-日期+时间（年月日+时分） 2-日期(年月日) 3-日期(年月) 4-时间（时分） 5-时分秒 6-分秒 7-年月日 时分秒 8-年月日+小时
      type: {
        type: [Number, String],
        default: 1
      },
      //年份区间
      startYear: {
        type: Number,
        default: 1980
      },
      //年份区间
      endYear: {
        type: Number,
        default: 2050
      },
      hoursData: {
        type: Array,
        default() {
          return [];
        }
      },
      minutesData: {
        type: Array,
        default() {
          return [];
        }
      },
      secondsData: {
        type: Array,
        default() {
          return [];
        }
      },
      //显示标题
      title: {
        type: String,
        default: ""
      },
      //标题字体大小
      titleSize: {
        type: [Number, String],
        default: 34
      },
      //标题字体颜色
      titleColor: {
        type: String,
        default: "#333"
      },
      //"取消"字体颜色
      cancelColor: {
        type: String,
        default: "#888"
      },
      //"确定"字体颜色
      color: {
        type: String,
        default: ""
      },
      //设置默认显示日期 2019-08-01 || 2019-08-01 17:01 || 2019/08/01
      setDateTime: {
        type: String,
        default: ""
      },
      //单位置顶
      unitTop: {
        type: Boolean,
        default: false
      },
      //圆角设置
      radius: {
        type: Boolean,
        default: false
      },
      //头部背景色
      headerBackground: {
        type: String,
        default: "#fff"
      },
      //根据实际调整，不建议使用深颜色
      bodyBackground: {
        type: String,
        default: "#fff"
      },
      //单位置顶时，单位条背景色
      unitBackground: {
        type: String,
        default: "#fff"
      },
      height: {
        type: [Number, String],
        default: 520
      },
      //点击遮罩 是否可关闭
      maskClosable: {
        type: Boolean,
        default: true
      },
      zIndex: {
        type: [Number, String],
        default: 998
      }
    },
    data() {
      let immediate = true;
      return {
        immediate,
        isShow: false,
        years: [],
        months: [],
        days: [],
        hours: [],
        minutes: [],
        seconds: [],
        year: 0,
        month: 0,
        day: 0,
        hour: 0,
        minute: 0,
        second: 0,
        startDate: "",
        endDate: "",
        value: [],
        isEnd: true,
        firstShow: false
      };
    },
    mounted() {
      this.$nextTick(() => {
        setTimeout(() => {
          this.initData();
        }, 20);
      });
    },
    computed: {
      yearOrMonth() {
        return `${this.year}-${this.month}`;
      },
      propsChange() {
        return `${this.type}-${this.startYear}-${this.endYear}-${this.hoursData}-${this.minutesData}-${this.secondsData}`;
      },
      getColor() {
        return this.color || uni && uni.$tui && uni.$tui.color.primary || "#5677fc";
      },
      getMaskZIndex() {
        return Number(this.zIndex) + 1;
      },
      getPickerZIndex() {
        return Number(this.zIndex) + 2;
      }
    },
    watch: {
      yearOrMonth() {
        this.setDays();
      },
      propsChange() {
        this.$nextTick(() => {
          setTimeout(() => {
            this.initData();
          }, 20);
        });
      },
      setDateTime(val) {
        if (val && val !== true) {
          setTimeout(() => {
            this.initData();
          }, 20);
        }
      }
    },
    methods: {
      stop() {
      },
      formatNum: function(num) {
        return num < 10 ? "0" + num : num + "";
      },
      generateArray: function(start, end) {
        return Array.from(new Array(end + 1).keys()).slice(start);
      },
      getIndex: function(arr, val) {
        if (!arr || arr.length === 0)
          return 0;
        let index2 = arr.indexOf(val);
        return ~index2 ? index2 : 0;
      },
      getCharCount(str) {
        let regex = new RegExp("/", "g");
        let result = str.match(regex);
        return !result ? 0 : result.length;
      },
      //日期时间处理
      initSelectValue() {
        let fdate = "";
        if (this.setDateTime && this.setDateTime !== true) {
          fdate = this.setDateTime.replace(/\-/g, "/");
          if (this.type == 3 && this.getCharCount(fdate) === 1) {
            fdate += "/01";
          } else if (this.type == 0) {
            fdate += "/01/01";
          }
          fdate = fdate && fdate.indexOf("/") == -1 ? `2023/01/01 ${fdate}` : fdate;
        }
        let time = null;
        if (fdate)
          time = new Date(fdate);
        else
          time = /* @__PURE__ */ new Date();
        this.year = time.getFullYear();
        this.month = time.getMonth() + 1;
        this.day = time.getDate();
        this.hour = time.getHours();
        this.minute = time.getMinutes();
        this.second = time.getSeconds();
      },
      initData() {
        this.initSelectValue();
        const type = Number(this.type);
        switch (type) {
          case 0:
            this.setYears();
            break;
          case 1:
            this.setYears();
            this.setMonths();
            this.setDays();
            this.setHours();
            this.setMinutes();
            break;
          case 2:
            this.setYears();
            this.setMonths();
            this.setDays();
            break;
          case 3:
            this.setYears();
            this.setMonths();
            break;
          case 4:
            this.setHours();
            this.setMinutes();
            break;
          case 5:
            this.setHours();
            this.setMinutes();
            this.setSeconds();
            break;
          case 6:
            this.setMinutes();
            this.setSeconds();
            break;
          case 7:
            this.setYears();
            this.setMonths();
            this.setDays();
            this.setHours();
            this.setMinutes();
            this.setSeconds();
            break;
          case 8:
            this.setYears();
            this.setMonths();
            this.setDays();
            this.setHours();
            break;
        }
        this.$nextTick(() => {
          setTimeout(() => {
            this.setDefaultValues();
          }, 0);
        });
      },
      setDefaultValues() {
        let vals = [];
        const year = this.getIndex(this.years, this.year);
        const month = this.getIndex(this.months, this.month);
        const day = this.getIndex(this.days, this.day);
        const hour = this.getIndex(this.hours, this.hour);
        const minute = this.getIndex(this.minutes, this.minute);
        const second = this.getIndex(this.seconds, this.second);
        const type = Number(this.type);
        switch (type) {
          case 0:
            vals = [year];
          case 1:
            vals = [year, month, day, hour, minute];
            break;
          case 2:
            vals = [year, month, day];
            break;
          case 3:
            vals = [year, month];
            break;
          case 4:
            vals = [hour, minute];
            break;
          case 5:
            vals = [hour, minute, second];
            break;
          case 6:
            vals = [minute, second];
            break;
          case 7:
            vals = [year, month, day, hour, minute, second];
            break;
          case 8:
            vals = [year, month, day, hour];
            break;
        }
        if (this.value.join(",") === vals.join(","))
          return;
        setTimeout(() => {
          this.value = vals;
        }, 200);
      },
      setYears() {
        this.years = this.generateArray(this.startYear, this.endYear);
      },
      setMonths() {
        this.months = this.generateArray(1, 12);
      },
      setDays() {
        if (this.type == 3 || this.type == 4)
          return;
        let totalDays = new Date(this.year, this.month, 0).getDate();
        totalDays = !totalDays || totalDays < 1 ? 1 : totalDays;
        this.days = this.generateArray(1, totalDays);
      },
      setHours() {
        if (this.hoursData && this.hoursData.length > 0) {
          this.hours = this.hoursData;
        } else {
          this.hours = this.generateArray(0, 23);
        }
      },
      setMinutes() {
        if (this.minutesData && this.minutesData.length > 0) {
          this.minutes = this.minutesData;
        } else {
          this.minutes = this.generateArray(0, 59);
        }
      },
      setSeconds() {
        if (this.secondsData && this.secondsData.length > 0) {
          this.seconds = this.secondsData;
        } else {
          this.seconds = this.generateArray(0, 59);
        }
      },
      show() {
        this.firstShow = true;
        setTimeout(() => {
          this.isShow = true;
          this.value = [];
          this.$nextTick(() => {
            setTimeout(() => {
              this.value = [...this.value];
            }, 50);
          });
        }, 50);
      },
      hide() {
        this.isShow = false;
        this.$emit("cancel", {});
      },
      maskClick() {
        if (!this.maskClosable)
          return;
        this.hide();
      },
      change(e) {
        if (!this.firstShow)
          return;
        this.value = e.detail.value;
        const type = Number(this.type);
        switch (type) {
          case 0:
            this.year = this.years[this.value[0]];
            break;
          case 1:
            this.year = this.years[this.value[0]];
            this.month = this.months[this.value[1]];
            this.day = this.days[this.value[2]];
            this.hour = this.hours[this.value[3]];
            this.minute = this.minutes[this.value[4]];
            break;
          case 2:
            this.year = this.years[this.value[0]];
            this.month = this.months[this.value[1]];
            this.day = this.days[this.value[2]];
            break;
          case 3:
            this.year = this.years[this.value[0]];
            this.month = this.months[this.value[1]];
            break;
          case 4:
            this.hour = this.hours[this.value[0]];
            this.minute = this.minutes[this.value[1]];
            break;
          case 5:
            this.hour = this.hours[this.value[0]];
            this.minute = this.minutes[this.value[1]];
            this.second = this.seconds[this.value[2]];
            break;
          case 6:
            this.minute = this.minutes[this.value[0]];
            this.second = this.seconds[this.value[1]];
            break;
          case 7:
            this.year = this.years[this.value[0]];
            this.month = this.months[this.value[1]];
            this.day = this.days[this.value[2]];
            this.hour = this.hours[this.value[3]];
            this.minute = this.minutes[this.value[4]];
            this.second = this.seconds[this.value[5]];
            break;
          case 8:
            this.year = this.years[this.value[0]];
            this.month = this.months[this.value[1]];
            this.day = this.days[this.value[2]];
            this.hour = this.hours[this.value[3]];
            break;
        }
        this.isEnd = true;
      },
      selectResult() {
        let result = {};
        let year = this.year;
        let month = this.formatNum(this.month || 0);
        let day = this.formatNum(this.day || 0);
        let hour = this.formatNum(this.hour || 0);
        let minute = this.formatNum(this.minute || 0);
        let second = this.formatNum(this.second || 0);
        const type = Number(this.type);
        switch (type) {
          case 0:
            result = {
              year,
              result: year
            };
            break;
          case 1:
            result = {
              year,
              month,
              day,
              hour,
              minute,
              result: `${year}-${month}-${day} ${hour}:${minute}`
            };
            break;
          case 2:
            result = {
              year,
              month,
              day,
              result: `${year}-${month}-${day}`
            };
            break;
          case 3:
            result = {
              year,
              month,
              result: `${year}-${month}`
            };
            break;
          case 4:
            result = {
              hour,
              minute,
              result: `${hour}:${minute}`
            };
            break;
          case 5:
            result = {
              hour,
              minute,
              second,
              result: `${hour}:${minute}:${second}`
            };
            break;
          case 6:
            result = {
              minute,
              second,
              result: `${minute}:${second}`
            };
            break;
          case 7:
            result = {
              year,
              month,
              day,
              hour,
              minute,
              second,
              result: `${year}-${month}-${day} ${hour}:${minute}:${second}`
            };
            break;
          case 8:
            result = {
              year,
              month,
              day,
              hour,
              result: `${year}-${month}-${day} ${hour}:00`
            };
            break;
        }
        this.$emit("confirm", result);
      },
      waitFix() {
        if (this.isEnd) {
          this.selectResult();
        } else {
          setTimeout(() => {
            this.waitFix();
          }, 50);
        }
      },
      btnFix() {
        setTimeout(() => {
          this.waitFix();
          this.hide();
        }, 80);
      },
      pickerstart() {
        this.isEnd = false;
      }
    }
  };
  function _sfc_render$9(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock(
      "view",
      {
        class: "tui-datetime-picker",
        style: vue.normalizeStyle({ zIndex: $props.zIndex })
      },
      [
        vue.createElementVNode(
          "view",
          {
            class: vue.normalizeClass(["tui-datetime__mask", { "tui-datetime__mask-show": $data.isShow }]),
            style: vue.normalizeStyle({ zIndex: $options.getMaskZIndex }),
            onTouchmove: _cache[0] || (_cache[0] = vue.withModifiers((...args) => $options.stop && $options.stop(...args), ["stop", "prevent"])),
            catchtouchmove: "stop",
            onClick: _cache[1] || (_cache[1] = (...args) => $options.maskClick && $options.maskClick(...args))
          },
          null,
          38
          /* CLASS, STYLE, NEED_HYDRATION */
        ),
        vue.createElementVNode(
          "view",
          {
            class: vue.normalizeClass(["tui-datetime__header", { "tui-show": $data.isShow }]),
            style: vue.normalizeStyle({ zIndex: $options.getPickerZIndex })
          },
          [
            vue.createElementVNode(
              "view",
              {
                class: vue.normalizeClass(["tui-picker-header", { "tui-date-radius": $props.radius }]),
                style: vue.normalizeStyle({ backgroundColor: $props.headerBackground }),
                onTouchmove: _cache[4] || (_cache[4] = vue.withModifiers((...args) => $options.stop && $options.stop(...args), ["stop", "prevent"])),
                catchtouchmove: "stop"
              },
              [
                vue.createElementVNode(
                  "view",
                  {
                    class: "tui-btn-picker",
                    style: vue.normalizeStyle({ color: $props.cancelColor }),
                    "hover-class": "tui-opacity",
                    "hover-stay-time": 150,
                    onClick: _cache[2] || (_cache[2] = (...args) => $options.hide && $options.hide(...args))
                  },
                  "取消",
                  4
                  /* STYLE */
                ),
                vue.createElementVNode(
                  "view",
                  {
                    class: "tui-pickerdate__title",
                    style: vue.normalizeStyle({ fontSize: $props.titleSize + "rpx", color: $props.titleColor })
                  },
                  vue.toDisplayString($props.title),
                  5
                  /* TEXT, STYLE */
                ),
                vue.createElementVNode(
                  "view",
                  {
                    class: "tui-btn-picker",
                    style: vue.normalizeStyle({ color: $options.getColor }),
                    "hover-class": "tui-opacity",
                    "hover-stay-time": 150,
                    onClick: _cache[3] || (_cache[3] = (...args) => $options.btnFix && $options.btnFix(...args))
                  },
                  "确定",
                  4
                  /* STYLE */
                )
              ],
              38
              /* CLASS, STYLE, NEED_HYDRATION */
            ),
            $props.unitTop ? (vue.openBlock(), vue.createElementBlock(
              "view",
              {
                key: 0,
                class: "tui-date-header",
                style: vue.normalizeStyle({ backgroundColor: $props.unitBackground })
              },
              [
                $props.type < 4 || $props.type == 7 || $props.type == 8 ? (vue.openBlock(), vue.createElementBlock("view", {
                  key: 0,
                  class: "tui-date-unit"
                }, "年")) : vue.createCommentVNode("v-if", true),
                $props.type < 4 && $props.type > 0 || $props.type == 7 || $props.type == 8 ? (vue.openBlock(), vue.createElementBlock("view", {
                  key: 1,
                  class: "tui-date-unit"
                }, "月")) : vue.createCommentVNode("v-if", true),
                $props.type == 1 || $props.type == 2 || $props.type == 7 || $props.type == 8 ? (vue.openBlock(), vue.createElementBlock("view", {
                  key: 2,
                  class: "tui-date-unit"
                }, "日")) : vue.createCommentVNode("v-if", true),
                $props.type == 1 || $props.type == 4 || $props.type == 5 || $props.type == 7 || $props.type == 8 ? (vue.openBlock(), vue.createElementBlock("view", {
                  key: 3,
                  class: "tui-date-unit"
                }, "时")) : vue.createCommentVNode("v-if", true),
                ($props.type == 1 || $props.type > 3) && $props.type != 8 ? (vue.openBlock(), vue.createElementBlock("view", {
                  key: 4,
                  class: "tui-date-unit"
                }, "分")) : vue.createCommentVNode("v-if", true),
                $props.type > 4 && $props.type != 8 ? (vue.openBlock(), vue.createElementBlock("view", {
                  key: 5,
                  class: "tui-date-unit"
                }, "秒")) : vue.createCommentVNode("v-if", true)
              ],
              4
              /* STYLE */
            )) : vue.createCommentVNode("v-if", true),
            vue.createElementVNode(
              "view",
              {
                onTouchstart: _cache[6] || (_cache[6] = vue.withModifiers((...args) => $options.pickerstart && $options.pickerstart(...args), ["stop"])),
                class: "tui-date__picker-body",
                style: vue.normalizeStyle({ backgroundColor: $props.bodyBackground, height: $props.height + "rpx" })
              },
              [
                (vue.openBlock(), vue.createElementBlock("picker-view", {
                  key: $props.type,
                  "immediate-change": $data.immediate,
                  value: $data.value,
                  onChange: _cache[5] || (_cache[5] = (...args) => $options.change && $options.change(...args)),
                  class: "tui-datetime__picker-view"
                }, [
                  $props.type < 4 || $props.type == 7 || $props.type == 8 ? (vue.openBlock(), vue.createElementBlock("picker-view-column", { key: 0 }, [
                    (vue.openBlock(true), vue.createElementBlock(
                      vue.Fragment,
                      null,
                      vue.renderList($data.years, (item, index2) => {
                        return vue.openBlock(), vue.createElementBlock(
                          "view",
                          {
                            class: vue.normalizeClass(["tui-date__column-item", { "tui-font-size_32": !$props.unitTop && $props.type == 7 }]),
                            key: index2
                          },
                          [
                            vue.createTextVNode(
                              vue.toDisplayString(item) + " ",
                              1
                              /* TEXT */
                            ),
                            !$props.unitTop ? (vue.openBlock(), vue.createElementBlock("text", {
                              key: 0,
                              class: "tui-date__unit-text"
                            }, "年")) : vue.createCommentVNode("v-if", true)
                          ],
                          2
                          /* CLASS */
                        );
                      }),
                      128
                      /* KEYED_FRAGMENT */
                    ))
                  ])) : vue.createCommentVNode("v-if", true),
                  $props.type < 4 && $props.type > 0 || $props.type == 7 || $props.type == 8 ? (vue.openBlock(), vue.createElementBlock("picker-view-column", { key: 1 }, [
                    (vue.openBlock(true), vue.createElementBlock(
                      vue.Fragment,
                      null,
                      vue.renderList($data.months, (item, index2) => {
                        return vue.openBlock(), vue.createElementBlock(
                          "view",
                          {
                            class: vue.normalizeClass(["tui-date__column-item", { "tui-font-size_32": !$props.unitTop && $props.type == 7 }]),
                            key: index2
                          },
                          [
                            vue.createTextVNode(
                              vue.toDisplayString($options.formatNum(item)) + " ",
                              1
                              /* TEXT */
                            ),
                            !$props.unitTop ? (vue.openBlock(), vue.createElementBlock("text", {
                              key: 0,
                              class: "tui-date__unit-text"
                            }, "月")) : vue.createCommentVNode("v-if", true)
                          ],
                          2
                          /* CLASS */
                        );
                      }),
                      128
                      /* KEYED_FRAGMENT */
                    ))
                  ])) : vue.createCommentVNode("v-if", true),
                  $props.type == 1 || $props.type == 2 || $props.type == 7 || $props.type == 8 ? (vue.openBlock(), vue.createElementBlock("picker-view-column", { key: 2 }, [
                    (vue.openBlock(true), vue.createElementBlock(
                      vue.Fragment,
                      null,
                      vue.renderList($data.days, (item, index2) => {
                        return vue.openBlock(), vue.createElementBlock(
                          "view",
                          {
                            class: vue.normalizeClass(["tui-date__column-item", { "tui-font-size_32": !$props.unitTop && $props.type == 7 }]),
                            key: index2
                          },
                          [
                            vue.createTextVNode(
                              vue.toDisplayString($options.formatNum(item)) + " ",
                              1
                              /* TEXT */
                            ),
                            !$props.unitTop ? (vue.openBlock(), vue.createElementBlock("text", {
                              key: 0,
                              class: "tui-date__unit-text"
                            }, "日")) : vue.createCommentVNode("v-if", true)
                          ],
                          2
                          /* CLASS */
                        );
                      }),
                      128
                      /* KEYED_FRAGMENT */
                    ))
                  ])) : vue.createCommentVNode("v-if", true),
                  $props.type == 1 || $props.type == 4 || $props.type == 5 || $props.type == 7 || $props.type == 8 ? (vue.openBlock(), vue.createElementBlock("picker-view-column", { key: 3 }, [
                    (vue.openBlock(true), vue.createElementBlock(
                      vue.Fragment,
                      null,
                      vue.renderList($data.hours, (item, index2) => {
                        return vue.openBlock(), vue.createElementBlock(
                          "view",
                          {
                            class: vue.normalizeClass(["tui-date__column-item", { "tui-font-size_32": !$props.unitTop && $props.type == 7 }]),
                            key: index2
                          },
                          [
                            vue.createTextVNode(
                              vue.toDisplayString($options.formatNum(item)) + " ",
                              1
                              /* TEXT */
                            ),
                            !$props.unitTop ? (vue.openBlock(), vue.createElementBlock("text", {
                              key: 0,
                              class: "tui-date__unit-text"
                            }, "时")) : vue.createCommentVNode("v-if", true)
                          ],
                          2
                          /* CLASS */
                        );
                      }),
                      128
                      /* KEYED_FRAGMENT */
                    ))
                  ])) : vue.createCommentVNode("v-if", true),
                  ($props.type == 1 || $props.type > 3) && $props.type != 8 ? (vue.openBlock(), vue.createElementBlock("picker-view-column", { key: 4 }, [
                    (vue.openBlock(true), vue.createElementBlock(
                      vue.Fragment,
                      null,
                      vue.renderList($data.minutes, (item, index2) => {
                        return vue.openBlock(), vue.createElementBlock(
                          "view",
                          {
                            class: vue.normalizeClass(["tui-date__column-item", { "tui-font-size_32": !$props.unitTop && $props.type == 7 }]),
                            key: index2
                          },
                          [
                            vue.createTextVNode(
                              vue.toDisplayString($options.formatNum(item)) + " ",
                              1
                              /* TEXT */
                            ),
                            !$props.unitTop ? (vue.openBlock(), vue.createElementBlock("text", {
                              key: 0,
                              class: "tui-date__unit-text"
                            }, "分")) : vue.createCommentVNode("v-if", true)
                          ],
                          2
                          /* CLASS */
                        );
                      }),
                      128
                      /* KEYED_FRAGMENT */
                    ))
                  ])) : vue.createCommentVNode("v-if", true),
                  $props.type > 4 && $props.type != 8 ? (vue.openBlock(), vue.createElementBlock("picker-view-column", { key: 5 }, [
                    (vue.openBlock(true), vue.createElementBlock(
                      vue.Fragment,
                      null,
                      vue.renderList($data.seconds, (item, index2) => {
                        return vue.openBlock(), vue.createElementBlock(
                          "view",
                          {
                            class: vue.normalizeClass(["tui-date__column-item", { "tui-font-size_32": !$props.unitTop && $props.type == 7 }]),
                            key: index2
                          },
                          [
                            vue.createTextVNode(
                              vue.toDisplayString($options.formatNum(item)) + " ",
                              1
                              /* TEXT */
                            ),
                            !$props.unitTop ? (vue.openBlock(), vue.createElementBlock("text", {
                              key: 0,
                              class: "tui-date__unit-text"
                            }, "秒")) : vue.createCommentVNode("v-if", true)
                          ],
                          2
                          /* CLASS */
                        );
                      }),
                      128
                      /* KEYED_FRAGMENT */
                    ))
                  ])) : vue.createCommentVNode("v-if", true)
                ], 40, ["immediate-change", "value"]))
              ],
              36
              /* STYLE, NEED_HYDRATION */
            )
          ],
          6
          /* CLASS, STYLE */
        )
      ],
      4
      /* STYLE */
    );
  }
  const __easycom_5 = /* @__PURE__ */ _export_sfc(_sfc_main$a, [["render", _sfc_render$9], ["__scopeId", "data-v-bd7b091c"], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/components/thorui/tui-datetime/tui-datetime.vue"]]);
  const _sfc_main$9 = {
    name: "tui-input",
    emits: ["input", "update:modelValue", "focus", "blur", "confirm", "click", "keyboardheightchange"],
    //这里加group是为了避免在表单中使用时给组件加value属性
    props: {
      //是否为必填项
      required: {
        type: Boolean,
        default: false
      },
      requiredColor: {
        type: String,
        default: ""
      },
      //左侧标题
      label: {
        type: String,
        default: ""
      },
      //标题字体大小
      labelSize: {
        type: [Number, String],
        default: 0
      },
      labelColor: {
        type: String,
        default: ""
      },
      //label 最小宽度 rpx
      labelWidth: {
        type: Number,
        default: 140
      },
      clearable: {
        type: Boolean,
        default: false
      },
      //px
      clearSize: {
        type: Number,
        default: 15
      },
      clearColor: {
        type: String,
        default: "#bfbfbf"
      },
      //获取焦点
      focus: {
        type: Boolean,
        default: false
      },
      placeholder: {
        type: String,
        default: ""
      },
      placeholderStyle: {
        type: String,
        default: ""
      },
      //输入框名称
      name: {
        type: String,
        default: ""
      },
      //输入框值
      value: {
        type: [Number, String],
        default: ""
      },
      //输入框值
      modelValue: {
        type: [Number, String],
        default: ""
      },
      modelModifiers: {
        default: () => ({})
      },
      //与官方input type属性一致
      type: {
        type: String,
        default: "text"
      },
      password: {
        type: Boolean,
        default: false
      },
      disabled: {
        type: Boolean,
        default: false
      },
      maxlength: {
        type: [Number, String],
        default: 140
      },
      min: {
        type: [Number, String],
        default: "NaN"
      },
      max: {
        type: [Number, String],
        default: "NaN"
      },
      cursorSpacing: {
        type: Number,
        default: 0
      },
      confirmType: {
        type: String,
        default: "done"
      },
      confirmHold: {
        type: Boolean,
        default: false
      },
      cursor: {
        type: Number,
        default: -1
      },
      selectionStart: {
        type: Number,
        default: -1
      },
      selectionEnd: {
        type: Number,
        default: -1
      },
      adjustPosition: {
        type: Boolean,
        default: true
      },
      holdKeyboard: {
        type: Boolean,
        default: false
      },
      autoBlur: {
        type: Boolean,
        default: false
      },
      //输入框字体大小 rpx
      size: {
        type: [Number, String],
        default: 0
      },
      //输入框字体颜色
      color: {
        type: String,
        default: ""
      },
      // 是否显示 input 边框
      inputBorder: {
        type: Boolean,
        default: false
      },
      borderColor: {
        type: String,
        default: "rgba(0, 0, 0, 0.1)"
      },
      //input是否显示为圆角
      isFillet: {
        type: Boolean,
        default: false
      },
      // 是否显示上边框
      borderTop: {
        type: Boolean,
        default: false
      },
      // 是否显示下边框
      borderBottom: {
        type: Boolean,
        default: true
      },
      //下边框线条是否有左偏移距离
      lineLeft: {
        type: Boolean,
        default: true
      },
      // 是否自动去除两端的空格
      trim: {
        type: Boolean,
        default: true
      },
      textRight: {
        type: Boolean,
        default: false
      },
      //输入框padding值
      padding: {
        type: String,
        default: ""
      },
      //输入框背景颜色
      backgroundColor: {
        type: String,
        default: ""
      },
      radius: {
        type: [Number, String],
        default: -1
      },
      //输入框margin-top值 rpx
      marginTop: {
        type: [Number, String],
        default: 0
      }
    },
    computed: {
      getLabelSize() {
        return this.labelSize || uni && uni.$tui && uni.$tui.tuiInput.labelSize || 32;
      },
      getLabelColor() {
        return this.labelColor || uni && uni.$tui && uni.$tui.tuiInput.labelColor || "#333";
      },
      getSize() {
        return this.size || uni && uni.$tui && uni.$tui.tuiInput.size || 32;
      },
      getColor() {
        return this.color || uni && uni.$tui && uni.$tui.tuiInput.color || "#333";
      },
      getRadius() {
        let radius = this.radius;
        if (radius === -1 || radius === true) {
          radius = uni && uni.$tui && uni.$tui.tuiInput.radius;
        }
        return Number(radius || 0);
      },
      getStyles() {
        const padding = this.padding || uni && uni.$tui && uni.$tui.tuiInput.padding || "26rpx 30rpx";
        const bgColor = this.backgroundColor || uni && uni.$tui && uni.$tui.tuiInput.backgroundColor || "#FFFFFF";
        let radius = this.getRadius;
        let styles = `padding:${padding};background:${bgColor};margin-top:${this.marginTop}rpx;`;
        if (radius && radius !== true && radius !== -1) {
          styles += `border-radius:${radius}rpx;`;
        }
        if (this.borderTop || this.borderBottom || this.inputBorder) {
          styles += `border-color:${this.borderColor};`;
        }
        return styles;
      },
      getRequiredColor() {
        return this.requiredColor || uni && uni.$tui && uni.$tui.tuiInput.requiredColor || "#EB0909";
      }
    },
    data() {
      return {
        placeholderStyl: "",
        focused: false,
        inputVal: ""
      };
    },
    watch: {
      focus(val) {
        this.$nextTick(() => {
          setTimeout(() => {
            this.focused = val;
          }, 50);
        });
      },
      placeholderStyle() {
        this.fieldPlaceholderStyle();
      },
      modelValue(newVal) {
        this.inputVal = newVal;
      },
      value(newVal) {
        this.inputVal = newVal;
      }
    },
    created() {
      this.fieldPlaceholderStyle();
      setTimeout(() => {
        if (this.value && !this.modelValue) {
          this.inputVal = this.value;
        } else {
          this.inputVal = this.modelValue;
        }
      }, 50);
    },
    mounted() {
      this.$nextTick(() => {
        setTimeout(() => {
          this.focused = this.focus;
        }, 300);
      });
    },
    methods: {
      fieldPlaceholderStyle() {
        if (this.placeholderStyle) {
          this.placeholderStyl = this.placeholderStyle;
        } else {
          const size = uni.upx2px(this.size || uni && uni.$tui && uni.$tui.tuiInput.size || 32);
          this.placeholderStyl = `font-size:${size}px`;
        }
      },
      onInput(event) {
        let value = event.detail.value;
        if (this.trim)
          value = this.trimStr(value);
        this.inputVal = value;
        const cVal = Number(value);
        if ((this.modelModifiers.number || this.type === "digit" || this.type === "number") && !isNaN(cVal) && Number.isSafeInteger(cVal)) {
          let eVal = this.type === "digit" ? value : cVal;
          if (typeof cVal === "number") {
            const min = Number(this.min);
            const max = Number(this.max);
            if (typeof min === "number" && cVal < min) {
              eVal = min;
            } else if (typeof max === "number" && max < cVal) {
              eVal = max;
            }
          }
          value = isNaN(eVal) ? value : eVal;
        }
        this.$nextTick(() => {
          event.detail.value !== "" && (this.inputVal = value);
        });
        const inputValue = event.detail.value !== "" ? value : "";
        this.$emit("input", inputValue);
        this.$emit("update:modelValue", inputValue);
      },
      onFocus(event) {
        this.$emit("focus", event);
      },
      onBlur(event) {
        this.$emit("blur", event);
      },
      onConfirm(e) {
        this.$emit("confirm", e);
      },
      onClear(event) {
        if (this.disabled)
          return;
        uni.hideKeyboard();
        this.inputVal = "";
        this.$emit("input", "");
        this.$emit("update:modelValue", "");
      },
      fieldClick() {
        this.$emit("click", {
          name: this.name
        });
      },
      onKeyboardheightchange(e) {
        this.$emit("keyboardheightchange", e.detail);
      },
      trimStr(str) {
        return str.replace(/^\s+|\s+$/g, "");
      }
    }
  };
  function _sfc_render$8(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock(
      "view",
      {
        class: vue.normalizeClass(["tui-input__wrap", { "tui-border__top": $props.borderTop && !$props.inputBorder, "tui-border__bottom": $props.borderBottom && !$props.inputBorder, "tui-radius__fillet": $props.isFillet && !$options.getRadius, "tui-input__border-nvue": $props.inputBorder }]),
        style: vue.normalizeStyle($options.getStyles),
        onClick: _cache[6] || (_cache[6] = (...args) => $options.fieldClick && $options.fieldClick(...args))
      },
      [
        $props.borderTop && !$props.inputBorder ? (vue.openBlock(), vue.createElementBlock(
          "view",
          {
            key: 0,
            class: "tui-input__border-top",
            style: vue.normalizeStyle({ borderTopColor: $props.borderColor })
          },
          null,
          4
          /* STYLE */
        )) : vue.createCommentVNode("v-if", true),
        $props.borderBottom && !$props.inputBorder ? (vue.openBlock(), vue.createElementBlock(
          "view",
          {
            key: 1,
            class: vue.normalizeClass(["tui-input__border-bottom", { "tui-line__left": $props.lineLeft }]),
            style: vue.normalizeStyle({ borderBottomColor: $props.borderColor })
          },
          null,
          6
          /* CLASS, STYLE */
        )) : vue.createCommentVNode("v-if", true),
        $props.inputBorder ? (vue.openBlock(), vue.createElementBlock(
          "view",
          {
            key: 2,
            class: vue.normalizeClass(["tui-input__border", { "tui-radius__fillet": $props.isFillet && !$options.getRadius }]),
            style: vue.normalizeStyle({ borderColor: $props.borderColor, borderRadius: $options.getRadius * 2 + "rpx" })
          },
          null,
          6
          /* CLASS, STYLE */
        )) : vue.createCommentVNode("v-if", true),
        $props.required ? (vue.openBlock(), vue.createElementBlock(
          "view",
          {
            key: 3,
            class: "tui-input__required",
            style: vue.normalizeStyle({ color: $options.getRequiredColor })
          },
          "*",
          4
          /* STYLE */
        )) : vue.createCommentVNode("v-if", true),
        $props.label ? (vue.openBlock(), vue.createElementBlock(
          "view",
          {
            key: 4,
            class: "tui-input__label",
            style: vue.normalizeStyle({ fontSize: $options.getLabelSize + "rpx", color: $options.getLabelColor, minWidth: $props.labelWidth + "rpx" })
          },
          [
            vue.createElementVNode(
              "text",
              {
                style: vue.normalizeStyle({ fontSize: $options.getLabelSize + "rpx", color: $options.getLabelColor })
              },
              vue.toDisplayString($props.label),
              5
              /* TEXT, STYLE */
            )
          ],
          4
          /* STYLE */
        )) : vue.createCommentVNode("v-if", true),
        vue.renderSlot(_ctx.$slots, "left", {}, void 0, true),
        vue.createElementVNode("input", {
          class: vue.normalizeClass(["tui-input__self", { "tui-text__right": $props.textRight, "tui-input__disabled": $props.disabled }]),
          style: vue.normalizeStyle({ fontSize: $options.getSize + "rpx", color: $props.color }),
          "placeholder-class": "tui-input__placeholder",
          type: $props.type,
          name: $props.name,
          value: $data.inputVal,
          password: $props.password,
          placeholder: $data.inputVal ? "" : $props.placeholder,
          "placeholder-style": $data.placeholderStyl,
          disabled: $props.disabled,
          "cursor-spacing": $props.cursorSpacing,
          maxlength: $props.maxlength,
          focus: $data.focused,
          "confirm-type": $props.confirmType,
          "confirm-hold": $props.confirmHold,
          cursor: $props.cursor,
          "selection-start": $props.selectionStart,
          "selection-end": $props.selectionEnd,
          "adjust-position": $props.adjustPosition,
          "hold-keyboard": $props.holdKeyboard,
          "auto-blur": $props.autoBlur,
          onFocus: _cache[0] || (_cache[0] = (...args) => $options.onFocus && $options.onFocus(...args)),
          onBlur: _cache[1] || (_cache[1] = (...args) => $options.onBlur && $options.onBlur(...args)),
          onInput: _cache[2] || (_cache[2] = (...args) => $options.onInput && $options.onInput(...args)),
          onConfirm: _cache[3] || (_cache[3] = (...args) => $options.onConfirm && $options.onConfirm(...args)),
          onKeyboardheightchange: _cache[4] || (_cache[4] = (...args) => $options.onKeyboardheightchange && $options.onKeyboardheightchange(...args))
        }, null, 46, ["type", "name", "value", "password", "placeholder", "placeholder-style", "disabled", "cursor-spacing", "maxlength", "focus", "confirm-type", "confirm-hold", "cursor", "selection-start", "selection-end", "adjust-position", "hold-keyboard", "auto-blur"]),
        $props.clearable && $data.inputVal != "" ? (vue.openBlock(), vue.createElementBlock("icon", {
          key: 5,
          type: "clear",
          size: $props.clearSize,
          color: $props.clearColor,
          onClick: _cache[5] || (_cache[5] = vue.withModifiers((...args) => $options.onClear && $options.onClear(...args), ["stop"]))
        }, null, 8, ["size", "color"])) : vue.createCommentVNode("v-if", true),
        vue.renderSlot(_ctx.$slots, "right", {}, void 0, true)
      ],
      6
      /* CLASS, STYLE */
    );
  }
  const tuiInput = /* @__PURE__ */ _export_sfc(_sfc_main$9, [["render", _sfc_render$8], ["__scopeId", "data-v-0df87698"], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/components/thorui/tui-input/tui-input.vue"]]);
  const _sfc_main$8 = {
    name: "tuiButton",
    emits: ["click", "getuserinfo", "contact", "getphonenumber", "error", "chooseavatar", "launchapp"],
    props: {
      //样式类型 primary, white, danger, warning, green,blue, gray，black,brown,gray-primary,gray-danger,gray-warning,gray-green
      type: {
        type: String,
        default: "primary"
      },
      //是否加阴影 移除
      shadow: {
        type: Boolean,
        default: false
      },
      // 宽度 rpx或 %
      width: {
        type: String,
        default: "100%"
      },
      //高度 rpx
      height: {
        type: String,
        default: ""
      },
      //medium 184*40 / small 120 40/ mini 58*32
      btnSize: {
        type: String,
        default: ""
      },
      //字体大小 rpx
      size: {
        type: [Number, String],
        default: 0
      },
      bold: {
        type: Boolean,
        default: false
      },
      margin: {
        type: String,
        default: "0"
      },
      //形状 circle(圆角), square(默认方形)，rightAngle(平角)
      shape: {
        type: String,
        default: "square"
      },
      plain: {
        type: Boolean,
        default: false
      },
      //link样式，去掉边框，结合plain一起使用
      link: {
        type: Boolean,
        default: false
      },
      disabled: {
        type: Boolean,
        default: false
      },
      //禁用后背景是否为灰色 （非空心button生效）
      disabledGray: {
        type: Boolean,
        default: false
      },
      loading: {
        type: Boolean,
        default: false
      },
      formType: {
        type: String,
        default: ""
      },
      openType: {
        type: String,
        default: ""
      },
      appParameter: {
        type: String,
        default: ""
      },
      index: {
        type: [Number, String],
        default: 0
      },
      //是否需要阻止重复点击【默认200ms】
      preventClick: {
        type: Boolean,
        default: false
      }
    },
    computed: {
      getWidth() {
        let width = this.width;
        if (this.btnSize && this.btnSize !== true) {
          width = {
            "medium": "368rpx",
            "small": "240rpx",
            "mini": "116rpx"
          }[this.btnSize] || this.width;
        }
        return width;
      },
      getHeight() {
        let height = this.height || uni && uni.$tui && uni.$tui.tuiButton.height || "96rpx";
        if (this.btnSize && this.btnSize !== true) {
          height = {
            "medium": "80rpx",
            "small": "80rpx",
            "mini": "64rpx"
          }[this.btnSize] || "96rpx";
        }
        return height;
      },
      getSize() {
        return this.size || uni && uni.$tui && uni.$tui.tuiButton.size || 32;
      }
    },
    data() {
      return {
        time: 0
      };
    },
    methods: {
      hexToRGB(hex) {
        if (hex.length === 4) {
          let text = hex.substring(1, 4);
          hex = "#" + text + text;
        }
        let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
          r: parseInt(result[1], 16),
          g: parseInt(result[2], 16),
          b: parseInt(result[3], 16)
        } : {};
      },
      getColorByType(type, isText, plain) {
        const global2 = uni && uni.$tui && uni.$tui.color;
        let color2 = "";
        const colors = {
          "primary": global2 && global2.primary || "#5677fc",
          "white": "#fff",
          "danger": global2 && global2.danger || "#EB0909",
          "warning": global2 && global2.warning || "#ff7900",
          "green": global2 && global2.success || "#07c160",
          "blue": global2 && global2.blue || "#007aff",
          "gray": "#bfbfbf",
          "black": "#333333",
          "brown": "#ac9157",
          "gray-primary": "#f2f2f2",
          "gray-danger": "#f2f2f2",
          "gray-warning": "#f2f2f2",
          "gray-green": "#f2f2f2"
        };
        if (isText) {
          if (type && ~type.indexOf("gray-")) {
            const tp = type.replace("gray-", "");
            color2 = colors[tp];
          } else if (type === "white") {
            color2 = "#333";
          } else {
            if (plain) {
              color2 = colors[type];
            } else {
              color2 = "#fff";
            }
          }
        } else {
          color2 = colors[type] || colors.primary;
        }
        return color2;
      },
      getShadow(type, plain) {
        const color2 = this.getColorByType(type);
        if (plain || !color2)
          return "none";
        const rgb = this.hexToRGB(color2);
        return `0 10rpx 14rpx 0 rgba(${rgb.r}, ${rgb.g}, ${rgb.b}, 0.2)`;
      },
      getBgColor(type, plain) {
        return plain ? "transparent" : this.getColorByType(type);
      },
      getColor(type, plain) {
        return this.getColorByType(type, true, plain);
      },
      handleClick() {
        if (this.disabled)
          return;
        if (this.preventClick) {
          if ((/* @__PURE__ */ new Date()).getTime() - this.time <= 200)
            return;
          this.time = (/* @__PURE__ */ new Date()).getTime();
          setTimeout(() => {
            this.time = 0;
          }, 200);
        }
        this.$emit("click", {
          index: Number(this.index)
        });
      },
      bindgetuserinfo({
        detail = {}
      } = {}) {
        this.$emit("getuserinfo", detail);
      },
      bindcontact({
        detail = {}
      } = {}) {
        this.$emit("contact", detail);
      },
      bindgetphonenumber({
        detail = {}
      } = {}) {
        this.$emit("getphonenumber", detail);
      },
      binderror({
        detail = {}
      } = {}) {
        this.$emit("error", detail);
      },
      bindchooseavatar({
        detail = {}
      } = {}) {
        this.$emit("chooseavatar", detail);
      },
      bindlaunchapp({
        detail = {}
      } = {}) {
        this.$emit("launchapp", detail);
      },
      getDisabledClass: function(disabled, type, plain) {
        let className = "";
        if (disabled && type != "white" && type.indexOf("-") == -1) {
          let classVal = this.disabledGray ? "tui-gray-disabled" : "tui-dark-disabled";
          className = plain ? "tui-dark-disabled-outline" : classVal;
        }
        return className;
      },
      getShapeClass: function(shape, plain) {
        let className = "";
        if (shape == "circle") {
          className = plain ? "tui-outline-fillet" : "tui-fillet";
        } else if (shape == "rightAngle") {
          className = plain ? "tui-outline-rightAngle" : "tui-rightAngle";
        }
        return className;
      },
      getHoverClass: function(disabled, type, plain) {
        let className = "";
        if (!disabled) {
          className = plain ? "tui-outline-hover" : "tui-" + (type || "primary") + "-hover";
        }
        return className;
      }
    }
  };
  function _sfc_render$7(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock(
      "view",
      {
        class: vue.normalizeClass(["tui-button__wrap", [($props.width === "100%" || !$props.width || $props.width === true) && (!$props.btnSize || $props.btnSize === true) ? "tui-btn__flex-1" : "", $options.getShapeClass($props.shape, $props.plain), !$props.disabled ? "tui-button__hover" : ""]]),
        style: vue.normalizeStyle({ width: $options.getWidth, height: $options.getHeight, margin: $props.margin })
      },
      [
        vue.createElementVNode("button", {
          class: vue.normalizeClass(["tui-btn", [
            $props.plain ? "tui-" + $props.type + "-outline" : "tui-btn-" + ($props.type || "primary"),
            $options.getDisabledClass($props.disabled, $props.type, $props.plain),
            $options.getShapeClass($props.shape, $props.plain),
            $props.bold ? "tui-text-bold" : "",
            $props.link ? "tui-btn__link" : ""
          ]]),
          style: vue.normalizeStyle({ width: $options.getWidth, height: $options.getHeight, lineHeight: $options.getHeight, fontSize: $options.getSize + "rpx", background: $options.getBgColor($props.type, $props.plain), color: $options.getColor($props.type, $props.plain), boxShadow: $props.shadow ? $options.getShadow($props.type, $props.plain) : "none" }),
          loading: $props.loading,
          "form-type": $props.formType,
          "open-type": $props.openType,
          "app-parameter": $props.appParameter,
          onGetuserinfo: _cache[0] || (_cache[0] = (...args) => $options.bindgetuserinfo && $options.bindgetuserinfo(...args)),
          onGetphonenumber: _cache[1] || (_cache[1] = (...args) => $options.bindgetphonenumber && $options.bindgetphonenumber(...args)),
          onContact: _cache[2] || (_cache[2] = (...args) => $options.bindcontact && $options.bindcontact(...args)),
          onError: _cache[3] || (_cache[3] = (...args) => $options.binderror && $options.binderror(...args)),
          onChooseavatar: _cache[4] || (_cache[4] = (...args) => $options.bindchooseavatar && $options.bindchooseavatar(...args)),
          onLaunchapp: _cache[5] || (_cache[5] = (...args) => $options.bindlaunchapp && $options.bindlaunchapp(...args)),
          disabled: $props.disabled,
          onClick: _cache[6] || (_cache[6] = (...args) => $options.handleClick && $options.handleClick(...args))
        }, [
          vue.renderSlot(_ctx.$slots, "default", {}, void 0, true)
        ], 46, ["loading", "form-type", "open-type", "app-parameter", "disabled"]),
        !$props.link && $props.plain ? (vue.openBlock(), vue.createElementBlock(
          "view",
          {
            key: 0,
            class: vue.normalizeClass(["tui-button__border", [$options.getShapeClass($props.shape, $props.plain), $options.getDisabledClass($props.disabled, $props.type, $props.plain)]]),
            style: vue.normalizeStyle({ borderColor: $options.getBgColor($props.type) })
          },
          null,
          6
          /* CLASS, STYLE */
        )) : vue.createCommentVNode("v-if", true)
      ],
      6
      /* CLASS, STYLE */
    );
  }
  const tuiButton = /* @__PURE__ */ _export_sfc(_sfc_main$8, [["render", _sfc_render$7], ["__scopeId", "data-v-2af5f154"], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/components/thorui/tui-button/tui-button.vue"]]);
  const form = {
    //非必填情况下,如果值为空,则不进行校验
    //当出现错误时返回错误消息，否则返回空即为验证通过
    /*
     formData:Object 表单对象。{key:value,key:value},key==rules.name
     rules: Array [{name:name,rule:[],msg:[],validator:[],{name:name,rule:[],msg:[],validator:[]}]
    		name:name 属性=> 元素的名称
    		rule:字符串数组 ["required","isMobile","isEmail","isCarNo","isIdCard","isAmount","isNum","isChinese","isNotChinese","isEnglish",isEnAndNo","isSpecial","isEmoji",""isDate","isUrl","isSame:key","range:[1,9]","minLength:9","maxLength:Number","isKeyword:key1,key2,key3..."]
    		msg:数组 []。 与数组 rule 长度相同,对应的错误提示信息
    		validator:[{msg:'错误消息',method:Function}]，自定义验证方法组，函数约定：(value)=>{ return true or false}
    */
    validation: function(formData, rules2) {
      for (let item of rules2) {
        let key = item.name;
        let rule = item.rule || [];
        let validator = item.validator || [];
        let msgArr = item.msg;
        const ruleLen = rule.length;
        const validatorLen = validator.length;
        if (!key || ruleLen === 0 && validatorLen === 0 || !~rule.indexOf("required") && formData[key].toString().length === 0) {
          continue;
        }
        for (let i = 0, length = rule.length; i < length; i++) {
          let ruleItem = rule[i];
          let msg = msgArr[i];
          if (!msg || !ruleItem)
            continue;
          let value = null;
          if (~ruleItem.indexOf(":")) {
            let temp = ruleItem.split(":");
            ruleItem = temp[0];
            value = temp[1];
          }
          let isError = false;
          switch (ruleItem) {
            case "required":
              isError = form._isNullOrEmpty(formData[key]);
              break;
            case "isMobile":
              isError = !form._isMobile(formData[key]);
              break;
            case "isEmail":
              isError = !form._isEmail(formData[key]);
              break;
            case "isCarNo":
              isError = !form._isCarNo(formData[key]);
              break;
            case "isIdCard":
              isError = !form._isIdCard(formData[key]);
              break;
            case "isAmount":
              isError = !form._isAmount(formData[key]);
              break;
            case "isNum":
              isError = !form._isNum(formData[key]);
              break;
            case "isChinese":
              isError = !form._isChinese(formData[key]);
              break;
            case "isNotChinese":
              isError = !form._isNotChinese(formData[key]);
              break;
            case "isEnglish":
              isError = !form._isEnglish(formData[key]);
              break;
            case "isEnAndNo":
              isError = !form._isEnAndNo(formData[key]);
              break;
            case "isEnOrNo":
              isError = !form._isEnOrNo(formData[key]);
              break;
            case "isSpecial":
              isError = form._isSpecial(formData[key]);
              break;
            case "isEmoji":
              isError = form._isEmoji(formData[key]);
              break;
            case "isDate":
              isError = !form._isDate(formData[key]);
              break;
            case "isUrl":
              isError = !form._isUrl(formData[key]);
              break;
            case "isSame":
              isError = !form._isSame(formData[key], formData[value]);
              break;
            case "range":
              let range = null;
              try {
                range = JSON.parse(value);
                if (range.length <= 1) {
                  throw new Error("range值传入有误！");
                }
              } catch (e) {
                return "range值传入有误！";
              }
              isError = !form._isRange(formData[key], range[0], range[1]);
              break;
            case "minLength":
              isError = !form._minLength(formData[key], value);
              break;
            case "maxLength":
              isError = !form._maxLength(formData[key], value);
              break;
            case "isKeyword":
              isError = !form._isKeyword(formData[key], value);
              break;
          }
          if (isError) {
            return msg;
          }
        }
        if (validator && validator.length > 0) {
          for (let model of validator) {
            let func = model.method;
            if (func && !func(formData[key])) {
              return model.msg;
            }
          }
        }
      }
      return "";
    },
    //允许填写字符串null或者undefined
    _isNullOrEmpty: function(value) {
      return value === null || value === "" || value === void 0 ? true : false;
    },
    _isMobile: function(value) {
      return /^(?:13\d|14\d|15\d|16\d|17\d|18\d|19\d)\d{5}(\d{3}|\*{3})$/.test(value);
    },
    _isEmail: function(value) {
      return /^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$/.test(value);
    },
    _isCarNo: function(value) {
      const xreg = /^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]{1}[A-Z]{1}(([0-9]{5}[DF]$)|([DF][A-HJ-NP-Z0-9][0-9]{4}$))/;
      const creg = /^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]{1}[A-Z]{1}[A-HJ-NP-Z0-9]{4}[A-HJ-NP-Z0-9挂学警港澳]{1}$/;
      if (value.length === 7) {
        return creg.test(value);
      } else if (value.length === 8) {
        return xreg.test(value);
      } else {
        return false;
      }
    },
    _isIdCard: function(value) {
      let idCard = value;
      if (idCard.length == 15) {
        return this.__isValidityBrithBy15IdCard;
      } else if (idCard.length == 18) {
        let arrIdCard = idCard.split("");
        if (this.__isValidityBrithBy18IdCard(idCard) && this.__isTrueValidateCodeBy18IdCard(arrIdCard)) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    },
    __isTrueValidateCodeBy18IdCard: function(arrIdCard) {
      let sum = 0;
      let Wi = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2, 1];
      let ValideCode = [1, 0, 10, 9, 8, 7, 6, 5, 4, 3, 2];
      if (arrIdCard[17].toLowerCase() == "x") {
        arrIdCard[17] = 10;
      }
      for (let i = 0; i < 17; i++) {
        sum += Wi[i] * arrIdCard[i];
      }
      let valCodePosition = sum % 11;
      if (arrIdCard[17] == ValideCode[valCodePosition]) {
        return true;
      } else {
        return false;
      }
    },
    __isValidityBrithBy18IdCard: function(idCard18) {
      let year = idCard18.substring(6, 10);
      let month = idCard18.substring(10, 12);
      let day = idCard18.substring(12, 14);
      let temp_date = new Date(year, parseFloat(month) - 1, parseFloat(day));
      if (temp_date.getFullYear() != parseFloat(year) || temp_date.getMonth() != parseFloat(month) - 1 || temp_date.getDate() != parseFloat(day)) {
        return false;
      } else {
        return true;
      }
    },
    __isValidityBrithBy15IdCard: function(idCard15) {
      let year = idCard15.substring(6, 8);
      let month = idCard15.substring(8, 10);
      let day = idCard15.substring(10, 12);
      let temp_date = new Date(year, parseFloat(month) - 1, parseFloat(day));
      if (temp_date.getYear() != parseFloat(year) || temp_date.getMonth() != parseFloat(month) - 1 || temp_date.getDate() != parseFloat(day)) {
        return false;
      } else {
        return true;
      }
    },
    _isAmount: function(value) {
      return /^([0-9]*[.]?[0-9])[0-9]{0,1}$/.test(value);
    },
    _isNum: function(value) {
      return /^[0-9]+$/.test(value);
    },
    //是否全部为中文
    _isChinese: function(value) {
      let reg = /^[\u4e00-\u9fa5]+$/;
      return value !== "" && reg.test(value) && !form._isSpecial(value) && !form._isEmoji(value);
    },
    //是否不包含中文，可以有特殊字符
    _isNotChinese: function(value) {
      let reg = /.*[\u4e00-\u9fa5]+.*$/;
      let result = true;
      if (reg.test(value)) {
        result = false;
      }
      return result;
    },
    _isEnglish: function(value) {
      return /^[a-zA-Z]*$/.test(value);
    },
    _isEnAndNo: function(value) {
      return /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,20}$/.test(value);
    },
    _isEnOrNo: function(value) {
      let reg = /.*[\u4e00-\u9fa5]+.*$/;
      let result = true;
      if (reg.test(value) || form._isSpecial(value) || form._isEmoji(value)) {
        result = false;
      }
      return result;
    },
    _isSpecial: function(value) {
      let regEn = /[`~!@#$%^&*()_+<>?:"{},.\/;'[\]]/im, regCn = /[·！#￥（——）：；“”‘、，|《。》？、【】[\]]/im;
      if (regEn.test(value) || regCn.test(value)) {
        return true;
      }
      return false;
    },
    _isEmoji: function(value) {
      return /\uD83C[\uDF00-\uDFFF]|\uD83D[\uDC00-\uDE4F]/g.test(value);
    },
    _isDate: function(value) {
      const reg = /^(?:(?!0000)[0-9]{4}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-8])|(?:0[13-9]|1[0-2])-(?:29|30)|(?:0[13578]|1[02])-31)|(?:[0-9]{2}(?:0[48]|[2468][048]|[13579][26])|(?:0[48]|[2468][048]|[13579][26])00)-02-29)$/;
      return reg.test(value);
    },
    _isUrl: function(value) {
      return /^((https?|ftp|file):\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})(:[0-9]{1,5})?((\/?)|(\/[\\\w_!~*\\'()\\\.;?:@&=+$,%#-]+)+\/?)$/.test(value);
    },
    _isSame: function(value1, value2) {
      return value1 === value2;
    },
    _isRange: function(value, range1, range2) {
      if (!range1 && range1 != 0 && (!range2 && range2 != 0)) {
        return true;
      } else if (!range1 && range1 != 0) {
        return value <= range2;
      } else if (!range2 && range2 != 0) {
        return value >= range1;
      } else {
        return value >= range1 && value <= range2;
      }
    },
    _minLength: function(value, min) {
      return value.length >= Number(min);
    },
    _maxLength: function(value, max) {
      return value.length <= Number(max);
    },
    _isKeyword: function(value, keywords) {
      let result = true;
      if (!keywords)
        return result;
      let key = keywords.split(",");
      for (let i = 0, len = key.length; i < len; i++) {
        if (~value.indexOf(key[i])) {
          result = false;
          break;
        }
      }
      return result;
    }
  };
  const form$1 = {
    validation: form.validation
  };
  const _sfc_main$7 = {
    components: {
      tuiRadio: __easycom_1$1,
      tuiRadioGroup: __easycom_3,
      tuiNumberbox: __easycom_4,
      tuiInput,
      tuiDatetime: __easycom_5,
      tuiButton
    },
    data() {
      return {
        pwdArr: ["", "", "", "", "", ""],
        pricevalue: 18.5,
        weightvalue: 269.7,
        totalpricevalue: 4989.45,
        buycostvalue: 450,
        initweightvalue: 45.9,
        type: 2,
        startYear: 1980,
        endYear: 2030,
        cancelColor: "#888",
        color: "#5677fc",
        setDateTime: "",
        result: "",
        unitTop: false,
        radius: false,
        sexItems: [
          {
            name: "公",
            value: "1",
            checked: true
          },
          {
            name: "母",
            value: "2",
            checked: false
          },
          {
            name: "无",
            value: "3",
            checked: false
          }
        ],
        statusItems: [
          {
            name: "养殖中",
            value: "1",
            checked: true
          },
          {
            name: "待出栏",
            value: "2",
            checked: false
          },
          {
            name: "已销售",
            value: "3",
            checked: false
          },
          {
            name: "已处理",
            value: "4",
            checked: false
          }
        ]
      };
    },
    methods: {
      formSubmit: function(e) {
        let formData = e.detail.value;
        let checkRes = form$1.validation(formData, rules);
        if (!checkRes) {
          uni.showToast({
            title: "验证通过!",
            icon: "none"
          });
        } else {
          uni.showToast({
            title: checkRes,
            icon: "none"
          });
        }
      },
      formReset: function(e) {
        formatAppLog("log", "at pages/ann/dangan/daadd.vue:200", "清空数据");
      },
      change: function(e) {
        this.value = e.value;
      },
      //调用此方法显示选择器，需等组件初始化完成后调用，避免在onLoad中调用
      show: function(e) {
        this.$refs.dateTime && this.$refs.dateTime.show();
      },
      changedatatime: function(e) {
        this.result = e.result;
      }
    }
  };
  let rules = [{
    name: "mobile",
    rule: ["required"],
    msg: ["请输入编号"]
  }];
  function _sfc_render$6(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_tui_list_cell = resolveEasycom(vue.resolveDynamicComponent("tui-list-cell"), __easycom_0$3);
    const _component_tui_radio = resolveEasycom(vue.resolveDynamicComponent("tui-radio"), __easycom_1$1);
    const _component_tui_label = resolveEasycom(vue.resolveDynamicComponent("tui-label"), __easycom_2);
    const _component_tui_radio_group = resolveEasycom(vue.resolveDynamicComponent("tui-radio-group"), __easycom_3);
    const _component_tui_numberbox = resolveEasycom(vue.resolveDynamicComponent("tui-numberbox"), __easycom_4);
    const _component_tui_datetime = resolveEasycom(vue.resolveDynamicComponent("tui-datetime"), __easycom_5);
    return vue.openBlock(), vue.createElementBlock("view", { class: "container" }, [
      vue.createElementVNode(
        "form",
        {
          onSubmit: _cache[1] || (_cache[1] = (...args) => $options.formSubmit && $options.formSubmit(...args)),
          onReset: _cache[2] || (_cache[2] = (...args) => $options.formReset && $options.formReset(...args))
        },
        [
          vue.createVNode(_component_tui_list_cell, { hover: false }, {
            default: vue.withCtx(() => [
              vue.createElementVNode("view", { class: "tui-line-cell" }, [
                vue.createElementVNode("view", { class: "tui-title" }, "编号"),
                vue.createElementVNode("input", {
                  "placeholder-class": "tui-phcolor",
                  class: "tui-input",
                  name: "mobile",
                  placeholder: "请输入编号",
                  maxlength: "50",
                  type: "text"
                })
              ])
            ]),
            _: 1
            /* STABLE */
          }),
          vue.createVNode(_component_tui_list_cell, { hover: false }, {
            default: vue.withCtx(() => [
              vue.createElementVNode("view", { class: "tui-line-cell" }, [
                vue.createElementVNode("view", { class: "tui-title" }, "性别"),
                vue.createVNode(_component_tui_radio_group, { class: "tui-radio-group" }, {
                  default: vue.withCtx(() => [
                    (vue.openBlock(true), vue.createElementBlock(
                      vue.Fragment,
                      null,
                      vue.renderList($data.sexItems, (item, index2) => {
                        return vue.openBlock(), vue.createBlock(
                          _component_tui_label,
                          { key: index2 },
                          {
                            default: vue.withCtx(() => [
                              vue.createVNode(
                                _component_tui_list_cell,
                                null,
                                {
                                  default: vue.withCtx(() => [
                                    vue.createVNode(_component_tui_radio, {
                                      checked: item.checked,
                                      value: item.value,
                                      color: "#07c160",
                                      borderColor: "#999"
                                    }, null, 8, ["checked", "value"]),
                                    vue.createElementVNode(
                                      "text",
                                      { class: "tui-text" },
                                      vue.toDisplayString(item.name),
                                      1
                                      /* TEXT */
                                    )
                                  ]),
                                  _: 2
                                  /* DYNAMIC */
                                },
                                1024
                                /* DYNAMIC_SLOTS */
                              )
                            ]),
                            _: 2
                            /* DYNAMIC */
                          },
                          1024
                          /* DYNAMIC_SLOTS */
                        );
                      }),
                      128
                      /* KEYED_FRAGMENT */
                    ))
                  ]),
                  _: 1
                  /* STABLE */
                })
              ])
            ]),
            _: 1
            /* STABLE */
          }),
          vue.createVNode(_component_tui_list_cell, {
            hover: false,
            unlined: ""
          }, {
            default: vue.withCtx(() => [
              vue.createElementVNode("view", { class: "tui-line-cell" }, [
                vue.createElementVNode("view", { class: "tui-title" }, "状态"),
                vue.createVNode(_component_tui_radio_group, { class: "tui-radio-group-status" }, {
                  default: vue.withCtx(() => [
                    (vue.openBlock(true), vue.createElementBlock(
                      vue.Fragment,
                      null,
                      vue.renderList($data.statusItems, (item, index2) => {
                        return vue.openBlock(), vue.createBlock(
                          _component_tui_label,
                          { key: index2 },
                          {
                            default: vue.withCtx(() => [
                              vue.createVNode(
                                _component_tui_list_cell,
                                null,
                                {
                                  default: vue.withCtx(() => [
                                    vue.createVNode(_component_tui_radio, {
                                      checked: item.checked,
                                      value: item.value,
                                      color: "#07c160",
                                      borderColor: "#999"
                                    }, null, 8, ["checked", "value"]),
                                    vue.createElementVNode(
                                      "text",
                                      { class: "tui-text" },
                                      vue.toDisplayString(item.name),
                                      1
                                      /* TEXT */
                                    )
                                  ]),
                                  _: 2
                                  /* DYNAMIC */
                                },
                                1024
                                /* DYNAMIC_SLOTS */
                              )
                            ]),
                            _: 2
                            /* DYNAMIC */
                          },
                          1024
                          /* DYNAMIC_SLOTS */
                        );
                      }),
                      128
                      /* KEYED_FRAGMENT */
                    ))
                  ]),
                  _: 1
                  /* STABLE */
                })
              ])
            ]),
            _: 1
            /* STABLE */
          }),
          vue.createVNode(_component_tui_list_cell, { hover: false }, {
            default: vue.withCtx(() => [
              vue.createElementVNode("view", { class: "tui-line-cell" }, [
                vue.createElementVNode("view", { class: "tui-title" }, "单价"),
                vue.createVNode(_component_tui_numberbox, {
                  name: "price",
                  step: 0.5,
                  value: $data.pricevalue,
                  min: 0.01,
                  max: 5e3,
                  width: 400,
                  height: 60,
                  "background-color": "rgb(248,248,248)",
                  hange: "change"
                }, null, 8, ["value"])
              ])
            ]),
            _: 1
            /* STABLE */
          }),
          vue.createVNode(_component_tui_list_cell, { hover: false }, {
            default: vue.withCtx(() => [
              vue.createElementVNode("view", { class: "tui-line-cell" }, [
                vue.createElementVNode("view", { class: "tui-title" }, "体重"),
                vue.createVNode(_component_tui_numberbox, {
                  name: "weight",
                  step: 0.5,
                  value: $data.weightvalue,
                  min: 0.01,
                  max: 5e3,
                  width: 400,
                  height: 60,
                  "background-color": "rgb(248,248,248)",
                  hange: "change"
                }, null, 8, ["value"])
              ])
            ]),
            _: 1
            /* STABLE */
          }),
          vue.createVNode(_component_tui_list_cell, { hover: false }, {
            default: vue.withCtx(() => [
              vue.createElementVNode("view", { class: "tui-line-cell" }, [
                vue.createElementVNode("view", { class: "tui-title" }, "总价"),
                vue.createVNode(_component_tui_numberbox, {
                  name: "totalprice",
                  step: 0.5,
                  value: $data.totalpricevalue,
                  min: 0.01,
                  max: 5e3,
                  width: 400,
                  height: 60,
                  "background-color": "rgb(248,248,248)",
                  hange: "change"
                }, null, 8, ["value"])
              ])
            ]),
            _: 1
            /* STABLE */
          }),
          vue.createVNode(_component_tui_list_cell, { hover: false }, {
            default: vue.withCtx(() => [
              vue.createElementVNode("view", { class: "tui-line-cell" }, [
                vue.createElementVNode("view", { class: "tui-title" }, "买入成本"),
                vue.createVNode(_component_tui_numberbox, {
                  name: "buycost",
                  step: 0.5,
                  value: $data.buycostvalue,
                  min: 0.01,
                  max: 5e3,
                  width: 400,
                  height: 60,
                  "background-color": "rgb(248,248,248)",
                  hange: "change"
                }, null, 8, ["value"])
              ])
            ]),
            _: 1
            /* STABLE */
          }),
          vue.createVNode(_component_tui_list_cell, { hover: false }, {
            default: vue.withCtx(() => [
              vue.createElementVNode("view", { class: "tui-line-cell" }, [
                vue.createElementVNode("view", { class: "tui-title" }, "买入日期"),
                vue.createElementVNode(
                  "view",
                  { style: { "display": "flex", "align-items": "center", "justify-content": "center", "text-align": "center", "width": "inherit" } },
                  vue.toDisplayString($data.result),
                  1
                  /* TEXT */
                ),
                vue.createElementVNode("button", {
                  type: "primary",
                  onClick: _cache[0] || (_cache[0] = (...args) => $options.show && $options.show(...args)),
                  class: "btn-time"
                }, "选择"),
                vue.createVNode(_component_tui_datetime, {
                  type: 2,
                  ref: "dateTime",
                  onConfirm: $options.changedatatime
                }, null, 8, ["onConfirm"])
              ])
            ]),
            _: 1
            /* STABLE */
          }),
          vue.createVNode(_component_tui_list_cell, { hover: false }, {
            default: vue.withCtx(() => [
              vue.createElementVNode("view", { class: "tui-line-cell" }, [
                vue.createElementVNode("view", { class: "tui-title" }, "初始体重"),
                vue.createVNode(_component_tui_numberbox, {
                  name: "weight",
                  step: 0.5,
                  value: $data.initweightvalue,
                  min: 0.01,
                  max: 5e3,
                  width: 400,
                  height: 60,
                  "background-color": "rgb(248,248,248)",
                  hange: "change"
                }, null, 8, ["value"])
              ])
            ]),
            _: 1
            /* STABLE */
          }),
          vue.createVNode(_component_tui_list_cell, { hover: false }, {
            default: vue.withCtx(() => [
              vue.createElementVNode("view", { class: "tui-line-cell" }, [
                vue.createElementVNode("view", { class: "tui-title" }, "预计出栏日期"),
                vue.createVNode(_component_tui_numberbox, {
                  name: "weight",
                  step: 0.5,
                  value: $data.initweightvalue,
                  min: 0.01,
                  max: 5e3,
                  width: 400,
                  height: 60,
                  "background-color": "rgb(248,248,248)",
                  hange: "change"
                }, null, 8, ["value"])
              ])
            ]),
            _: 1
            /* STABLE */
          }),
          vue.createElementVNode("view", { class: "tui-btn-box" }, [
            vue.createElementVNode("button", {
              class: "tui-button-primary",
              "hover-class": "tui-button-hover",
              formType: "submit",
              type: "primary",
              style: { "background-color": "rgb(23,193,99)" }
            }, "登记"),
            vue.createElementVNode("button", {
              class: "tui-button-primary tui-button-gray",
              "hover-class": "tui-button-gray_hover",
              formType: "reset"
            }, "取消登记")
          ])
        ],
        32
        /* NEED_HYDRATION */
      )
    ]);
  }
  const PagesAnnDanganDaadd = /* @__PURE__ */ _export_sfc(_sfc_main$7, [["render", _sfc_render$6], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/pages/ann/dangan/daadd.vue"]]);
  const _sfc_main$6 = {
    name: "tuiSticky",
    emits: ["sticky", "change"],
    props: {
      scrollTop: {
        type: Number
      },
      //吸顶时与顶部的距离，单位px
      stickyTop: {
        type: [Number, String],
        default: 0
      },
      //是否指定容器，即内容放置插槽content内
      container: {
        type: Boolean,
        default: false
      },
      //是否是原生自带header
      isNativeHeader: {
        type: Boolean,
        default: true
      },
      //吸顶容器 高度 rpx
      stickyHeight: {
        type: String,
        default: "auto"
      },
      //占位容器背景颜色
      backgroundColor: {
        type: String,
        default: "transparent"
      },
      //是否重新计算[有异步加载时使用,设置大于0的数]
      recalc: {
        type: Number,
        default: 0
      },
      //列表中的索引值
      index: {
        type: [Number, String],
        default: 0
      }
    },
    watch: {
      scrollTop(newValue, oldValue) {
        if (this.initialize != 0) {
          this.updateScrollChange(() => {
            this.updateStickyChange();
            this.initialize = 0;
          });
        } else {
          this.updateStickyChange();
        }
      },
      recalc(newValue, oldValue) {
        this.updateScrollChange(() => {
          this.updateStickyChange();
          this.initialize = 0;
        });
      }
    },
    created() {
      this.initialize = this.recalc;
    },
    mounted() {
      this.$nextTick(() => {
        setTimeout(() => {
          this.updateScrollChange();
        }, 50);
      });
    },
    data() {
      const Id = `tui_${Math.ceil(Math.random() * 1e6).toString(36)}`;
      return {
        timer: null,
        top: 0,
        height: 0,
        isFixed: false,
        initialize: 0,
        //重新初始化
        tui_Id: Id
      };
    },
    methods: {
      updateStickyChange() {
        const top = this.top;
        const height = this.height;
        const scrollTop = this.scrollTop;
        let stickyTop = this.stickyTop;
        if (this.container) {
          this.isFixed = scrollTop + stickyTop >= top && scrollTop + stickyTop < top + height ? true : false;
        } else {
          this.isFixed = scrollTop + stickyTop >= top ? true : false;
        }
        this.$emit("sticky", {
          isFixed: this.isFixed,
          index: this.index
        });
      },
      updateScrollChange(callback) {
        if (this.timer) {
          clearTimeout(this.timer);
          this.timer = null;
        }
        this.timer = setTimeout(() => {
          const selectId = `#${this.tui_Id}`;
          uni.createSelectorQuery().in(this).select(selectId).fields({
            size: true,
            rect: true
          }, (res) => {
            if (res) {
              this.top = res.top + (this.scrollTop || 0);
              this.height = res.height;
              callback && callback();
              this.$emit("change", {
                index: Number(this.index),
                top: this.top
              });
            }
          }).exec();
        }, 0);
      }
    }
  };
  function _sfc_render$5(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock("view", {
      class: "tui-sticky-class",
      id: $data.tui_Id
    }, [
      vue.createCommentVNode("sticky 容器"),
      $data.isFixed ? (vue.openBlock(), vue.createElementBlock(
        "view",
        {
          key: 0,
          style: vue.normalizeStyle({ height: $props.stickyHeight, background: $props.backgroundColor })
        },
        null,
        4
        /* STYLE */
      )) : vue.createCommentVNode("v-if", true),
      vue.createElementVNode(
        "view",
        {
          class: vue.normalizeClass({ "tui-sticky-fixed": $data.isFixed }),
          style: vue.normalizeStyle({ top: $data.isFixed ? $props.stickyTop + "px" : "auto" })
        },
        [
          vue.renderSlot(_ctx.$slots, "header", {}, void 0, true)
        ],
        6
        /* CLASS, STYLE */
      ),
      vue.createCommentVNode("sticky 容器"),
      vue.createCommentVNode("内容"),
      vue.renderSlot(_ctx.$slots, "content", {}, void 0, true)
    ], 8, ["id"]);
  }
  const __easycom_0 = /* @__PURE__ */ _export_sfc(_sfc_main$6, [["render", _sfc_render$5], ["__scopeId", "data-v-2e485390"], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/components/thorui/tui-sticky/tui-sticky.vue"]]);
  const _sfc_main$5 = {
    methods: {
      navigateTo() {
        uni.navigateTo({
          url: "/pages/ann/dangan/daadd"
        });
      }
    }
  };
  function _sfc_render$4(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_tui_sticky = resolveEasycom(vue.resolveDynamicComponent("tui-sticky"), __easycom_0);
    return vue.openBlock(), vue.createBlock(_component_tui_sticky, {
      scrollTop: _ctx.scrollTop,
      stickyTop: _ctx.stickyTop,
      stickyHeight: "80rpx",
      onClick: _cache[0] || (_cache[0] = ($event) => $options.navigateTo())
    }, {
      header: vue.withCtx(() => [
        vue.createElementVNode("view", { class: "tui-center" }, [
          vue.createElementVNode("view", { class: "tui-tag tui-green" }, "牲畜档案列表")
        ])
      ]),
      _: 1
      /* STABLE */
    }, 8, ["scrollTop", "stickyTop"]);
  }
  const PagesAnnDanganDlist = /* @__PURE__ */ _export_sfc(_sfc_main$5, [["render", _sfc_render$4], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/pages/ann/dangan/dlist.vue"]]);
  const _sfc_main$4 = {};
  function _sfc_render$3(_ctx, _cache) {
    return " 发布 ";
  }
  const PagesAnnFabuFabu = /* @__PURE__ */ _export_sfc(_sfc_main$4, [["render", _sfc_render$3], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/pages/ann/fabu/fabu.vue"]]);
  const _sfc_main$3 = {};
  function _sfc_render$2(_ctx, _cache) {
    return " 记账 ";
  }
  const PagesAnnKaccountKaccount = /* @__PURE__ */ _export_sfc(_sfc_main$3, [["render", _sfc_render$2], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/pages/ann/kaccount/kaccount.vue"]]);
  const _sfc_main$2 = {
    name: "tuiNavigationBar",
    emits: ["init", "change"],
    props: {
      //NavigationBar标题
      title: {
        type: String,
        default: ""
      },
      //NavigationBar标题颜色
      color: {
        type: String,
        default: "#333"
      },
      //NavigationBar背景颜色,不支持rgb
      backgroundColor: {
        type: String,
        default: "#fff"
      },
      //是否需要分割线
      splitLine: {
        type: Boolean,
        default: false
      },
      //是否设置不透明度
      isOpacity: {
        type: Boolean,
        default: true
      },
      //不透明度最大值 0-1
      maxOpacity: {
        type: [Number, String],
        default: 1
      },
      //背景透明 【设置该属性，则背景透明，只出现内容，isOpacity和maxOpacity失效】
      transparent: {
        type: Boolean,
        default: false
      },
      //滚动条滚动距离
      scrollTop: {
        type: [Number, String],
        default: 0
      },
      /*
      	 isOpacity 为true时生效
      	 opacity=scrollTop /windowWidth * scrollRatio
      	*/
      scrollRatio: {
        type: [Number, String],
        default: 0.3
      },
      //是否自定义header内容
      isCustom: {
        type: Boolean,
        default: false
      },
      //是否沉浸式
      isImmersive: {
        type: Boolean,
        default: true
      },
      isFixed: {
        type: Boolean,
        default: true
      },
      //是否开启高斯模糊效果[仅在支持的浏览器有效果]
      backdropFilter: {
        type: Boolean,
        default: false
      },
      //下拉隐藏NavigationBar，主要针对有回弹效果ios端
      dropDownHide: {
        type: Boolean,
        default: false
      },
      //z-index设置
      zIndex: {
        type: [Number, String],
        default: 996
      }
    },
    watch: {
      scrollTop(newValue, oldValue) {
        if (this.isOpacity && !this.transparent) {
          this.opacityChange();
        }
      },
      backgroundColor(val) {
        if (val) {
          if (this.isOpacity) {
            this.background = this.hexToRgb(val);
          } else {
            this.background = this.transparent ? "rgba(0, 0, 0, 0)" : val;
          }
        }
      }
    },
    data() {
      return {
        width: 375,
        //header宽度
        left: 375,
        //小程序端 左侧距胶囊按钮距离
        height: 44,
        //header高度
        top: 0,
        scrollH: 1,
        //滚动总高度,计算opacity
        opacity: 1,
        //0-1
        statusBarHeight: 0,
        //状态栏高度
        background: "255,255,255",
        //header背景色
        dropDownOpacity: 1
      };
    },
    created() {
      this.dropDownOpacity = this.backdropFilter && 0;
      this.opacity = this.isOpacity || this.transparent ? 0 : this.maxOpacity;
      if (this.isOpacity) {
        this.background = this.hexToRgb(this.backgroundColor);
      } else {
        this.background = this.transparent ? "rgba(0, 0, 0, 0)" : this.backgroundColor;
      }
      let obj = {};
      uni.getSystemInfo({
        success: (res) => {
          this.statusBarHeight = res.statusBarHeight;
          this.width = res.windowWidth;
          this.left = obj.left || res.windowWidth;
          if (this.isImmersive) {
            this.height = obj.top ? obj.top + obj.height + 8 : res.statusBarHeight + 44;
          }
          this.scrollH = res.windowWidth * this.scrollRatio;
          this.top = obj.top ? obj.top + (obj.height - 32) / 2 : res.statusBarHeight + 6;
          this.$emit("init", {
            width: this.width,
            height: this.height,
            left: this.left,
            top: this.top,
            statusBarHeight: this.statusBarHeight,
            opacity: this.opacity,
            windowHeight: res.windowHeight
          });
        }
      });
    },
    methods: {
      hexToRgb(hex) {
        let rgb = "255,255,255";
        if (hex && ~hex.indexOf("#")) {
          if (hex.length === 4) {
            let text = hex.substring(1, 4);
            hex = "#" + text + text;
          }
          let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
          if (result) {
            rgb = `${parseInt(result[1], 16)},${parseInt(result[2], 16)},${parseInt(result[3], 16)}`;
          }
        }
        return rgb;
      },
      opacityChange() {
        if (this.dropDownHide) {
          if (this.scrollTop < 0) {
            if (this.dropDownOpacity > 0) {
              this.dropDownOpacity = 1 - Math.abs(this.scrollTop) / 30;
            }
          } else {
            this.dropDownOpacity = 1;
          }
        }
        let scroll = this.scrollTop <= 1 ? 0 : this.scrollTop;
        let opacity = scroll / this.scrollH;
        if (this.opacity >= this.maxOpacity && opacity >= this.maxOpacity || this.opacity == 0 && opacity == 0) {
          return;
        }
        this.opacity = opacity > this.maxOpacity ? this.maxOpacity : opacity;
        if (this.backdropFilter) {
          this.dropDownOpacity = this.opacity >= this.maxOpacity ? 1 : this.opacity;
        }
        this.$emit("change", {
          opacity: this.opacity
        });
      }
    }
  };
  function _sfc_render$1(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock(
      "view",
      {
        class: vue.normalizeClass(["tui-navigation-bar", { "tui-bar-line": $data.opacity > 0.85 && $props.splitLine, "tui-navbar-fixed": $props.isFixed, "tui-backdrop__filter": $props.backdropFilter && $data.dropDownOpacity > 0 }]),
        style: vue.normalizeStyle({ height: $data.height + "px", background: $props.isOpacity ? `rgba(${$data.background},${$data.opacity})` : $data.background, opacity: $data.dropDownOpacity, zIndex: $props.isFixed ? $props.zIndex : "auto" })
      },
      [
        $props.isImmersive ? (vue.openBlock(), vue.createElementBlock(
          "view",
          {
            key: 0,
            class: "tui-status-bar",
            style: vue.normalizeStyle({ height: $data.statusBarHeight + "px" })
          },
          null,
          4
          /* STYLE */
        )) : vue.createCommentVNode("v-if", true),
        $props.title && !$props.isCustom ? (vue.openBlock(), vue.createElementBlock(
          "view",
          {
            key: 1,
            class: "tui-navigation_bar-title",
            style: vue.normalizeStyle({ opacity: $props.transparent || $data.opacity >= $props.maxOpacity ? 1 : $data.opacity, color: $props.color, paddingTop: $data.top - $data.statusBarHeight + "px" })
          },
          vue.toDisplayString($props.title),
          5
          /* TEXT, STYLE */
        )) : vue.createCommentVNode("v-if", true),
        vue.renderSlot(_ctx.$slots, "default", {}, void 0, true)
      ],
      6
      /* CLASS, STYLE */
    );
  }
  const __easycom_1 = /* @__PURE__ */ _export_sfc(_sfc_main$2, [["render", _sfc_render$1], ["__scopeId", "data-v-5bb0503f"], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/components/thorui/tui-navigation-bar/tui-navigation-bar.vue"]]);
  const _sfc_main$1 = {
    components: {
      tuiNavigationBar: __easycom_1
    },
    data() {
      return {
        top: 0,
        //标题图标距离顶部距离
        opacity: 0,
        scrollTop: 0.5
      };
    },
    methods: {
      initNavigation(e) {
        this.opacity = e.opacity;
        this.top = e.top;
      },
      opacityChange(e) {
        this.opacity = e.opacity;
      },
      back() {
        uni.navigateBack();
      }
    },
    onPageScroll(e) {
      this.scrollTop = e.scrollTop;
    }
  };
  function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_tui_icon = resolveEasycom(vue.resolveDynamicComponent("tui-icon"), __easycom_0$4);
    const _component_tui_navigation_bar = resolveEasycom(vue.resolveDynamicComponent("tui-navigation-bar"), __easycom_1);
    return vue.openBlock(), vue.createElementBlock(
      vue.Fragment,
      null,
      [
        vue.createVNode(_component_tui_navigation_bar, {
          splitLine: "",
          onInit: $options.initNavigation,
          onChange: $options.opacityChange,
          scrollTop: $data.scrollTop,
          title: "我的",
          backgroundColor: "#fff",
          color: "#333"
        }, {
          default: vue.withCtx(() => [
            vue.createElementVNode(
              "view",
              {
                class: "tui-header-icon",
                style: vue.normalizeStyle({ marginTop: $data.top + "px" })
              },
              [
                vue.createElementVNode("view", { style: { "position": "relative" } }, [
                  vue.createVNode(_component_tui_icon, {
                    name: "message",
                    class: "top-message-cls"
                  })
                ]),
                vue.createElementVNode("view", null, [
                  vue.createVNode(_component_tui_icon, {
                    name: "setup",
                    class: "top-setup-cls"
                  })
                ])
              ],
              4
              /* STYLE */
            )
          ]),
          _: 1
          /* STABLE */
        }, 8, ["onInit", "onChange", "scrollTop"]),
        vue.createElementVNode("view", { class: "top-back" }, [
          vue.createElementVNode("view", { style: { "height": "88rpx" } }),
          vue.createElementVNode("view", null, [
            vue.createElementVNode("image", {
              src: "/static/images/my/go.png",
              class: "touxiang-image"
            })
          ]),
          vue.createElementVNode("view", { class: "scgzxhzj" }, [
            vue.createElementVNode("view", { class: "scgzxhzj-v" }, [
              vue.createElementVNode("view", { class: "scgzxhzj-count" }, "25"),
              vue.createElementVNode("view", null, "收藏")
            ]),
            vue.createElementVNode("view", { class: "scgzxhzj-v" }, [
              vue.createElementVNode("view", { class: "scgzxhzj-count" }, "22"),
              vue.createElementVNode("view", null, "关注")
            ]),
            vue.createElementVNode("view", { class: "scgzxhzj-v" }, [
              vue.createElementVNode("view", { class: "scgzxhzj-count" }, "3"),
              vue.createElementVNode("view", null, "喜欢")
            ]),
            vue.createElementVNode("view", { class: "scgzxhzj-v" }, [
              vue.createElementVNode("view", { class: "scgzxhzj-count" }, "44"),
              vue.createElementVNode("view", null, "足迹")
            ])
          ]),
          vue.createElementVNode("view", { class: "menu-container" }, [
            vue.createElementVNode("navigator", {
              url: "/pages/ann/supply/index",
              class: "menu-item"
            }, [
              vue.createElementVNode("image", {
                src: "/static/images/my/go.png",
                class: "menu-icon"
              }),
              vue.createElementVNode("text", null, "我的供应")
            ]),
            vue.createElementVNode("navigator", {
              url: "/pages/ann/demand/index",
              class: "menu-item"
            }, [
              vue.createElementVNode("image", {
                src: "/static/images/my/go.png",
                class: "menu-icon"
              }),
              vue.createElementVNode("text", null, "我的求购")
            ]),
            vue.createElementVNode("navigator", {
              url: "/pages/ann/orders/index",
              class: "menu-item"
            }, [
              vue.createElementVNode("image", {
                src: "/static/images/my/go.png",
                class: "menu-icon"
              }),
              vue.createElementVNode("text", null, "我的订单")
            ]),
            vue.createElementVNode("navigator", {
              url: "/pages/ann/storeManagement/index",
              class: "menu-item"
            }, [
              vue.createElementVNode("image", {
                src: "/static/images/my/go.png",
                class: "menu-icon"
              }),
              vue.createElementVNode("text", null, "店铺管理")
            ]),
            vue.createElementVNode("navigator", {
              url: "/pages/ann/reviews/index",
              class: "menu-item"
            }, [
              vue.createElementVNode("image", {
                src: "/static/images/my/go.png",
                class: "menu-icon"
              }),
              vue.createElementVNode("text", null, "我的评价")
            ])
          ])
        ]),
        vue.createElementVNode("view", { class: "help-section" }, [
          vue.createElementVNode("text", { class: "help-title" }, "帮助"),
          vue.createElementVNode("view", { class: "help-links" }, [
            vue.createElementVNode("navigator", {
              url: "/pages/feedback/index",
              class: "help-link"
            }, [
              vue.createElementVNode("image", {
                src: "/static/images/my/go.png",
                class: "menu-icon"
              }),
              vue.createElementVNode("text", null, "我的反馈")
            ]),
            vue.createElementVNode("navigator", {
              url: "/pages/chat/index",
              class: "help-link"
            }, [
              vue.createElementVNode("image", {
                src: "/static/images/my/go.png",
                class: "menu-icon"
              }),
              vue.createElementVNode("text", null, "在线客服")
            ]),
            vue.createElementVNode("navigator", {
              url: "/pages/phone/index",
              class: "help-link"
            }, [
              vue.createElementVNode("image", {
                src: "/static/images/my/go.png",
                class: "menu-icon"
              }),
              vue.createElementVNode("text", null, "客服电话")
            ]),
            vue.createElementVNode("navigator", {
              url: "/pages/settings/index",
              class: "help-link"
            }, [
              vue.createElementVNode("image", {
                src: "/static/images/my/go.png",
                class: "menu-icon"
              }),
              vue.createElementVNode("text", null, "更多设置")
            ])
          ])
        ])
      ],
      64
      /* STABLE_FRAGMENT */
    );
  }
  const PagesAnnMyMy = /* @__PURE__ */ _export_sfc(_sfc_main$1, [["render", _sfc_render], ["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/pages/ann/my/my.vue"]]);
  __definePage("pages/ann/index/index", PagesAnnIndexIndex);
  __definePage("pages/ann/supply/supply", PagesAnnSupplySupply);
  __definePage("pages/ann/orders/order", PagesAnnOrdersOrder);
  __definePage("pages/ann/index/maps", PagesAnnIndexMaps);
  __definePage("pages/ann/market/market", PagesAnnMarketMarket);
  __definePage("pages/ann/dangan/dindex", PagesAnnDanganDindex);
  __definePage("pages/ann/dangan/daadd", PagesAnnDanganDaadd);
  __definePage("pages/ann/dangan/empty", PagesAnnDanganEmpty);
  __definePage("pages/ann/dangan/dlist", PagesAnnDanganDlist);
  __definePage("pages/ann/fabu/fabu", PagesAnnFabuFabu);
  __definePage("pages/ann/kaccount/kaccount", PagesAnnKaccountKaccount);
  __definePage("pages/ann/my/my", PagesAnnMyMy);
  const _sfc_main = {
    onLaunch: function() {
      plus.screen.lockOrientation("portrait-primary");
    },
    onShow: function() {
    },
    onHide: function() {
    }
  };
  const App = /* @__PURE__ */ _export_sfc(_sfc_main, [["__file", "D:/项目/1.组件模板/ThorUI/nn-thor-ui/App.vue"]]);
  function checkPlatform() {
    const systemInfo = uni.getSystemInfoSync();
    return systemInfo.platform;
  }
  function isPC() {
    let systemInfo = uni.getSystemInfoSync();
    formatAppLog("log", "at utils/utils.js:8", systemInfo);
    if (systemInfo.platform === "android" || systemInfo.platform === "ios") {
      return false;
    } else if (systemInfo.platform === "windows" || systemInfo.platform === "mac") {
      return true;
    } else {
      return true;
    }
  }
  const tui = {
    //接口地址
    interfaceUrl: function() {
      return "https://www.thorui.cn";
    },
    toast: function(text, duration, success) {
      uni.showToast({
        duration: duration || 2e3,
        title: text || "出错啦~",
        icon: success ? "success" : "none"
      });
    },
    modal: function(title, content, showCancel, callback, confirmColor, confirmText) {
      uni.showModal({
        title: title || "提示",
        content,
        showCancel,
        cancelColor: "#555",
        confirmColor: confirmColor || "#5677fc",
        confirmText: confirmText || "确定",
        success(res) {
          if (res.confirm) {
            callback && callback(true);
          } else {
            callback && callback(false);
          }
        }
      });
    },
    isAndroid: function() {
      const res = uni.getSystemInfoSync();
      return res.platform.toLocaleLowerCase() == "android";
    },
    isPhoneX: function() {
      const res = uni.getSystemInfoSync();
      let iphonex = false;
      let models = ["iphonex", "iphonexr", "iphonexsmax", "iphone11", "iphone11pro", "iphone11promax"];
      const model = res.model.replace(/\s/g, "").toLowerCase();
      if (models.includes(model)) {
        iphonex = true;
      }
      return iphonex;
    },
    constNum: function() {
      let time = 0;
      time = this.isAndroid() ? 300 : 0;
      return time;
    },
    delayed: null,
    loadding: false,
    showLoading: function(title, mask = true) {
      uni.showLoading({
        mask,
        title: title || "请稍候..."
      });
    },
    /**
     * 请求数据处理
     * @param string url 请求地址
     * @param string method 请求方式
     *  GET or POST
     * @param {*} postData 请求参数
     * @param bool isDelay 是否延迟显示loading
     * @param bool isForm 数据格式
     *  true: 'application/x-www-form-urlencoded'
     *  false:'application/json'
     * @param bool hideLoading 是否隐藏loading
     *  true: 隐藏
     *  false:显示
     */
    request: async function(url, method, postData, isDelay, isForm, hideLoading) {
      tui.loadding && uni.hideLoading();
      tui.loadding = false;
      if (!hideLoading) {
        if (isDelay) {
          tui.delayed = setTimeout(() => {
            tui.loadding = true;
            tui.showLoading();
            clearTimeout(tui.delayed);
          }, 1e3);
        } else {
          tui.loadding = true;
          tui.showLoading();
        }
      }
      return new Promise((resolve, reject) => {
        uni.request({
          url: tui.interfaceUrl() + url,
          data: postData,
          header: {
            "content-type": isForm ? "application/x-www-form-urlencoded" : "application/json",
            "Authorization": tui.getToken()
          },
          method,
          //'GET','POST'
          dataType: "json",
          success: (res) => {
            clearTimeout(tui.delayed);
            tui.delayed = null;
            if (tui.loadding && !hideLoading) {
              uni.hideLoading();
            }
            resolve(res.data);
          },
          fail: (res) => {
            clearTimeout(tui.delayed);
            tui.delayed = null;
            tui.toast("网络不给力，请稍后再试~");
            reject(res);
          }
        });
      });
    },
    /**
     * 上传文件
     * @param string url 请求地址
     * @param string src 文件路径
     */
    uploadFile: function(url, src) {
      tui.showLoading();
      return new Promise((resolve, reject) => {
        uni.uploadFile({
          url: tui.interfaceUrl() + url,
          filePath: src,
          name: "file",
          header: {
            "token": tui.getToken()
          },
          formData: {
            // sizeArrayText:""
          },
          success: function(res) {
            uni.hideLoading();
            let d = JSON.parse(res.data.replace(/\ufeff/g, "") || "{}");
            if (d.code % 100 == 0) {
              let fileObj = d.data;
              resolve(fileObj);
            } else {
              that.toast(res.msg);
            }
          },
          fail: function(res) {
            reject(res);
            that.toast(res.msg);
          }
        });
      });
    },
    tuiJsonp: function(url, callback, callbackname) {
    },
    //设置用户信息
    setUserInfo: function(mobile, token) {
      uni.setStorageSync("thorui_mobile", mobile);
    },
    //获取token
    getToken() {
      return uni.getStorageSync("thorui_token");
    },
    //判断是否登录
    isLogin: function() {
      return uni.getStorageSync("thorui_mobile") ? true : false;
    },
    //跳转页面，校验登录状态
    href(url, isVerify) {
      if (isVerify && !tui.isLogin()) {
        uni.navigateTo({
          url: "/pages/common/login/login"
        });
      } else {
        uni.navigateTo({
          url
        });
      }
    }
  };
  function getDevtoolsGlobalHook() {
    return getTarget().__VUE_DEVTOOLS_GLOBAL_HOOK__;
  }
  function getTarget() {
    return typeof navigator !== "undefined" && typeof window !== "undefined" ? window : typeof global !== "undefined" ? global : {};
  }
  const isProxyAvailable = typeof Proxy === "function";
  const HOOK_SETUP = "devtools-plugin:setup";
  const HOOK_PLUGIN_SETTINGS_SET = "plugin:settings:set";
  class ApiProxy {
    constructor(plugin, hook) {
      this.target = null;
      this.targetQueue = [];
      this.onQueue = [];
      this.plugin = plugin;
      this.hook = hook;
      const defaultSettings = {};
      if (plugin.settings) {
        for (const id in plugin.settings) {
          const item = plugin.settings[id];
          defaultSettings[id] = item.defaultValue;
        }
      }
      const localSettingsSaveId = `__vue-devtools-plugin-settings__${plugin.id}`;
      let currentSettings = { ...defaultSettings };
      try {
        const raw = localStorage.getItem(localSettingsSaveId);
        const data = JSON.parse(raw);
        Object.assign(currentSettings, data);
      } catch (e) {
      }
      this.fallbacks = {
        getSettings() {
          return currentSettings;
        },
        setSettings(value) {
          try {
            localStorage.setItem(localSettingsSaveId, JSON.stringify(value));
          } catch (e) {
          }
          currentSettings = value;
        }
      };
      hook.on(HOOK_PLUGIN_SETTINGS_SET, (pluginId, value) => {
        if (pluginId === this.plugin.id) {
          this.fallbacks.setSettings(value);
        }
      });
      this.proxiedOn = new Proxy({}, {
        get: (_target, prop) => {
          if (this.target) {
            return this.target.on[prop];
          } else {
            return (...args) => {
              this.onQueue.push({
                method: prop,
                args
              });
            };
          }
        }
      });
      this.proxiedTarget = new Proxy({}, {
        get: (_target, prop) => {
          if (this.target) {
            return this.target[prop];
          } else if (prop === "on") {
            return this.proxiedOn;
          } else if (Object.keys(this.fallbacks).includes(prop)) {
            return (...args) => {
              this.targetQueue.push({
                method: prop,
                args,
                resolve: () => {
                }
              });
              return this.fallbacks[prop](...args);
            };
          } else {
            return (...args) => {
              return new Promise((resolve) => {
                this.targetQueue.push({
                  method: prop,
                  args,
                  resolve
                });
              });
            };
          }
        }
      });
    }
    async setRealTarget(target) {
      this.target = target;
      for (const item of this.onQueue) {
        this.target.on[item.method](...item.args);
      }
      for (const item of this.targetQueue) {
        item.resolve(await this.target[item.method](...item.args));
      }
    }
  }
  function setupDevtoolsPlugin(pluginDescriptor, setupFn) {
    const target = getTarget();
    const hook = getDevtoolsGlobalHook();
    const enableProxy = isProxyAvailable && pluginDescriptor.enableEarlyProxy;
    if (hook && (target.__VUE_DEVTOOLS_PLUGIN_API_AVAILABLE__ || !enableProxy)) {
      hook.emit(HOOK_SETUP, pluginDescriptor, setupFn);
    } else {
      const proxy = enableProxy ? new ApiProxy(pluginDescriptor, hook) : null;
      const list = target.__VUE_DEVTOOLS_PLUGINS__ = target.__VUE_DEVTOOLS_PLUGINS__ || [];
      list.push({
        pluginDescriptor,
        setupFn,
        proxy
      });
      if (proxy)
        setupFn(proxy.proxiedTarget);
    }
  }
  /*!
   * vuex v4.1.0
   * (c) 2022 Evan You
   * @license MIT
   */
  var storeKey = "store";
  function forEachValue(obj, fn) {
    Object.keys(obj).forEach(function(key) {
      return fn(obj[key], key);
    });
  }
  function isObject(obj) {
    return obj !== null && typeof obj === "object";
  }
  function isPromise(val) {
    return val && typeof val.then === "function";
  }
  function assert(condition, msg) {
    if (!condition) {
      throw new Error("[vuex] " + msg);
    }
  }
  function partial(fn, arg) {
    return function() {
      return fn(arg);
    };
  }
  function genericSubscribe(fn, subs, options) {
    if (subs.indexOf(fn) < 0) {
      options && options.prepend ? subs.unshift(fn) : subs.push(fn);
    }
    return function() {
      var i = subs.indexOf(fn);
      if (i > -1) {
        subs.splice(i, 1);
      }
    };
  }
  function resetStore(store2, hot) {
    store2._actions = /* @__PURE__ */ Object.create(null);
    store2._mutations = /* @__PURE__ */ Object.create(null);
    store2._wrappedGetters = /* @__PURE__ */ Object.create(null);
    store2._modulesNamespaceMap = /* @__PURE__ */ Object.create(null);
    var state = store2.state;
    installModule(store2, state, [], store2._modules.root, true);
    resetStoreState(store2, state, hot);
  }
  function resetStoreState(store2, state, hot) {
    var oldState = store2._state;
    var oldScope = store2._scope;
    store2.getters = {};
    store2._makeLocalGettersCache = /* @__PURE__ */ Object.create(null);
    var wrappedGetters = store2._wrappedGetters;
    var computedObj = {};
    var computedCache = {};
    var scope = vue.effectScope(true);
    scope.run(function() {
      forEachValue(wrappedGetters, function(fn, key) {
        computedObj[key] = partial(fn, store2);
        computedCache[key] = vue.computed(function() {
          return computedObj[key]();
        });
        Object.defineProperty(store2.getters, key, {
          get: function() {
            return computedCache[key].value;
          },
          enumerable: true
          // for local getters
        });
      });
    });
    store2._state = vue.reactive({
      data: state
    });
    store2._scope = scope;
    if (store2.strict) {
      enableStrictMode(store2);
    }
    if (oldState) {
      if (hot) {
        store2._withCommit(function() {
          oldState.data = null;
        });
      }
    }
    if (oldScope) {
      oldScope.stop();
    }
  }
  function installModule(store2, rootState, path, module, hot) {
    var isRoot = !path.length;
    var namespace = store2._modules.getNamespace(path);
    if (module.namespaced) {
      if (store2._modulesNamespaceMap[namespace] && true) {
        console.error("[vuex] duplicate namespace " + namespace + " for the namespaced module " + path.join("/"));
      }
      store2._modulesNamespaceMap[namespace] = module;
    }
    if (!isRoot && !hot) {
      var parentState = getNestedState(rootState, path.slice(0, -1));
      var moduleName = path[path.length - 1];
      store2._withCommit(function() {
        {
          if (moduleName in parentState) {
            console.warn(
              '[vuex] state field "' + moduleName + '" was overridden by a module with the same name at "' + path.join(".") + '"'
            );
          }
        }
        parentState[moduleName] = module.state;
      });
    }
    var local = module.context = makeLocalContext(store2, namespace, path);
    module.forEachMutation(function(mutation, key) {
      var namespacedType = namespace + key;
      registerMutation(store2, namespacedType, mutation, local);
    });
    module.forEachAction(function(action, key) {
      var type = action.root ? key : namespace + key;
      var handler = action.handler || action;
      registerAction(store2, type, handler, local);
    });
    module.forEachGetter(function(getter, key) {
      var namespacedType = namespace + key;
      registerGetter(store2, namespacedType, getter, local);
    });
    module.forEachChild(function(child, key) {
      installModule(store2, rootState, path.concat(key), child, hot);
    });
  }
  function makeLocalContext(store2, namespace, path) {
    var noNamespace = namespace === "";
    var local = {
      dispatch: noNamespace ? store2.dispatch : function(_type, _payload, _options) {
        var args = unifyObjectStyle(_type, _payload, _options);
        var payload = args.payload;
        var options = args.options;
        var type = args.type;
        if (!options || !options.root) {
          type = namespace + type;
          if (!store2._actions[type]) {
            console.error("[vuex] unknown local action type: " + args.type + ", global type: " + type);
            return;
          }
        }
        return store2.dispatch(type, payload);
      },
      commit: noNamespace ? store2.commit : function(_type, _payload, _options) {
        var args = unifyObjectStyle(_type, _payload, _options);
        var payload = args.payload;
        var options = args.options;
        var type = args.type;
        if (!options || !options.root) {
          type = namespace + type;
          if (!store2._mutations[type]) {
            console.error("[vuex] unknown local mutation type: " + args.type + ", global type: " + type);
            return;
          }
        }
        store2.commit(type, payload, options);
      }
    };
    Object.defineProperties(local, {
      getters: {
        get: noNamespace ? function() {
          return store2.getters;
        } : function() {
          return makeLocalGetters(store2, namespace);
        }
      },
      state: {
        get: function() {
          return getNestedState(store2.state, path);
        }
      }
    });
    return local;
  }
  function makeLocalGetters(store2, namespace) {
    if (!store2._makeLocalGettersCache[namespace]) {
      var gettersProxy = {};
      var splitPos = namespace.length;
      Object.keys(store2.getters).forEach(function(type) {
        if (type.slice(0, splitPos) !== namespace) {
          return;
        }
        var localType = type.slice(splitPos);
        Object.defineProperty(gettersProxy, localType, {
          get: function() {
            return store2.getters[type];
          },
          enumerable: true
        });
      });
      store2._makeLocalGettersCache[namespace] = gettersProxy;
    }
    return store2._makeLocalGettersCache[namespace];
  }
  function registerMutation(store2, type, handler, local) {
    var entry = store2._mutations[type] || (store2._mutations[type] = []);
    entry.push(function wrappedMutationHandler(payload) {
      handler.call(store2, local.state, payload);
    });
  }
  function registerAction(store2, type, handler, local) {
    var entry = store2._actions[type] || (store2._actions[type] = []);
    entry.push(function wrappedActionHandler(payload) {
      var res = handler.call(store2, {
        dispatch: local.dispatch,
        commit: local.commit,
        getters: local.getters,
        state: local.state,
        rootGetters: store2.getters,
        rootState: store2.state
      }, payload);
      if (!isPromise(res)) {
        res = Promise.resolve(res);
      }
      if (store2._devtoolHook) {
        return res.catch(function(err) {
          store2._devtoolHook.emit("vuex:error", err);
          throw err;
        });
      } else {
        return res;
      }
    });
  }
  function registerGetter(store2, type, rawGetter, local) {
    if (store2._wrappedGetters[type]) {
      {
        console.error("[vuex] duplicate getter key: " + type);
      }
      return;
    }
    store2._wrappedGetters[type] = function wrappedGetter(store22) {
      return rawGetter(
        local.state,
        // local state
        local.getters,
        // local getters
        store22.state,
        // root state
        store22.getters
        // root getters
      );
    };
  }
  function enableStrictMode(store2) {
    vue.watch(function() {
      return store2._state.data;
    }, function() {
      {
        assert(store2._committing, "do not mutate vuex store state outside mutation handlers.");
      }
    }, { deep: true, flush: "sync" });
  }
  function getNestedState(state, path) {
    return path.reduce(function(state2, key) {
      return state2[key];
    }, state);
  }
  function unifyObjectStyle(type, payload, options) {
    if (isObject(type) && type.type) {
      options = payload;
      payload = type;
      type = type.type;
    }
    {
      assert(typeof type === "string", "expects string as the type, but found " + typeof type + ".");
    }
    return { type, payload, options };
  }
  var LABEL_VUEX_BINDINGS = "vuex bindings";
  var MUTATIONS_LAYER_ID = "vuex:mutations";
  var ACTIONS_LAYER_ID = "vuex:actions";
  var INSPECTOR_ID = "vuex";
  var actionId = 0;
  function addDevtools(app, store2) {
    setupDevtoolsPlugin(
      {
        id: "org.vuejs.vuex",
        app,
        label: "Vuex",
        homepage: "https://next.vuex.vuejs.org/",
        logo: "https://vuejs.org/images/icons/favicon-96x96.png",
        packageName: "vuex",
        componentStateTypes: [LABEL_VUEX_BINDINGS]
      },
      function(api) {
        api.addTimelineLayer({
          id: MUTATIONS_LAYER_ID,
          label: "Vuex Mutations",
          color: COLOR_LIME_500
        });
        api.addTimelineLayer({
          id: ACTIONS_LAYER_ID,
          label: "Vuex Actions",
          color: COLOR_LIME_500
        });
        api.addInspector({
          id: INSPECTOR_ID,
          label: "Vuex",
          icon: "storage",
          treeFilterPlaceholder: "Filter stores..."
        });
        api.on.getInspectorTree(function(payload) {
          if (payload.app === app && payload.inspectorId === INSPECTOR_ID) {
            if (payload.filter) {
              var nodes = [];
              flattenStoreForInspectorTree(nodes, store2._modules.root, payload.filter, "");
              payload.rootNodes = nodes;
            } else {
              payload.rootNodes = [
                formatStoreForInspectorTree(store2._modules.root, "")
              ];
            }
          }
        });
        api.on.getInspectorState(function(payload) {
          if (payload.app === app && payload.inspectorId === INSPECTOR_ID) {
            var modulePath = payload.nodeId;
            makeLocalGetters(store2, modulePath);
            payload.state = formatStoreForInspectorState(
              getStoreModule(store2._modules, modulePath),
              modulePath === "root" ? store2.getters : store2._makeLocalGettersCache,
              modulePath
            );
          }
        });
        api.on.editInspectorState(function(payload) {
          if (payload.app === app && payload.inspectorId === INSPECTOR_ID) {
            var modulePath = payload.nodeId;
            var path = payload.path;
            if (modulePath !== "root") {
              path = modulePath.split("/").filter(Boolean).concat(path);
            }
            store2._withCommit(function() {
              payload.set(store2._state.data, path, payload.state.value);
            });
          }
        });
        store2.subscribe(function(mutation, state) {
          var data = {};
          if (mutation.payload) {
            data.payload = mutation.payload;
          }
          data.state = state;
          api.notifyComponentUpdate();
          api.sendInspectorTree(INSPECTOR_ID);
          api.sendInspectorState(INSPECTOR_ID);
          api.addTimelineEvent({
            layerId: MUTATIONS_LAYER_ID,
            event: {
              time: Date.now(),
              title: mutation.type,
              data
            }
          });
        });
        store2.subscribeAction({
          before: function(action, state) {
            var data = {};
            if (action.payload) {
              data.payload = action.payload;
            }
            action._id = actionId++;
            action._time = Date.now();
            data.state = state;
            api.addTimelineEvent({
              layerId: ACTIONS_LAYER_ID,
              event: {
                time: action._time,
                title: action.type,
                groupId: action._id,
                subtitle: "start",
                data
              }
            });
          },
          after: function(action, state) {
            var data = {};
            var duration = Date.now() - action._time;
            data.duration = {
              _custom: {
                type: "duration",
                display: duration + "ms",
                tooltip: "Action duration",
                value: duration
              }
            };
            if (action.payload) {
              data.payload = action.payload;
            }
            data.state = state;
            api.addTimelineEvent({
              layerId: ACTIONS_LAYER_ID,
              event: {
                time: Date.now(),
                title: action.type,
                groupId: action._id,
                subtitle: "end",
                data
              }
            });
          }
        });
      }
    );
  }
  var COLOR_LIME_500 = 8702998;
  var COLOR_DARK = 6710886;
  var COLOR_WHITE = 16777215;
  var TAG_NAMESPACED = {
    label: "namespaced",
    textColor: COLOR_WHITE,
    backgroundColor: COLOR_DARK
  };
  function extractNameFromPath(path) {
    return path && path !== "root" ? path.split("/").slice(-2, -1)[0] : "Root";
  }
  function formatStoreForInspectorTree(module, path) {
    return {
      id: path || "root",
      // all modules end with a `/`, we want the last segment only
      // cart/ -> cart
      // nested/cart/ -> cart
      label: extractNameFromPath(path),
      tags: module.namespaced ? [TAG_NAMESPACED] : [],
      children: Object.keys(module._children).map(
        function(moduleName) {
          return formatStoreForInspectorTree(
            module._children[moduleName],
            path + moduleName + "/"
          );
        }
      )
    };
  }
  function flattenStoreForInspectorTree(result, module, filter, path) {
    if (path.includes(filter)) {
      result.push({
        id: path || "root",
        label: path.endsWith("/") ? path.slice(0, path.length - 1) : path || "Root",
        tags: module.namespaced ? [TAG_NAMESPACED] : []
      });
    }
    Object.keys(module._children).forEach(function(moduleName) {
      flattenStoreForInspectorTree(result, module._children[moduleName], filter, path + moduleName + "/");
    });
  }
  function formatStoreForInspectorState(module, getters, path) {
    getters = path === "root" ? getters : getters[path];
    var gettersKeys = Object.keys(getters);
    var storeState = {
      state: Object.keys(module.state).map(function(key) {
        return {
          key,
          editable: true,
          value: module.state[key]
        };
      })
    };
    if (gettersKeys.length) {
      var tree = transformPathsToObjectTree(getters);
      storeState.getters = Object.keys(tree).map(function(key) {
        return {
          key: key.endsWith("/") ? extractNameFromPath(key) : key,
          editable: false,
          value: canThrow(function() {
            return tree[key];
          })
        };
      });
    }
    return storeState;
  }
  function transformPathsToObjectTree(getters) {
    var result = {};
    Object.keys(getters).forEach(function(key) {
      var path = key.split("/");
      if (path.length > 1) {
        var target = result;
        var leafKey = path.pop();
        path.forEach(function(p) {
          if (!target[p]) {
            target[p] = {
              _custom: {
                value: {},
                display: p,
                tooltip: "Module",
                abstract: true
              }
            };
          }
          target = target[p]._custom.value;
        });
        target[leafKey] = canThrow(function() {
          return getters[key];
        });
      } else {
        result[key] = canThrow(function() {
          return getters[key];
        });
      }
    });
    return result;
  }
  function getStoreModule(moduleMap, path) {
    var names = path.split("/").filter(function(n) {
      return n;
    });
    return names.reduce(
      function(module, moduleName, i) {
        var child = module[moduleName];
        if (!child) {
          throw new Error('Missing module "' + moduleName + '" for path "' + path + '".');
        }
        return i === names.length - 1 ? child : child._children;
      },
      path === "root" ? moduleMap : moduleMap.root._children
    );
  }
  function canThrow(cb) {
    try {
      return cb();
    } catch (e) {
      return e;
    }
  }
  var Module = function Module2(rawModule, runtime) {
    this.runtime = runtime;
    this._children = /* @__PURE__ */ Object.create(null);
    this._rawModule = rawModule;
    var rawState = rawModule.state;
    this.state = (typeof rawState === "function" ? rawState() : rawState) || {};
  };
  var prototypeAccessors$1 = { namespaced: { configurable: true } };
  prototypeAccessors$1.namespaced.get = function() {
    return !!this._rawModule.namespaced;
  };
  Module.prototype.addChild = function addChild(key, module) {
    this._children[key] = module;
  };
  Module.prototype.removeChild = function removeChild(key) {
    delete this._children[key];
  };
  Module.prototype.getChild = function getChild(key) {
    return this._children[key];
  };
  Module.prototype.hasChild = function hasChild(key) {
    return key in this._children;
  };
  Module.prototype.update = function update(rawModule) {
    this._rawModule.namespaced = rawModule.namespaced;
    if (rawModule.actions) {
      this._rawModule.actions = rawModule.actions;
    }
    if (rawModule.mutations) {
      this._rawModule.mutations = rawModule.mutations;
    }
    if (rawModule.getters) {
      this._rawModule.getters = rawModule.getters;
    }
  };
  Module.prototype.forEachChild = function forEachChild(fn) {
    forEachValue(this._children, fn);
  };
  Module.prototype.forEachGetter = function forEachGetter(fn) {
    if (this._rawModule.getters) {
      forEachValue(this._rawModule.getters, fn);
    }
  };
  Module.prototype.forEachAction = function forEachAction(fn) {
    if (this._rawModule.actions) {
      forEachValue(this._rawModule.actions, fn);
    }
  };
  Module.prototype.forEachMutation = function forEachMutation(fn) {
    if (this._rawModule.mutations) {
      forEachValue(this._rawModule.mutations, fn);
    }
  };
  Object.defineProperties(Module.prototype, prototypeAccessors$1);
  var ModuleCollection = function ModuleCollection2(rawRootModule) {
    this.register([], rawRootModule, false);
  };
  ModuleCollection.prototype.get = function get(path) {
    return path.reduce(function(module, key) {
      return module.getChild(key);
    }, this.root);
  };
  ModuleCollection.prototype.getNamespace = function getNamespace(path) {
    var module = this.root;
    return path.reduce(function(namespace, key) {
      module = module.getChild(key);
      return namespace + (module.namespaced ? key + "/" : "");
    }, "");
  };
  ModuleCollection.prototype.update = function update$1(rawRootModule) {
    update2([], this.root, rawRootModule);
  };
  ModuleCollection.prototype.register = function register(path, rawModule, runtime) {
    var this$1$1 = this;
    if (runtime === void 0)
      runtime = true;
    {
      assertRawModule(path, rawModule);
    }
    var newModule = new Module(rawModule, runtime);
    if (path.length === 0) {
      this.root = newModule;
    } else {
      var parent = this.get(path.slice(0, -1));
      parent.addChild(path[path.length - 1], newModule);
    }
    if (rawModule.modules) {
      forEachValue(rawModule.modules, function(rawChildModule, key) {
        this$1$1.register(path.concat(key), rawChildModule, runtime);
      });
    }
  };
  ModuleCollection.prototype.unregister = function unregister(path) {
    var parent = this.get(path.slice(0, -1));
    var key = path[path.length - 1];
    var child = parent.getChild(key);
    if (!child) {
      {
        console.warn(
          "[vuex] trying to unregister module '" + key + "', which is not registered"
        );
      }
      return;
    }
    if (!child.runtime) {
      return;
    }
    parent.removeChild(key);
  };
  ModuleCollection.prototype.isRegistered = function isRegistered(path) {
    var parent = this.get(path.slice(0, -1));
    var key = path[path.length - 1];
    if (parent) {
      return parent.hasChild(key);
    }
    return false;
  };
  function update2(path, targetModule, newModule) {
    {
      assertRawModule(path, newModule);
    }
    targetModule.update(newModule);
    if (newModule.modules) {
      for (var key in newModule.modules) {
        if (!targetModule.getChild(key)) {
          {
            console.warn(
              "[vuex] trying to add a new module '" + key + "' on hot reloading, manual reload is needed"
            );
          }
          return;
        }
        update2(
          path.concat(key),
          targetModule.getChild(key),
          newModule.modules[key]
        );
      }
    }
  }
  var functionAssert = {
    assert: function(value) {
      return typeof value === "function";
    },
    expected: "function"
  };
  var objectAssert = {
    assert: function(value) {
      return typeof value === "function" || typeof value === "object" && typeof value.handler === "function";
    },
    expected: 'function or object with "handler" function'
  };
  var assertTypes = {
    getters: functionAssert,
    mutations: functionAssert,
    actions: objectAssert
  };
  function assertRawModule(path, rawModule) {
    Object.keys(assertTypes).forEach(function(key) {
      if (!rawModule[key]) {
        return;
      }
      var assertOptions = assertTypes[key];
      forEachValue(rawModule[key], function(value, type) {
        assert(
          assertOptions.assert(value),
          makeAssertionMessage(path, key, type, value, assertOptions.expected)
        );
      });
    });
  }
  function makeAssertionMessage(path, key, type, value, expected) {
    var buf = key + " should be " + expected + ' but "' + key + "." + type + '"';
    if (path.length > 0) {
      buf += ' in module "' + path.join(".") + '"';
    }
    buf += " is " + JSON.stringify(value) + ".";
    return buf;
  }
  function createStore(options) {
    return new Store(options);
  }
  var Store = function Store2(options) {
    var this$1$1 = this;
    if (options === void 0)
      options = {};
    {
      assert(typeof Promise !== "undefined", "vuex requires a Promise polyfill in this browser.");
      assert(this instanceof Store2, "store must be called with the new operator.");
    }
    var plugins = options.plugins;
    if (plugins === void 0)
      plugins = [];
    var strict = options.strict;
    if (strict === void 0)
      strict = false;
    var devtools = options.devtools;
    this._committing = false;
    this._actions = /* @__PURE__ */ Object.create(null);
    this._actionSubscribers = [];
    this._mutations = /* @__PURE__ */ Object.create(null);
    this._wrappedGetters = /* @__PURE__ */ Object.create(null);
    this._modules = new ModuleCollection(options);
    this._modulesNamespaceMap = /* @__PURE__ */ Object.create(null);
    this._subscribers = [];
    this._makeLocalGettersCache = /* @__PURE__ */ Object.create(null);
    this._scope = null;
    this._devtools = devtools;
    var store2 = this;
    var ref = this;
    var dispatch2 = ref.dispatch;
    var commit2 = ref.commit;
    this.dispatch = function boundDispatch(type, payload) {
      return dispatch2.call(store2, type, payload);
    };
    this.commit = function boundCommit(type, payload, options2) {
      return commit2.call(store2, type, payload, options2);
    };
    this.strict = strict;
    var state = this._modules.root.state;
    installModule(this, state, [], this._modules.root);
    resetStoreState(this, state);
    plugins.forEach(function(plugin) {
      return plugin(this$1$1);
    });
  };
  var prototypeAccessors = { state: { configurable: true } };
  Store.prototype.install = function install(app, injectKey) {
    app.provide(injectKey || storeKey, this);
    app.config.globalProperties.$store = this;
    var useDevtools = this._devtools !== void 0 ? this._devtools : true;
    if (useDevtools) {
      addDevtools(app, this);
    }
  };
  prototypeAccessors.state.get = function() {
    return this._state.data;
  };
  prototypeAccessors.state.set = function(v) {
    {
      assert(false, "use store.replaceState() to explicit replace store state.");
    }
  };
  Store.prototype.commit = function commit(_type, _payload, _options) {
    var this$1$1 = this;
    var ref = unifyObjectStyle(_type, _payload, _options);
    var type = ref.type;
    var payload = ref.payload;
    var options = ref.options;
    var mutation = { type, payload };
    var entry = this._mutations[type];
    if (!entry) {
      {
        console.error("[vuex] unknown mutation type: " + type);
      }
      return;
    }
    this._withCommit(function() {
      entry.forEach(function commitIterator(handler) {
        handler(payload);
      });
    });
    this._subscribers.slice().forEach(function(sub) {
      return sub(mutation, this$1$1.state);
    });
    if (options && options.silent) {
      console.warn(
        "[vuex] mutation type: " + type + ". Silent option has been removed. Use the filter functionality in the vue-devtools"
      );
    }
  };
  Store.prototype.dispatch = function dispatch(_type, _payload) {
    var this$1$1 = this;
    var ref = unifyObjectStyle(_type, _payload);
    var type = ref.type;
    var payload = ref.payload;
    var action = { type, payload };
    var entry = this._actions[type];
    if (!entry) {
      {
        console.error("[vuex] unknown action type: " + type);
      }
      return;
    }
    try {
      this._actionSubscribers.slice().filter(function(sub) {
        return sub.before;
      }).forEach(function(sub) {
        return sub.before(action, this$1$1.state);
      });
    } catch (e) {
      {
        console.warn("[vuex] error in before action subscribers: ");
        console.error(e);
      }
    }
    var result = entry.length > 1 ? Promise.all(entry.map(function(handler) {
      return handler(payload);
    })) : entry[0](payload);
    return new Promise(function(resolve, reject) {
      result.then(function(res) {
        try {
          this$1$1._actionSubscribers.filter(function(sub) {
            return sub.after;
          }).forEach(function(sub) {
            return sub.after(action, this$1$1.state);
          });
        } catch (e) {
          {
            console.warn("[vuex] error in after action subscribers: ");
            console.error(e);
          }
        }
        resolve(res);
      }, function(error) {
        try {
          this$1$1._actionSubscribers.filter(function(sub) {
            return sub.error;
          }).forEach(function(sub) {
            return sub.error(action, this$1$1.state, error);
          });
        } catch (e) {
          {
            console.warn("[vuex] error in error action subscribers: ");
            console.error(e);
          }
        }
        reject(error);
      });
    });
  };
  Store.prototype.subscribe = function subscribe(fn, options) {
    return genericSubscribe(fn, this._subscribers, options);
  };
  Store.prototype.subscribeAction = function subscribeAction(fn, options) {
    var subs = typeof fn === "function" ? { before: fn } : fn;
    return genericSubscribe(subs, this._actionSubscribers, options);
  };
  Store.prototype.watch = function watch$1(getter, cb, options) {
    var this$1$1 = this;
    {
      assert(typeof getter === "function", "store.watch only accepts a function.");
    }
    return vue.watch(function() {
      return getter(this$1$1.state, this$1$1.getters);
    }, cb, Object.assign({}, options));
  };
  Store.prototype.replaceState = function replaceState(state) {
    var this$1$1 = this;
    this._withCommit(function() {
      this$1$1._state.data = state;
    });
  };
  Store.prototype.registerModule = function registerModule(path, rawModule, options) {
    if (options === void 0)
      options = {};
    if (typeof path === "string") {
      path = [path];
    }
    {
      assert(Array.isArray(path), "module path must be a string or an Array.");
      assert(path.length > 0, "cannot register the root module by using registerModule.");
    }
    this._modules.register(path, rawModule);
    installModule(this, this.state, path, this._modules.get(path), options.preserveState);
    resetStoreState(this, this.state);
  };
  Store.prototype.unregisterModule = function unregisterModule(path) {
    var this$1$1 = this;
    if (typeof path === "string") {
      path = [path];
    }
    {
      assert(Array.isArray(path), "module path must be a string or an Array.");
    }
    this._modules.unregister(path);
    this._withCommit(function() {
      var parentState = getNestedState(this$1$1.state, path.slice(0, -1));
      delete parentState[path[path.length - 1]];
    });
    resetStore(this);
  };
  Store.prototype.hasModule = function hasModule(path) {
    if (typeof path === "string") {
      path = [path];
    }
    {
      assert(Array.isArray(path), "module path must be a string or an Array.");
    }
    return this._modules.isRegistered(path);
  };
  Store.prototype.hotUpdate = function hotUpdate(newOptions) {
    this._modules.update(newOptions);
    resetStore(this, true);
  };
  Store.prototype._withCommit = function _withCommit(fn) {
    var committing = this._committing;
    this._committing = true;
    fn();
    this._committing = committing;
  };
  Object.defineProperties(Store.prototype, prototypeAccessors);
  const store$1 = createStore({
    state: {
      //用户登录手机号
      mobile: uni.getStorageSync("thorui_mobile") || "echo.",
      //是否登录 项目中改为真实登录信息判断，如token
      isLogin: uni.getStorageSync("thorui_mobile") ? true : false,
      //登录后跳转的页面路径 + 页面参数
      returnUrl: "",
      //app版本
      version: "2.9.5",
      //当前是否有网络连接
      networkConnected: true,
      isOnline: true,
      userAvatar: "/static/images/my/mine_avatar_3x.jpg"
    },
    mutations: {
      login(state, payload) {
        if (payload) {
          state.mobile = payload.mobile;
        }
        state.isLogin = true;
      },
      logout(state) {
        state.mobile = "";
        state.isLogin = false;
        state.returnUrl = "";
      },
      setReturnUrl(state, returnUrl) {
        state.returnUrl = returnUrl;
      },
      networkChange(state, payload) {
        state.networkConnected = payload.isConnected;
      },
      setOnline(state, payload) {
        state.isOnline = payload.isOnline;
      },
      setAvatar(state, payload) {
        state.userAvatar = payload.avatar;
      }
    },
    actions: {
      getOnlineStatus: async function({
        commit,
        state
      }) {
        return await new Promise((resolve, reject) => {
          resolve(true);
        });
      }
    }
  });
  const base = {
    /**
     * content-type:
     * form=>application/x-www-form-urlencoded
     * json=>application/json
     * ......
     */
    config() {
      return {
        //接口域名：https://base.com
        host: "",
        //接口地址：/login
        url: "",
        data: {},
        header: {
          "content-type": "application/json"
        },
        //有效值必须大写
        method: "POST",
        //大于0时才生效，否则使用全局配置或者默认值
        timeout: 0,
        dataType: "json",
        //String，不同接口请求名称不可相同，否则会拦截重复key的请求，不传默认不拦截
        requestTaskKey: "",
        //是否只返回简洁的接口数据：true 仅返回接口数据data，false 返回包含header、statusCode、errMsg、data等数据
        concise: false,
        showLoading: true,
        errorMsg: "网络不给力，请稍后再试~"
      };
    },
    getOptions(config) {
      let options = {
        ...config
      };
      ["host", "timeout", "requestTaskKey", "showLoading", "errorMsg"].forEach((item) => {
        delete options[item];
      });
      return options;
    },
    merge(a, b) {
      return Object.assign({}, a, b);
    },
    mergeConfig(defaultConfig, config, init) {
      let header = base.merge(defaultConfig.header, config.header || {});
      let params = base.merge(defaultConfig, config);
      params.header = header;
      if (!init) {
        let url = base.combineURLs(params.host, params.url);
        params.url = url;
      }
      return params;
    },
    //如果host为空，则直接使用传入的目标地址
    combineURLs(host, target) {
      return host ? host.replace(/\s+/g, "") + "/" + target.replace(/\s+/g, "").replace(/^\/+/, "") : target;
    },
    toast(text, duration, success) {
      uni.showToast({
        title: text || "出错啦~",
        icon: success ? "success" : "none",
        duration: duration || 2e3
      });
    },
    showLoading(title, mask = true) {
      uni.showLoading({
        mask,
        title: title || "请稍候..."
      });
    }
  };
  class RequestTaskKeyStore {
    constructor(keys = []) {
      this.taskKeyStore = keys;
    }
    setRequestTaskStorage(taskArr, taskKey) {
      taskKey && taskArr.push(taskKey);
      this.taskKeyStore = taskArr;
    }
    removeRequestTaskKey(taskKey) {
      if (!taskKey)
        return;
      let taskArr = [...this.taskKeyStore];
      const index2 = taskArr.indexOf(taskKey);
      if (~index2) {
        taskArr.splice(index2, 1);
        this.setRequestTaskStorage(taskArr);
      }
    }
    requestTaskStorage(taskKey) {
      let result = false;
      if (!taskKey)
        return result;
      let taskArr = [...this.taskKeyStore];
      if (taskArr.length > 0) {
        if (~taskArr.indexOf(taskKey)) {
          result = true;
        } else {
          this.setRequestTaskStorage(taskArr, taskKey);
        }
      } else {
        taskKey && this.setRequestTaskStorage(taskArr, taskKey);
      }
      return result;
    }
  }
  function createTaskKeyStore(keys = []) {
    return new RequestTaskKeyStore(keys);
  }
  const store = createTaskKeyStore();
  class THORUI_INNER {
    constructor(initConfig = {}) {
      this.initConfig = initConfig;
      this.request = [];
      this.response = [];
      this.dispatchRequest = this.dispatchRequest.bind(this);
      this.loading = false;
    }
    dispatchRequest(config = {}) {
      let params = base.mergeConfig(this.initConfig, config);
      if (params.requestTaskKey && store.requestTaskStorage(params.requestTaskKey)) {
        return new Promise((resolve, reject) => {
          reject({
            statusCode: -9999,
            errMsg: "request:cancelled"
          });
        });
      }
      let options = base.getOptions(params);
      let promise = Promise.resolve(options);
      promise = promise.then((config2) => {
        if (params.showLoading && !this.loading) {
          this.loading = true;
          base.showLoading();
        }
        return new Promise((resolve, reject) => {
          let requestTask = uni.request({
            ...options,
            success: (res) => {
              if (params.showLoading && this.loading) {
                this.loading = false;
                uni.hideLoading();
              }
              resolve(params.concise ? res.data : res);
            },
            fail: (err) => {
              if (params.showLoading && this.loading) {
                this.loading = false;
                uni.hideLoading();
              }
              if (params.errorMsg) {
                base.toast(params.errorMsg);
              }
              reject(err);
            },
            complete: () => {
              store.removeRequestTaskKey(params.requestTaskKey);
            }
          });
          if (params.timeout && typeof params.timeout === "number" && params.timeout > 3e3) {
            setTimeout(() => {
              try {
                store.removeRequestTaskKey(params.requestTaskKey);
                requestTask.abort();
              } catch (e) {
              }
              resolve({
                statusCode: -9999,
                errMsg: "request:cancelled"
              });
            }, params.timeout);
          }
        });
      });
      return promise;
    }
  }
  const inner = new THORUI_INNER(base.config());
  const http = {
    interceptors: {
      request: {
        use: (fulfilled, rejected) => {
          inner.request.push({
            fulfilled,
            rejected
          });
        },
        eject: (name) => {
          if (inner.request[name]) {
            inner.request[name] = null;
          }
        }
      },
      response: {
        use: (fulfilled, rejected) => {
          inner.response.push({
            fulfilled,
            rejected
          });
        },
        eject: (name) => {
          if (inner.response[name]) {
            inner.response[name] = null;
          }
        }
      }
    },
    create(config) {
      inner.initConfig = base.mergeConfig(base.config(), config, true);
    },
    get(url, config = {}) {
      config.method = "GET";
      config.url = url || config.url || "";
      return http.request(config);
    },
    post(url, config = {}) {
      config.method = "POST";
      config.url = url || config.url || "";
      return http.request(config);
    },
    all(requests) {
      return Promise.all(requests);
    },
    request(config = {}) {
      let chain = [inner.dispatchRequest, void 0];
      let promise = Promise.resolve(config);
      inner.request.forEach((interceptor) => {
        chain.unshift(interceptor.fulfilled, interceptor.rejected);
      });
      inner.response.forEach((interceptor) => {
        chain.push(interceptor.fulfilled, interceptor.rejected);
      });
      while (chain.length) {
        promise = promise.then(chain.shift(), chain.shift());
      }
      return promise;
    }
  };
  const color = {
    primary: "#5677fc",
    success: "#07c160",
    warning: "#ff7900",
    danger: "#EB0909",
    pink: "#f74d54",
    blue: "#007AFF",
    link: "#586c94"
  };
  const propsConfig = {
    //组件内主色配置 
    color,
    //组件名称，字体图标组件 tui-icon
    tuiIcon: {
      //组件属性值
      size: 32,
      unit: "px",
      color: "#999"
    },
    //按钮组件 tui-button
    tuiButton: {
      height: "96rpx",
      size: 32
    },
    //列表项组件 tui-list-cell
    tuiListCell: {
      arrowColor: "#c0c0c0",
      lineColor: "#eaeef1",
      lineLeft: 44,
      padding: "26rpx 44rpx",
      color: "#333",
      size: 28
    },
    //按钮组件 tui-form-button
    tuiFormButton: {
      background: color.primary,
      color: "#fff",
      height: "96rpx",
      size: 32,
      radius: "6rpx"
    },
    //文本组件 tui-text
    tuiText: {
      size: 32,
      unit: "rpx",
      color: ""
    },
    //输入框组件 tui-input
    tuiInput: {
      requiredColor: color.danger,
      labelSize: 32,
      labelColor: "#333",
      size: 32,
      color: "#333",
      padding: "26rpx 30rpx",
      backgroundColor: "#FFFFFF",
      radius: 0
    },
    //表单项组件 tui-form-item
    tuiFormItem: {
      padding: "28rpx 30rpx",
      labelSize: 32,
      labelColor: "#333",
      labelFontWeight: 400,
      asteriskColor: color.danger,
      background: "#fff",
      arrowColor: "#c0c0c0",
      borderColor: "#eaeef1",
      radius: "0rpx",
      position: 2
    },
    //表单校验组件 tui-form
    tuiForm: {
      tipBackgroundColor: color.pink,
      duration: 2e3
    },
    //全局方法,调用 uni.$tui.toast
    toast(text, duration, success) {
      uni.showToast({
        duration: duration || 2e3,
        title: text || "出错啦~",
        icon: success ? "success" : "none"
      });
    },
    modal(title, content, showCancel, callback, confirmColor, confirmText) {
      uni.showModal({
        title: title || "提示",
        content,
        showCancel,
        cancelColor: "#555",
        confirmColor: confirmColor || color.primary,
        confirmText: confirmText || "确定",
        success(res) {
          if (res.confirm) {
            callback && callback(true);
          } else {
            callback && callback(false);
          }
        }
      });
    },
    //跳转页面
    href(url, isMain) {
      if (isMain) {
        uni.switchTab({
          url
        });
      } else {
        uni.navigateTo({
          url
        });
      }
    },
    rpx2px(value) {
      return uni.upx2px(value);
    }
  };
  http.create({
    host: "https://www.thorui.cn",
    header: {
      "content-type": "application/x-www-form-urlencoded"
    }
  });
  http.interceptors.request.use((config) => {
    let token = uni.getStorageSync("thorui_token");
    if (config.header) {
      config.header["Authorization"] = token;
    } else {
      config.header = {
        "Authorization": token
      };
    }
    return config;
  });
  http.interceptors.response.use((response) => {
    return response;
  });
  uni.$tui = propsConfig;
  function createApp() {
    const app = vue.createVueApp(App);
    app.use(store$1);
    app.config.globalProperties.tui = tui;
    app.config.globalProperties.http = http;
    app.config.globalProperties.$checkPlatform = checkPlatform;
    app.config.globalProperties.$isPC = isPC;
    return {
      app
    };
  }
  const { app: __app__, Vuex: __Vuex__, Pinia: __Pinia__ } = createApp();
  uni.Vuex = __Vuex__;
  uni.Pinia = __Pinia__;
  __app__.provide("__globalStyles", __uniConfig.styles);
  __app__._component.mpType = "app";
  __app__._component.render = () => {
  };
  __app__.mount("#app");
})(Vue);
