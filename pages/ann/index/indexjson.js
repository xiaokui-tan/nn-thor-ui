export default {
	tabBar: [{
			pagePath: '/pages/ann/index/index',
			text: '首页',
			iconPath: '/static/ann/tabbar/index.png',
			selectedIconPath: '/static/ann/tabbar/index-active.png',
			iconSize: 80
		},
		{
			pagePath: '/pages/ann/dangan/dindex',
			text: '档案',
			iconPath: '/static/ann/tabbar/dangan1.png',
			selectedIconPath: '/static/ann/tabbar/dangan1-active.png'
		},
		{
			pagePath: '/pages/ann/dangan/dindex',
			text: '发布',
			iconPath: '/static/ann/tabbar/fabu.png',
			hump: true,
			selectedIconPath: '/static/ann/tabbar/fabu-active.png'
		},
		{
			pagePath: '/pages/ann/dangan/dindex',
			text: '记账',
			iconPath: '/static/ann/tabbar/jizhang1.png',
			selectedIconPath: '/static/ann/tabbar/jizhang1-active.png'
		},
		{
			pagePath: '/pages/ann/my/my',
			text: '我的',
			iconPath: '/static/ann/tabbar/my1.png',
			selectedIconPath: '/static/ann/tabbar/my1-active.png'
		}
	],
	category: [{
			img: '../../../static/ann/index/dangan.png',
			name: '档案',
			page: '/pages/ann/dangan/dindex'
		},
		{
			img: '../../../static/ann/index/jizhang.png',
			name: '记账',
			page: '/pages/ann/dangan/dindex'
		}
	],
	action: [{
		img: '../../../static/ann/index/gongying.png',
		name: '供应',
		page: '/pages/ann/supply/supply'
	}, {
		img: '../../../static/ann/index/qiugou.png',
		name: '求购',
		page: '/pages/ann/orders/order'
	}, {
		img: '../../../static/ann/index/ditu.png',
		name: '地图',
		page: '/pages/ann/index/maps'
	}, {
		img: '../../../static/ann/index/hangqing.png',
		name: '行情',
		page: '/pages/ann/market/market'
	}],
	items: [{
			id: 0,
			text: '全部',
			isScreen: false,
			down: false
		}, {
			id: 1,
			text: '推荐',
			isScreen: false,
			down: false
		},
		{
			id: 2,
			text: '供应',
			isScreen: false,
			down: false
		},
		{
			id: 3,
			text: '求购',
			isScreen: false,
			down: false
		},
		{
			id: 4,
			text: '综合',
			isScreen: false,
			down: false
		}
	],
	comsorts: [{
		id: '1',
		value: '按时间从近到远',
		selected: false
	}, {
		id: '2',
		value: '按时间从远到近',
		selected: false
	}, 
	// {
	// 	id: '3',
	// 	value: '按位置从近到远',
	// 	selected: false
	// }, {
	// 	id: '4',
	// 	value: '按总价从低到高',
	// 	selected: false
	// }, {
	// 	id: '5',
	// 	value: '按分类',
	// 	selected: false
	// }, 
	],
	indexdetails: [{
			"id": "1",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡",
			"actiontype": 0, //0供应 1求购
			"isrecommend": 0, //是否推荐，0否1是
			"address": "广东省深圳市南山区XX街XX号",
			"hasphonenum": true, //有联系电话
			"allowonlineconnect": false, //可以在线沟通
			"fabudate": "2024-05-01" //发布日期
		},
		{
			"id": "2",
			"image": "../../../static/ann/images/neimeng-cattle.png",
			"title": "草原牛",
			"actiontype": 1,
			"isrecommend": 1, //是否推荐，0否1是
			"address": "内蒙古自治区呼和浩特市赛罕区XX路XX号",
			"hasphonenum": false,
			"allowonlineconnect": true,
			"fabudate": "2024-05-07"
		},
		{
			"id": "3",
			"image": "../../../static/ann/images/goat.png",
			"title": "山羊",
			"actiontype": 0,
			"isrecommend": 1, //是否推荐，0否1是
			"address": "河南省郑州市二七区XX大道XX号",
			"hasphonenum": true,
			"allowonlineconnect": true,
			"fabudate": "2024-05-04"
		},
		{
			"id": "4",
			"image": "../../../static/ann/images/hetian-horse.png",
			"title": "和田马",
			"actiontype": 1,
			"isrecommend": 0, //是否推荐，0否1是
			"address": "浙江省杭州市西湖区XX路XX号",
			"hasphonenum": false,
			"allowonlineconnect": false
		},
		{
			"id": "6",
			"image": "../../../static/ann/images/rabbit.png",
			"title": "兔子",
			"actiontype": 1,
			"isrecommend": 1, //是否推荐，0否1是
			"address": "广东省广州市天河区XX路XX号",
			"hasphonenum": false,
			"allowonlineconnect": true,
			"fabudate": "2024-05-09"
		},
		{
			"id": "7",
			"image": "../../../static/ann/images/jiayang-duck.png",
			"title": "家养鸭",
			"actiontype": 0,
			"isrecommend": 0, //是否推荐，0否1是
			"address": "福建省厦门市思明区XX街XX号",
			"hasphonenum": true,
			"allowonlineconnect": true,
			"fabudate": "2024-05-03"
		},
		{
			"id": "8",
			"image": "../../../static/ann/images/yufei-goose.png",
			"title": "育肥鹅",
			"actiontype": 1,
			"isrecommend": 1, //是否推荐，0否1是
			"address": "四川省成都市武侯区XX路XX号",
			"hasphonenum": false,
			"allowonlineconnect": false,
			"fabudate": "2024-05-02"
		},
		{
			"id": "9",
			"image": "../../../static/ann/images/hemu-dove.png",
			"title": "食用贺姆鸽",
			"actiontype": 0,
			"isrecommend": 0, //是否推荐，0否1是
			"address": "湖南省长沙市芙蓉区XX街XX号",
			"hasphonenum": true,
			"allowonlineconnect": true,
			"fabudate": "2024-05-11"
		}, {
			"id": "10",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡1",
			"actiontype": 0, //0供应 1求购
			"isrecommend": 0, //是否推荐，0否1是
			"address": "广东省深圳市南山区XX街XX号",
			"hasphonenum": true, //有联系电话
			"allowonlineconnect": false, //可以在线沟通
			"fabudate": "2024-01-01" //发布日期
		},
		{
			"id": "11",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡11",
			"actiontype": 1,
			"isrecommend": 0,
			"address": "陕西省西安市长安区杨庄街道",
			"hasphonenum": false,
			"allowonlineconnect": true,
			"fabudate": "2023-04-21"
		},
		{
			"id": "12",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡12",
			"actiontype": 0,
			"isrecommend": 1,
			"address": "浙江省杭州市萧山区宁围镇",
			"hasphonenum": true,
			"allowonlineconnect": true,
			"fabudate": "2024-01-12"
		},
		{
			"id": "13",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡13",
			"actiontype": 0,
			"isrecommend": 0,
			"address": "四川省成都市青羊区草市街",
			"hasphonenum": false,
			"allowonlineconnect": false,
			"fabudate": "2023-06-05"
		},
		{
			"id": "14",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡14",
			"actiontype": 1,
			"isrecommend": 1,
			"address": "湖南省长沙市望城区金星村",
			"hasphonenum": true,
			"allowonlineconnect": true,
			"fabudate": "2023-11-17"
		},
		{
			"id": "15",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡15",
			"actiontype": 1,
			"isrecommend": 0,
			"address": "广东省深圳市宝安区新安街道",
			"hasphonenum": false,
			"allowonlineconnect": false,
			"fabudate": "2024-03-22"
		},
		{
			"id": "16",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡16",
			"actiontype": 0,
			"isrecommend": 1,
			"address": "江苏省南京市鼓楼区华侨路",
			"hasphonenum": true,
			"allowonlineconnect": true,
			"fabudate": "2023-07-08"
		},
		{
			"id": "17",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡17",
			"actiontype": 0,
			"isrecommend": 0,
			"address": "福建省厦门市思明区莲前街道",
			"hasphonenum": false,
			"allowonlineconnect": false,
			"fabudate": "2023-09-19"
		},
		{
			"id": "18",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡18",
			"actiontype": 1,
			"isrecommend": 1,
			"address": "辽宁省沈阳市沈河区北站路",
			"hasphonenum": true,
			"allowonlineconnect": true,
			"fabudate": "2024-05-30"
		},
		{
			"id": "19",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡19",
			"actiontype": 1,
			"isrecommend": 0,
			"address": "内蒙古自治区呼和浩特市回民区",
			"hasphonenum": false,
			"allowonlineconnect": true,
			"fabudate": "2023-12-01"
		},
		{
			"id": "20",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡20",
			"actiontype": 0,
			"isrecommend": 1,
			"address": "山东省青岛市市南区香港中路",
			"hasphonenum": true,
			"allowonlineconnect": false,
			"fabudate": "2023-08-15"
		},
		{
			"id": "21",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡21",
			"actiontype": 1,
			"isrecommend": 0,
			"address": "北京市东城区王府井",
			"hasphonenum": true,
			"allowonlineconnect": false,
			"fabudate": "2023-05-09"
		},
		{
			"id": "22",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡22",
			"actiontype": 0,
			"isrecommend": 1,
			"address": "上海市黄浦区南京东路",
			"hasphonenum": false,
			"allowonlineconnect": true,
			"fabudate": "2023-10-23"
		},
		{
			"id": "23",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡23",
			"actiontype": 1,
			"isrecommend": 1,
			"address": "天津市和平区劝业场",
			"hasphonenum": true,
			"allowonlineconnect": false,
			"fabudate": "2024-02-17"
		},
		{
			"id": "24",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡24",
			"actiontype": 0,
			"isrecommend": 0,
			"address": "重庆市渝中区解放碑",
			"hasphonenum": false,
			"allowonlineconnect": true,
			"fabudate": "2023-03-12"
		},
		{
			"id": "25",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡25",
			"actiontype": 1,
			"isrecommend": 1,
			"address": "河北省石家庄市长安区",
			"hasphonenum": true,
			"allowonlineconnect": false,
			"fabudate": "2024-07-28"
		},
		{
			"id": "26",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡26",
			"actiontype": 0,
			"isrecommend": 0,
			"address": "山西省太原市迎泽区",
			"hasphonenum": false,
			"allowonlineconnect": true,
			"fabudate": "2023-08-11"
		},
		{
			"id": "27",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡27",
			"actiontype": 1,
			"isrecommend": 1,
			"address": "内蒙古自治区呼和浩特市新城区",
			"hasphonenum": true,
			"allowonlineconnect": false,
			"fabudate": "2023-12-20"
		},
		{
			"id": "28",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡28",
			"actiontype": 0,
			"isrecommend": 0,
			"address": "辽宁省大连市中山区",
			"hasphonenum": false,
			"allowonlineconnect": true,
			"fabudate": "2024-06-05"
		},
		{
			"id": "29",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡29",
			"actiontype": 1,
			"isrecommend": 1,
			"address": "吉林省长春市南关区",
			"hasphonenum": true,
			"allowonlineconnect": false,
			"fabudate": "2023-09-07"
		},
		{
			"id": "30",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡30",
			"actiontype": 0,
			"isrecommend": 0,
			"address": "黑龙江省哈尔滨市道里区",
			"hasphonenum": false,
			"allowonlineconnect": true,
			"fabudate": "2024-01-15"
		},
		{
			"id": "31",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡31",
			"actiontype": 1,
			"isrecommend": 1,
			"address": "上海市浦东新区张江镇",
			"hasphonenum": true,
			"allowonlineconnect": false,
			"fabudate": "2023-10-05"
		},
		{
			"id": "32",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡32",
			"actiontype": 0,
			"isrecommend": 0,
			"address": "江苏省苏州市工业园区",
			"hasphonenum": false,
			"allowonlineconnect": true,
			"fabudate": "2023-11-12"
		},
		{
			"id": "33",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡33",
			"actiontype": 1,
			"isrecommend": 1,
			"address": "浙江省杭州市西湖区",
			"hasphonenum": true,
			"allowonlineconnect": false,
			"fabudate": "2024-01-20"
		},
		{
			"id": "34",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡34",
			"actiontype": 0,
			"isrecommend": 0,
			"address": "安徽省合肥市瑶海区",
			"hasphonenum": false,
			"allowonlineconnect": true,
			"fabudate": "2023-12-15"
		},
		{
			"id": "35",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡35",
			"actiontype": 1,
			"isrecommend": 1,
			"address": "福建省福州市台江区",
			"hasphonenum": true,
			"allowonlineconnect": false,
			"fabudate": "2024-02-28"
		},
		{
			"id": "36",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡36",
			"actiontype": 0,
			"isrecommend": 0,
			"address": "江西省南昌市东湖区",
			"hasphonenum": false,
			"allowonlineconnect": true,
			"fabudate": "2023-08-19"
		},
		{
			"id": "37",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡37",
			"actiontype": 1,
			"isrecommend": 1,
			"address": "山东省济南市历下区",
			"hasphonenum": true,
			"allowonlineconnect": false,
			"fabudate": "2023-09-10"
		},
		{
			"id": "38",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡38",
			"actiontype": 0,
			"isrecommend": 0,
			"address": "河南省郑州市中原区",
			"hasphonenum": false,
			"allowonlineconnect": true,
			"fabudate": "2024-03-05"
		},
		{
			"id": "39",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡39",
			"actiontype": 1,
			"isrecommend": 1,
			"address": "湖北省武汉市江岸区",
			"hasphonenum": true,
			"allowonlineconnect": false,
			"fabudate": "2023-07-30"
		},
		{
			"id": "40",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡40",
			"actiontype": 0,
			"isrecommend": 0,
			"address": "湖南省长沙市芙蓉区",
			"hasphonenum": false,
			"allowonlineconnect": true,
			"fabudate": "2024-04-22"
		},
		{
			"id": "41",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡41",
			"actiontype": 1,
			"isrecommend": 1,
			"address": "广东省广州市天河区",
			"hasphonenum": true,
			"allowonlineconnect": false,
			"fabudate": "2023-10-18"
		},
		{
			"id": "42",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡42",
			"actiontype": 0,
			"isrecommend": 0,
			"address": "广西壮族自治区南宁市青秀区",
			"hasphonenum": false,
			"allowonlineconnect": true,
			"fabudate": "2024-01-12"
		},
		{
			"id": "43",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡43",
			"actiontype": 1,
			"isrecommend": 1,
			"address": "海南省海口市龙华区",
			"hasphonenum": true,
			"allowonlineconnect": false,
			"fabudate": "2023-11-25"
		},
		{
			"id": "44",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡44",
			"actiontype": 0,
			"isrecommend": 0,
			"address": "重庆市江北区",
			"hasphonenum": false,
			"allowonlineconnect": true,
			"fabudate": "2024-02-03"
		},
		{
			"id": "45",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡45",
			"actiontype": 1,
			"isrecommend": 1,
			"address": "四川省成都市武侯区",
			"hasphonenum": true,
			"allowonlineconnect": false,
			"fabudate": "2023-12-08"
		},
		{
			"id": "46",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡46",
			"actiontype": 0,
			"isrecommend": 0,
			"address": "贵州省贵阳市南明区",
			"hasphonenum": false,
			"allowonlineconnect": true,
			"fabudate": "2024-03-17"
		},
		{
			"id": "47",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡47",
			"actiontype": 1,
			"isrecommend": 1,
			"address": "云南省昆明市五华区",
			"hasphonenum": true,
			"allowonlineconnect": false,
			"fabudate": "2023-08-25"
		},
		{
			"id": "48",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡48",
			"actiontype": 0,
			"isrecommend": 0,
			"address": "西藏自治区拉萨市城关区",
			"hasphonenum": false,
			"allowonlineconnect": true,
			"fabudate": "2024-05-10"
		},
		{
			"id": "49",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡49",
			"actiontype": 1,
			"isrecommend": 1,
			"address": "陕西省西安市新城区",
			"hasphonenum": true,
			"allowonlineconnect": false,
			"fabudate": "2023-09-15"
		},
		{
			"id": "50",
			"image": "../../../static/ann/images/free-range-chicken7.png",
			"title": "农家土鸡50",
			"actiontype": 0,
			"isrecommend": 0,
			"address": "甘肃省兰州市城关区",
			"hasphonenum": false,
			"allowonlineconnect": true,
			"fabudate": "2024-06-20"
		}
	],
}