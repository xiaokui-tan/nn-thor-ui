<!-- render.js -->
<
script module = "amap"
lang = "renderjs" >
	export default {
		data() {
			return {
				map: null,
				myData: [],
			}
		},
		methods: {
			receiveMsg(newValue, oldValue, ownerInstance, instance) {
				console.log(newValue);
			},
			currentIndexChange(val) {
				console.log(val);
			},
			// 引入高德地图SDK
			init() {
				if (typeof window.AMap == 'function') {
					this.initAmap();
				} else {
					// 动态引入较大类库避免影响页面展示
					const script = document.createElement('script');
					script.src = 'https://webapi.amap.com/maps?v=1.4.15&key=你的key';
					script.onload = this.initAmap.bind(this);
					document.head.appendChild(script);
				}
			},
			//初始化地图
			initAmap() {
				this.map = new AMap.Map('amap', {
					resizeEnable: true,
					center: [121.664985, 29.971749],
					zooms: [14, 20], //设置地图级别范围
					zoom: 15,
				})
				this.map.on('complete', () => {
					console.log('加载完成')
				})
			},
		}
	} <
	/script>