import tuiBottomPopup from "@/components/thorui/tui-bottom-popup/tui-bottom-popup.vue"
import Empty from "./empty.vue"
import json from './djson.js'
export default {
	components: {
		tuiBottomPopup,
		Empty
	},
	data() {
		return {
			visible: true, // 根据您的逻辑设定 visible 的值
			category: json.category,
			basecategorys: json.basecategorys,
			popupShow: false, // 弹出框显示状态
			itemimg: "../../../static/ann/index/danganadd.png",
			currentTab: 0, // 默认选中的页签索引
			tabs: [ // 根据您的需求动态添加页签
				{
					name: '家畜',
					backgroundColor: 'rgb(253 250 250)',
					items: []
				}, // 每个页签包含一个 items 数组，用于存储该页签下的动物数据
				{
					name: '家禽',
					backgroundColor: 'rgb(253 250 250)',
					items: []
				},
				{
					name: '水产',
					backgroundColor: 'rgb(253 250 250)',
					items: []
				}
			],
		}
	},
	mounted() {
		// 将动物数据分类并存储到相应的页签中
		this.basecategorys.forEach(item => {
			if (item.animaltype === 1) {
				this.tabs[0].items.push(item); // 将家畜数据存储到第一个页签中
			} else if (item.animaltype === 2) {
				this.tabs[1].items.push(item); // 将家禽数据存储到第二个页签中
			} else if (item.animaltype === 3) {
				this.tabs[2].items.push(item); // 将水产数据存储到第三个页签中
			}
		});
		if (this.category.length <= 0) {
			this.visible = false;
		}
	},
	methods: {
		//调用此方法显示弹层
		showPopup: function() {
			this.popupShow = true
		},
		hiddenPopup: function() {
			this.popupShow = false
		},
		selected: function() {
			this.hiddenPopup();
		},
		change(e) {
			this.currentTab = e.index
		},
		navigateTo(item) {
			//switchTab用于跳转tabbar页面
			//navigateTo用于跳转普通页面
			uni.navigateTo({
				url: item.page + '?name=' + item.id
			});
		},
	}
}