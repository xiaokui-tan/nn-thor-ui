export default {
	category: [
		{
			id: 1,
			img: '../../../static/ann/index/dangan.png',
			name: '猪',
			page: '/pages/ann/dangan/dlist'
		},
		{
			id: 2,
			img: '../../../static/ann/index/jizhang.png',
			name: '牛',
			page: '/pages/ann/dangan/dlist'
		},
		{
			id: 3,
			img: '../../../static/ann/index/jizhang.png',
			name: '羊',
			page: '/pages/ann/dangan/dlist'
		},
		{
			id: 4,
			img: '../../../static/ann/index/jizhang.png',
			name: '鸡',
			page: '/pages/ann/dangan/dlist'
		},
	],
	basecategorys: [{
			"id": 1,
			"animaltype": 1,
			"isbreedhome": 0,
			"img": "../../../static/ann/index/dangan.png",
			"name": "猪"
		},
		{
			"id": 2,
			"animaltype": 1,
			"isbreedhome": 0,
			"img": "../../../static/ann/index/dangan.png",
			"name": "牛"
		},
		{
			"id": 3,
			"animaltype": 1,
			"isbreedhome": 0,
			"img": "../../../static/ann/index/dangan.png",
			"name": "羊"
		},
		{
			"id": 4,
			"animaltype": 1,
			"isbreedhome": 0,
			"img": "../../../static/ann/index/dangan.png",
			"name": "马"
		},
		{
			"id": 5,
			"animaltype": 1,
			"isbreedhome": 0,
			"img": "../../../static/ann/index/dangan.png",
			"name": "驴"
		},
		{
			"id": 6,
			"animaltype": 1,
			"isbreedhome": 0,
			"img": "../../../static/ann/index/dangan.png",
			"name": "骡"
		},
		{
			"id": 7,
			"animaltype": 1,
			"isbreedhome": 0,
			"img": "../../../static/ann/index/dangan.png",
			"name": "兔"
		},
		{
			"id": 8,
			"animaltype": 1,
			"isbreedhome": 0,
			"img": "../../../static/ann/index/dangan.png",
			"name": "狗"
		},
		{
			"id": 9,
			"animaltype": 1,
			"isbreedhome": 1,
			"img": "../../../static/ann/index/dangan.png",
			"name": "骆驼"
		},
		{
			"id": 10,
			"animaltype": 1,
			"isbreedhome": 1,
			"img": "../../../static/ann/index/dangan.png",
			"name": "鹿"
		},
		{
			"id": 11,
			"animaltype": 1,
			"isbreedhome": 1,
			"img": "../../../static/ann/index/dangan.png",
			"name": "蛇"
		},
		{
			"id": 12,
			"animaltype": 2,
			"isbreedhome": 0,
			"img": "../../../static/ann/index/dangan.png",
			"name": "鸡"
		},
		{
			"id": 13,
			"animaltype": 2,
			"isbreedhome": 0,
			"img": "../../../static/ann/index/dangan.png",
			"name": "鸭"
		},
		{
			"id": 14,
			"animaltype": 2,
			"isbreedhome": 0,
			"img": "../../../static/ann/index/dangan.png",
			"name": "鹅"
		},
		{
			"id": 15,
			"animaltype": 2,
			"isbreedhome": 1,
			"img": "../../../static/ann/index/dangan.png",
			"name": "鸽"
		},
		{
			"id": 16,
			"animaltype": 2,
			"isbreedhome": 1,
			"img": "../../../static/ann/index/dangan.png",
			"name": "鹌鹑"
		},
		{
			"id": 17,
			"animaltype": 2,
			"isbreedhome": 1,
			"img": "../../../static/ann/index/dangan.png",
			"name": "鹦鹉"
		},
		{
			"id": 18,
			"animaltype": 2,
			"isbreedhome": 1,
			"img": "../../../static/ann/index/dangan.png",
			"name": "斑鸠"
		},
		{
			"id": 19,
			"animaltype": 2,
			"isbreedhome": 1,
			"img": "../../../static/ann/index/dangan.png",
			"name": "鸸鹋"
		},
		{
			"id": 20,
			"animaltype": 2,
			"isbreedhome": 1,
			"img": "../../../static/ann/index/dangan.png",
			"name": "鸵鸟"
		},
		{
			"id": 21,
			"animaltype": 3,
			"isbreedhome": 1,
			"img": "../../../static/ann/index/dangan.png",
			"name": "牛蛙"
		},
		{
			"id": 22,
			"animaltype": 3,
			"isbreedhome": 1,
			"img": "../../../static/ann/index/dangan.png",
			"name": "鳄鱼"
		},
		{
			"id": 23,
			"animaltype": 3,
			"isbreedhome": 1,
			"img": "../../../static/ann/index/dangan.png",
			"name": "貂"
		},
		{
			"id": 24,
			"animaltype": 3,
			"isbreedhome": 1,
			"img": "../../../static/ann/index/dangan.png",
			"name": "狐"
		},
		{
			"id": 25,
			"animaltype": 3,
			"isbreedhome": 1,
			"img": "../../../static/ann/index/dangan.png",
			"name": "貉"
		}
	],
}