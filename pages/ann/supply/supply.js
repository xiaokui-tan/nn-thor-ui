import tuiScrollTop from "@/components/thorui/tui-scroll-top/tui-scroll-top.vue"

import sjson from '../supply/supplyjson.js'
import json from '../index/indexjson.js'

export default {
	components: {
		tuiScrollTop
	},
	props: {
		type: {
			type: Number,
			default: 0 // 默认值为0，代表供应
		}
	},
	data() {
		return {
			scrollTop: 0,
			isIndex: true,
			isShare: false,
			hideTop: false,
			customShare: true,
			visible: false,
			isdown: false,
			platform: '',
			isPC: false,
			//type: 0, //供应
			searchText: '',
			items: sjson.items,
			comsorts: json.comsorts,
			indexdetails: [],
			convertIndexDetails: []
		}
	},
	created() {

	},
	mounted() {
		this.platform = this.$checkPlatform();
		// this.isPC = this.$isPC();
		this.initshow();
	},
	onShow() {
		this.platform = this.$checkPlatform();
		// this.isPC = this.$isPC();
		this.initshow();
	},
	activated() {
		this.platform = this.$checkPlatform();
		// this.isPC = this.$isPC();
		this.initshow();
	},
	mounted() {
		const item = this.items.find(item => item.id === 0);
		if (item) {
			item.isScreen = true;
		}
		this.updatedIndexDetails(); //枚举类型转换为对应名称
		this.sortby(1); //默认排序
	},
	onPageScroll(e) {
		if (!this.hideTop) {
			this.scrollTop = e.scrollTop;
		}
	},
	computed: {
		// searchBarStyle() {
		// 	//const systemInfo = uni.getSystemInfoSync();
		// 	const topValue = (this.platform === 'android' || this.platform === 'ios') ? '80rpx' :
		// 		'70rpx';
		// 	console.log('searchBarStyle executed', topValue);
		// 	return {
		// 		position: 'fixed',
		// 		top: topValue,
		// 		left: '0',
		// 		right: '0',
		// 		zIndex: '9999'
		// 	};
		// },
		// listviewStyle() {
		// 	const topValue = (this.platform === 'android' || this.platform === 'ios') ? '80rpx' : '70rpx';
		// 	return {
		// 		marginTop: topValue
		// 	};
		// }
	},
	methods: {
		convertActionType(actionType) { //供应、求购 枚举类型转换为对应名称
			return actionType === 0 ? "供应" : "求购";
		},
		updatedIndexDetails() {
			this.indexdetails = json.indexdetails.filter(item => item.actiontype === this.type);
			this.convertIndexDetails = this.indexdetails.map(item => {
				return {
					...item, // 展开原始对象
					actionTypeText: this.convertActionType(item.actiontype) // 添加新属性
				};
			});
		},
		initshow() {
			console.log('Platform：' + this.platform);
			// console.log(this.isPC);
			if (this.$checkPlatform) {
				if (this.platform !== 'web') {
					this.visible = false;
					this.isdown = false;
					this.setItemsScreenStatus();
					this.updatedIndexDetails();
					this.sortby(1);
				}
			}
		},
		setItemsScreenStatus() {
			this.items.forEach(item => {
				item.isScreen = item.id === 0;
			});
		},
		//搜索栏输入后点击搜索
		handleInput(event) {
			// 更新 searchText，确保事件对象有detail属性
			this.searchText = event.value;
			this.search(this.searchText);
		},
		//搜索
		search(searchText) {
			this.updatedIndexDetails();
			if (this.searchText.length != 0) {
				this.searchItems(searchText);
			};
		},
		//模糊搜索
		searchItems(query) {
			// 将查询字符串转换为小写，以实现不区分大小写的搜索
			const lowerCaseQuery = query.toLowerCase();
			this.convertIndexDetails = this.convertIndexDetails.filter(item => {
				// 检查每个项目的title, actionTypeText, 和 address字段是否包含搜索关键词
				return item.title.toLowerCase().includes(lowerCaseQuery) ||
					item.actionTypeText.toLowerCase().includes(lowerCaseQuery) ||
					item.address.toLowerCase().includes(lowerCaseQuery);
			});
		},
		//搜索页签切换
		screen(item) {
			// 将所有项的isScreen属性重置为false
			this.items.forEach(item => {
				item.isScreen = false;
			});

			// 设置被点击项的isScreen属性为true
			item.isScreen = true;
			if (item.id == 4) {
				this.isdown = !this.isdown;
				this.visible = this.isdown;
			} else {
				this.isdown = false;
				this.visible = false;
			}
			this.screensearch(item);
			this.sortby(1);
		},
		screensearch(item) {
			if (item.id != 4) {
				this.search(this.searchText);
			}

			if (item.id == 0) {

			} else if (item.id == 1) {
				this.convertIndexDetails = this.convertIndexDetails.filter(item => item.isrecommend === 1);
			} else if (item.id == 2) {
				this.convertIndexDetails = this.convertIndexDetails.filter(item => item.actiontype === 0);
			}
		},
		comorderby(item) {
			this.search(this.searchText);
			if (item.id == 1) {
				this.sortby(item.id);
				this.visible = false;
				this.isdown = false;
			} else if (item.id == 2) {
				this.sortby(item.id);
				this.visible = false;
				this.isdown = false;
			}
		},
		sortby(order) {
			// 按 fabudate 倒序排列
			this.convertIndexDetails.sort((a, b) => {
				// 将 fabudate 从字符串转换为日期对象
				const dateA = new Date(a.fabudate);
				const dateB = new Date(b.fabudate);
				// 比较日期
				return order == 1 ? (dateB - dateA) : (dateA - dateB);
			});
		},
		goIndex() {
			uni.switchTab({
				url: '/pages/ann/index/index'
			});
		},
	}
}