import App from './App'
import {
	checkPlatform,
	isPC
} from './utils/utils'; // 确保路径正确
import store from './store'
import tui from './common/httpRequest'
import http from './components/common/tui-request'
import propsConfig from './components/thorui/tui-config/index'


//初始化请求配置项
http.create({
	host: 'https://www.thorui.cn',
	header: {
		'content-type': 'application/x-www-form-urlencoded'
	}
})
//请求拦截
http.interceptors.request.use(config => {
	let token = uni.getStorageSync('thorui_token');
	if (config.header) {
		config.header['Authorization'] = token
	} else {
		config.header = {
			'Authorization': token
		}
	}
	return config
})
//响应拦截
http.interceptors.response.use(response => {
	//TODO
	return response
})

//全局组件配置
uni.$tui = propsConfig

// #ifndef VUE3
import Vue from 'vue'

Vue.config.productionTip = false
Vue.prototype.http = http
Vue.prototype.tui = tui
Vue.prototype.$store = store
App.mpType = 'app'

const app = new Vue({
	store,
	...App
})
app.$mount()
// #endif

// import BaiduMap from 'vue-baidu-map'
// Vue.use(BaiduMap, {
// 	// ak 是在百度地图开发者平台申请的密钥 详见 http://lbsyun.baidu.com/apiconsole/key */
// 	ak: 'PlhFWpA02aoURjAOpnWcRGqw7AI8EEyO'  //sRDDfAKpCSG5iF1rvwph4Q95M6tDCApL
// })

// #ifdef VUE3
import {
	createSSRApp
} from 'vue'
export function createApp() {
	const app = createSSRApp(App)
	app.use(store)
	app.config.globalProperties.tui = tui;
	app.config.globalProperties.http = http;
	// 设置全局属性
	app.config.globalProperties.$checkPlatform = checkPlatform;
	app.config.globalProperties.$isPC = isPC;
	return {
		app
	}
}
// #endif