export function checkPlatform() {
	const systemInfo = uni.getSystemInfoSync();
	return systemInfo.platform; // 'android', 'ios', 'web'
}

export function isPC() {
	let systemInfo = uni.getSystemInfoSync();
	    console.log(systemInfo);	
	    // 判断平台
	    if (systemInfo.platform === 'android' || systemInfo.platform === 'ios') {
	        // 真实的移动设备
	        return false;
	    } else if (systemInfo.platform === 'windows' || systemInfo.platform === 'mac') {
	        // 桌面环境
	        return true;
	    } else {
	        // 其他情况，可能需要额外逻辑来判断
	        return true;
	    }
}